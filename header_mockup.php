<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt_BR" lang="pt_BR">
<head>
	<base href="pastoral_mock" />
    <meta name="language" content="pt_BR" />
    <meta charset='utf-8'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134533467-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-134533467-1');
    </script>


    <link href="/css/frontend.css?time=1602674795" media="screen" rel="stylesheet" type="text/css" />
<link href="/css/frontend-brasil.css" media="screen" rel="stylesheet" type="text/css" />
    <!-- JQUERY -->
    <script type="text/javascript" src="/js/jquery/jquery.js?time=1528113125"></script>
    <script type="text/javascript" src="/js/jquery/jquery.base64.min.js?time=1554906037"></script>

    <!-- WIDGETS -->
    <script type="text/javascript" src="/js/widgets.js?time=1556650917"></script>

    <!-- JQUERY UI. Arquivo JS e CSS -->
    <script type="text/javascript" src="/js/jquery/ui/js/jquery-ui-1.8.5.custom.min.js"></script>
    <script type="text/javascript" src="/js/jquery/ui/js/brazil/jquery.ui.datepicker-pt-BR.js"></script>
    <link type="text/css" rel="stylesheet" media="screen" href="/js/jquery/ui/css/1/jquery-ui-1.8.5.custom.css?time=1556648172"/>

    <!-- JQUERY TOOLTIP -->
    <script type="text/javascript" src="/js/jquery/jquery.bgiframe.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery.tooltip.js"></script>

    <!-- JQUERY MASKED INPUT -->
    <script type="text/javascript" src="/js/jquery/jquery.maskedinput-1.2.2.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery.maskMoney.js"></script>

    <!-- FRONTEND -->
    <script type="text/javascript" src="/js/frontend.js?time=1603458374"></script>

    <!-- TINYMCE -->
    <script type="text/javascript" src="https://cdn.tiny.cloud/1/9xilhj0srad9875vrhy3z6kwi7bwo6lzvzce9sdu8k1hlda5/tinymce/5/tinymce.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script type="text/javascript">

        let actualUrl = window.location.origin;

        tinymce.PluginManager.add('pciartifact', function(editor, url) {

            // Add a button that opens a window
            editor.ui.registry.addButton('customArtifactInsertButton', {
                text: 'Artefatos',
                onAction: function() {

                    tinymce.activeEditor.windowManager.openUrl({
                        title: 'Selecione o Artefato',
                        url : '/services/artifacts/explorer?targetUrl='+window.location.pathname,
                        height: 700,
                        width: 1000
                    });

                    window.selectedArtifactExplorer = function(server_id)
                    {
                        tinymce.activeEditor.windowManager.close();
                        tinymce.activeEditor.insertContent('<pci-artifact unselectable="on" class="mceNonEditable" server-id="'+server_id+'">Artefato '+server_id+'</pci-artifact>');
                    };

                    window.closeArtifactExplorer = function()
                    {
                        tinymce.activeEditor.windowManager.close();
                    };

                }
            });

        });

        tinymce.init({

            // forced_root_block: "",
            selector: ".editor",
            extended_valid_elements: "pci-artifact[*]",
            custom_elements: "pci-artifact",
            noneditable_noneditable_class: "mceNonEditable",

            paste_remove_styles_if_webkit: false,

            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste pciartifact noneditable autoresize"
            ],

            toolbar: "undo redo | styleselect fontsizeselect | forecolor backcolor | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | customArtifactInsertButton | fullscreen",

            relative_urls: false,
            remove_script_host : false,
            content_css : actualUrl + "/css/app_lider.css",

            setup : function(editor) {
                editor.on('PostProcess', function(ed) {
                    ed.content = ed.content.replace(/(<p>&nbsp;<\/p>)/gi,'<br />');
                });
            }
        });

    </script>

    <script type="text/javascript" src="/js/form.js?time=1602674777"></script>
<script type="text/javascript" src="/js/paginator.js?time=1428499180"></script>
    <title>:: Pastoral da Criança - Sistema de Informações 2021</title>
    <link rel="shortcut icon" href="/images/favicon.ico" />

            <script> let jsonData = {}; </script>
    
    
</head>

<body id="corpo">
        <div id="head">
        <script>
    function signOut() {
        console.log('passa');
        var auth2 = gapi.auth2
        if (auth2 == undefined) {
            window.location.href = '/user/logout';
            return;
        }
        auth2.getAuthInstance().signOut().then(function () {
            window.location.href = '/user/logout';
        });
    }

    $("body").ready(function () {
        $("#abre, #menu").on("click", function (e) {
            e.stopPropagation();
            $("#menu").toggleClass('show');
        });
        $("body").on("click", function () {
            $("#menu").removeClass('show');
        });
    });


</script>

<body>
        <div id="topBar">
        <form name="formSessionCulture" id="formSessionCulture" action="/culture/set-session-culture/index/origin-module/user/origin-controller/home/origin-action/diocese" method="POST" onsubmit="$('#carregando').show()">            
                        <div id="headerTop">
                <div>
                    
                </div>
                <div>
                    <span class="topBarItem">Base de dados: <b>dory</b> </span>
                    <span>
                        <select name="session_culture_id" id="session_culture_id" onchange="$('#formSessionCulture').submit()">
                                                        <option value="6">
                                Francês                            </option>
                                                        <option value="2">
                                English                            </option>
                                                        <option value="10">
                                kreyòl ayisyen                            </option>
                                                        <option value="5">
                                Português Angola                            </option>
                                                        <option value="9">
                                Español de Peru                            </option>
                                                        <option value="11">
                                Español de Republica Dominicana                            </option>
                                                        <option value="12">
                                French (Benin)                            </option>
                                                        <option value="3">
                                Español                            </option>
                                                        <option value="7">
                                Espanhol Guatemala                            </option>
                                                        <option value="1" selected='selected'>
                                Português do Brasil                            </option>
                                                    </select>
                    </span>
                </div>
                <div>
                                        <div id="welcome">
                        Olá  Gilberto Griesbach Junior                    </div>
                                    </div>
            </div>
            
            

        </form>        
        <!--/icons/identity.png-->
                    <label id="abre" for="check"><img src="/media/peoplepicture/thumbnail/753235.jpg?time=1504105683"></label>
                     
        <div id="menu">            
            <div class="scroller">
                <div class="scroller">
                    <a href="/core/contact/"><p>Contato</p><img src="/images/icons/mail_generic.png"/></a>
                    
                    <a onclick="signOut()"><p>Sair do sistema</p><img src="/images/icons/exit.png"/></a>
                    <a target="_BLANK" href="/permission/help/index/id/1783"><p>Ajuda</p><img src="/images/icons/help.png"/></a>
                </div>
            </div>
        </div>
    </div>
    <div id="loading"><div id="loadingText"> carregando... </div><div id="counter"></div></div>
    <style>

    </style>

</body>
        
<div id="quickMenu">
    <ul>
        <li>
            <a href="#" onclick="$('#menu-accordion-modal').dialog('open'); return false;">Menu de acesso</a>
        </li>
        <li>|</li>
        <li><a href="/user/home" title="Página inicial - Página inicial" class="tooltip">Página inicial</a></li>
        <li>|</li>
        <li>
            <a href="/user/login/select-system/">Trocar de país</a>
        </li>
        <li>|</li>
        <li>
            <a href="/mapas" target="_BLANK">Mapa</a>
        </li>  
                <li>|</li>
        <li>
            <a href="/chat/mediator/open-chat" target="_BLANK">Correios</a>
        </li>   
            </ul>
    
    
</div>
    </div>

    <div class="menu-mobile" style="padding: 0.5em; background-color: #30462b; display: none">
        <div class="open-menu" style="font-size:2em;cursor:pointer; color: white" onclick="$('#menu-accordion-modal').dialog('open'); return false;">☰ Menu </div>
    </div>

    <div id="enviroment" class="enviroment-development">
        <strong>ATENÇÃO:</strong>
        Utilizando o ambiente        <strong>Desenvolvimento</strong>.
        Saiba mais clicando        <a href="https://manual.pastoraldacrianca.org.br/pt_BR/enviroments" target="_BLANK">aqui</a>
    </div>

    
    <div id="format">
        <div id="formatContent">

                        
<div id="menu-accordion-modal" title="Menu de acesso">
                <h3><a href="#" style="color: #000">Atalho</a></h3>

        <div>
            <ul>
                                                        <li><a href="/indicator/extract/link-app-visita" title="Acompanhamento App Visita - " class="tooltip">Acompanhamento App Visita</a></a></li>
                                                        <li><a href="/indicator/extract/link-metas-cobertura-city" title="Metas e Coberturas Diocese - " class="tooltip">Metas e Coberturas Diocese</a></a></li>
                                                        <li><a href="/indicator/extract/link-metas-cobertura-sector" title="Metas e Coberturas / Estado - " class="tooltip">Metas e Coberturas / Estado</a></a></li>
                                                        <li><a href="/location/mandate-branch/edit" title="Alterar mandato de ramo - " class="tooltip">Alterar mandato de ramo</a></a></li>
                                                        <li><a href="/indicator/extract/index" title="Relatório de Indicadores - " class="tooltip">Relatório de Indicadores</a></a></li>
                                                        <li><a href="/form/types_fabsazul/monitor" title="Consultar Fabs digitadas - " class="tooltip">Consultar Fabs digitadas</a></a></li>
                                                        <li><a href="/indicator/extract/linkeemonth" title="Relatório estágio de evolução - " class="tooltip">Relatório estágio de evolução</a></a></li>
                                                        <li><a href="/indicator/extract/linkeequarter" title="Relatório estágio de evolução trimestre - " class="tooltip">Relatório estágio de evolução trimestre</a></a></li>
                                                        <li><a href="/indicator/extract/link58quarter" title="Planejamento de capacitação de líderes trimestre - " class="tooltip">Planejamento de capacitação de líderes trimestre</a></a></li>
                                                        <li><a href="/indicator/extract/link58month" title="Planejamento de capacitação de líderes - " class="tooltip">Planejamento de capacitação de líderes</a></a></li>
                                                        <li><a href="/form/types_capacitacao/monitor" title="Consultar pessoas capacitadas - " class="tooltip">Consultar pessoas capacitadas</a></a></li>
                                                        <li><a href="/entitynew/entity/filter" title="Consulta pessoa - " class="tooltip">Consulta pessoa</a></a></li>
                                                        <li><a href="/form/types_conciliacaosetor/monitor" title="Consultar Conciliação Bancaria de Setor - " class="tooltip">Consultar Conciliação Bancaria de Setor</a></a></li>
                                                        <li><a href="/indicator/extract/link-classifica-sector" title="Classificação dos Setores - " class="tooltip">Classificação dos Setores</a></a></li>
                                                        <li><a href="/location/mandate-sector/edit" title="Mandato de Setor  - " class="tooltip">Mandato de Setor </a></a></li>
                                                        <li><a href="/capacity/trainers-finder/index" title="Relatório de Capacitação - " class="tooltip">Relatório de Capacitação</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Acompanhamento Nutricional</a></h3>

        <div>
            <ul>
                                                        <li><a href="/bmi/index/index" title="Início - Início" class="tooltip">Início</a></a></li>
                                                        <li><a href="/report/bmi_child/analytical" title="Relatório Analítico - " class="tooltip">Relatório Analítico</a></a></li>
                                                        <li><a href="/core/rpc-call/index" title="Controle de sincronização - Controle de sincronização" class="tooltip">Controle de sincronização</a></a></li>
                                                        <li><a href="/form/types_acompnutric/monitor" title="Consultar Sincronização do AppNutri - " class="tooltip">Consultar Sincronização do AppNutri</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Aplicativo Visita Domiciliar</a></h3>

        <div>
            <ul>
                                                        <li><a href="/ead/questions-types/index" title="e-Capacitação - Tipos de Perguntas - " class="tooltip">e-Capacitação - Tipos de Perguntas</a></a></li>
                                                        <li><a href="/education/index/index" title="Inicio - Menu AppVisita  - " class="tooltip">Inicio - Menu AppVisita </a></a></li>
                                                        <li><a href="/education/test/question" title="Visualização de Material Educativo - " class="tooltip">Visualização de Material Educativo</a></a></li>
                                                        <li><a href="/bmi/analysischart/index" title="e-cartelas Acompanhamento nutricional - " class="tooltip">e-cartelas Acompanhamento nutricional</a></a></li>
                                                        <li><a href="/permission/transmission-rules/index" title="Regras de transmissão de comunicados - " class="tooltip">Regras de transmissão de comunicados</a></a></li>
                                                        <li><a href="/analytics/babel/index" title="Babel: léxico de fórmulas para e-cartelas - " class="tooltip">Babel: léxico de fórmulas para e-cartelas</a></a></li>
                                                        <li><a href="/education/report/index" title="Relatórios do aplicativo Visita Domiciliar - Relatório de dados de aplicativos da Pastoral da Criança" class="tooltip">Relatórios do aplicativo Visita Domiciliar</a></a></li>
                                                        <li><a href="/ead/index/index" title="e-Capacitação - " class="tooltip">e-Capacitação</a></a></li>
                                                        <li><a href="/bmi/pregnant/index" title="Relatório analítico gestantes - " class="tooltip">Relatório analítico gestantes</a></a></li>
                                                        <li><a href="/permission/resource-for-app/index" title="Permissões de aplicativos - " class="tooltip">Permissões de aplicativos</a></a></li>
                                                        <li><a href="/permission/role-for-app/index" title="Perfil de usuário de aplicatívos - " class="tooltip">Perfil de usuário de aplicatívos</a></a></li>
                                                        <li><a href="/user/social/index" title="Vinculação de usuário - " class="tooltip">Vinculação de usuário</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Localizações</a></h3>

        <div>
            <ul>
                                                        <li><a href="/location/location/menu" title="Localizações - Localizações" class="tooltip">Localizações</a></a></li>
                                                        <li><a href="/permission/location-by-user/index" title="Abrangência de localizações por usuário - " class="tooltip">Abrangência de localizações por usuário</a></a></li>
                                                        <li><a href="/location/type/index" title="Tipos de localizações - " class="tooltip">Tipos de localizações</a></a></li>
                                                        <li><a href="/location/search-lines/index" title="Configuração de linha de busca - " class="tooltip">Configuração de linha de busca</a></a></li>
                                                        <li><a href="/location/join/index" title="Junções (Comunidades / Ramos) - Junções (Comunidades / Ramos)" class="tooltip">Junções (Comunidades / Ramos)</a></a></li>
                                                        <li><a href="/permission/system/index" title="Configuração de Sistema (Países) - " class="tooltip">Configuração de Sistema (Países)</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Indicadores</a></h3>

        <div>
            <ul>
                                                        <li><a href="/indicator/group/index" title="Cadastro grupo de pesquisa - " class="tooltip">Cadastro grupo de pesquisa</a></a></li>
                                                        <li><a href="/snapshot/quarter-by-system/index" title="Liberar cálculo de trimestre - Liberar cálculo de trimestre" class="tooltip">Liberar cálculo de trimestre</a></a></li>
                                                        <li><a href="/indicator/uti/index" title="Classificação dos Setores - Classificação dos Setores" class="tooltip">Classificação dos Setores</a></a></li>
                                                        <li><a href="/report/indicators_community/populate" title="Popular indicadores de Estágios de Evolução - Popular indicadores de Estágios de Evolução" class="tooltip">Popular indicadores de Estágios de Evolução</a></a></li>
                                                        <li><a href="/indicator/indicator/index" title="Cadastro de indicadores - Cadastro de indicadores" class="tooltip">Cadastro de indicadores</a></a></li>
                                                        <li><a href="/indicator/report/index" title="Cadastro de relatórios de indicadores - Cadastro de formularios de indicadores" class="tooltip">Cadastro de relatórios de indicadores</a></a></li>
                                                        <li><a href="/indicator/constants/index" title="Constantes (Coalesce) - Constantes (Coalesce)" class="tooltip">Constantes (Coalesce)</a></a></li>
                                                        <li><a href="/snapshot/indicators_income/index" title="Relatório de Indicadores do IBGE - Relatório de Indicadores do IBGE" class="tooltip">Relatório de Indicadores do IBGE</a></a></li>
                                                        <li><a href="/report/indicators_income/populate" title="Geração de Indicadores do IBGE - Geração de Indicadores do IBGE" class="tooltip">Geração de Indicadores do IBGE</a></a></li>
                                                        <li><a href="/form/index/type" title="Relatório de FABS digitadas - " class="tooltip">Relatório de FABS digitadas</a></a></li>
                                                        <li><a href="/indicator/uti/full" title="Classificação dos Setores - uso interno - " class="tooltip">Classificação dos Setores - uso interno</a></a></li>
                                                        <li><a href="/indicator/report/index" title="Formular Relatório de Indicadores - " class="tooltip">Formular Relatório de Indicadores</a></a></li>
                                                        <li><a href="/indicator/extract/index" title="Relatório de indicadores - Relatório de indicadores" class="tooltip">Relatório de indicadores</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Entidades</a></h3>

        <div>
            <ul>
                                                        <li><a href="/entitynew/entity/index" title="Manutenção de Pessoa Física e Jurídica - " class="tooltip">Manutenção de Pessoa Física e Jurídica</a></a></li>
                                                        <li><a href="/entitynew/entity/filter" title="Novo cadastro de pessoas - " class="tooltip">Novo cadastro de pessoas</a></a></li>
                                                        <li><a href="/entitynew/automatic-join/index" title="Junção Automatica - " class="tooltip">Junção Automatica</a></a></li>
                                                        <li><a href="/entity/identity/index" title="Tipos de Identificação - " class="tooltip">Tipos de Identificação</a></a></li>
                                                        <li><a href="/entitynew/index/index" title="Contato com a base - Acompanhamento aos setores  - " class="tooltip">Contato com a base - Acompanhamento aos setores </a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Formulários (Fabs, Facs, Prest.Contas ...)</a></h3>

        <div>
            <ul>
                                                        <li><a href="/form/index/index" title="Início - " class="tooltip">Início</a></a></li>
                                                        <li><a href="/analytics/index/index" title="Gestão de Análises - " class="tooltip">Gestão de Análises</a></a></li>
                                                        <li><a href="/form/field/index" title="Gestão de Campos - Gestão de Campos" class="tooltip">Gestão de Campos</a></a></li>
                                                        <li><a href="/analytics/message/index" title="Mensagens - Mensagens associadas à análises e às perguntas do Caderno do Líder Eletrônico" class="tooltip">Mensagens</a></a></li>
                                                        <li><a href="/form/form-type/index" title="Tipos de Formulários - Tipos de Formulários" class="tooltip">Tipos de Formulários</a></a></li>
                                                        <li><a href="/form/category/index" title="Categorias - Categorias: tipos de agrupamentos. Por exemplo: causas de mortes, tipos de gastos etc." class="tooltip">Categorias</a></a></li>
                                                        <li><a href="/form/cause/index" title="Causas - Causas: variáveis associadas a um grupo. Por exemplo: no grupo de causas de mortes->sarampo." class="tooltip">Causas</a></a></li>
                                                        <li><a href="/filesbrowser/index/home" title="Upload de Arquivos - " class="tooltip">Upload de Arquivos</a></a></li>
                                                        <li><a href="/analytics/message-type/index" title="Tipos de Mensagens - " class="tooltip">Tipos de Mensagens</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Configuração de Sistema</a></h3>

        <div>
            <ul>
                                                        <li><a href="/filesbrowser/index/home" title="Gerenciador de Arquivos - Gerenciador de Arquivos" class="tooltip">Gerenciador de Arquivos</a></a></li>
                                                        <li><a href="/education/i18n/index" title="Internacionalização - " class="tooltip">Internacionalização</a></a></li>
                                                        <li><a href="/core/cron/index" title="Cron  - " class="tooltip">Cron </a></a></li>
                                                        <li><a href="/culture/index/index" title="Idiomas - Idiomas" class="tooltip">Idiomas</a></a></li>
                                                        <li><a href="/permission/menu-group/index" title="Menu Grupos - " class="tooltip">Menu Grupos</a></a></li>
                                                        <li><a href="/permission/menu-item/index" title="Menu Item - Menu Item" class="tooltip">Menu Item</a></a></li>
                                                        <li><a href="/user/index/index" title="Gestão de usuários - Gestão de usuários" class="tooltip">Gestão de usuários</a></a></li>
                                                        <li><a href="/permission/role/index" title="Gestão de Perfis - Gestão de Perfis" class="tooltip">Gestão de Perfis</a></a></li>
                                                        <li><a href="/core/cron/running" title="Processos em execução pelo CRON - " class="tooltip">Processos em execução pelo CRON</a></a></li>
                                                        <li><a href="/permission/user-audit-form-types/index" title="Permissões de auditoria de formulários - " class="tooltip">Permissões de auditoria de formulários</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Requisições</a></h3>

        <div>
            <ul>
                                                        <li><a href="/request/request/list" title="Minhas requisições - " class="tooltip">Minhas requisições</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Campanhas</a></h3>

        <div>
            <ul>
                                                        <li><a href="/financial/extract/index" title="Relatórios - " class="tooltip">Relatórios</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Geoprocessamento</a></h3>

        <div>
            <ul>
                                                        <li><a href="/location/parish/import-shapefile" title="Importar Shapes de Paróquias - Importar Shapes de Paróquias" class="tooltip">Importar Shapes de Paróquias</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Capacitação</a></h3>

        <div>
            <ul>
                                                        <li><a href="/form/types_capacitacao/monitor" title="Consultar pessoas capacitadas - " class="tooltip">Consultar pessoas capacitadas</a></a></li>
                                                        <li><a href="/capacity/report/index" title="Relatório Termo de compromisso (Analítico e Sintético) - " class="tooltip">Relatório Termo de compromisso (Analítico e Sintético)</a></a></li>
                                                        <li><a href="/capacity/appositer/index" title="Capacitação - Quem capacita quem - " class="tooltip">Capacitação - Quem capacita quem</a></a></li>
                                                        <li><a href="/capacity/step/index" title="Capacitação - Manutenção de Etapas - " class="tooltip">Capacitação - Manutenção de Etapas</a></a></li>
                                                        <li><a href="/capacity/theme/index" title="Capacitação - Manutenção de Temas - " class="tooltip">Capacitação - Manutenção de Temas</a></a></li>
                                                        <li><a href="/capacity/theme/index" title="Capacitação - Associar temas similares - " class="tooltip">Capacitação - Associar temas similares</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Certificados</a></h3>

        <div>
            <ul>
                                                        <li><a href="/capacity/extract/index" title="Certificados - " class="tooltip">Certificados</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Financeiro</a></h3>

        <div>
            <ul>
                                                        <li><a href="/financial/balance/index" title="Cadastro de Saldo Bancário - Cadastro de Saldo Bancário" class="tooltip">Cadastro de Saldo Bancário</a></a></li>
                                                        <li><a href="/financial/converttxt/index" title="Conversor de arquivo E.Elétrica - " class="tooltip">Conversor de arquivo E.Elétrica</a></a></li>
                                                        <li><a href="/financial/extract/coordination-balance" title="Relatório saldo bancário coordenações  - " class="tooltip">Relatório saldo bancário coordenações </a></a></li>
                                                        <li><a href="/financial/report/repasse" title="Relatório de repasse (Dioceses X Paróquias) - " class="tooltip">Relatório de repasse (Dioceses X Paróquias)</a></a></li>
                                                        <li><a href="/financial/extract/index" title="Relatório Cia de Luz - " class="tooltip">Relatório Cia de Luz</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Museu da Vida</a></h3>

        <div>
            <ul>
                                                        <li><a href="/pergamum/item/index" title="Cadastrar item do Museu da Vida - " class="tooltip">Cadastrar item do Museu da Vida</a></a></li>
                                                        <li><a href="/pergamum/fields/index" title="Museu da Vida - Campos - " class="tooltip">Museu da Vida - Campos</a></a></li>
                                                        <li><a href="/museum/guestbook/open" title="Museu da Vida - Visitas - " class="tooltip">Museu da Vida - Visitas</a></a></li>
                                                        <li><a href="/pergamum/field-values/index" title="Museu da Vida - Conteúdo - " class="tooltip">Museu da Vida - Conteúdo</a></a></li>
                                                        <li><a href="/permission/control-officials/index" title="Catraca - " class="tooltip">Catraca</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Serviços</a></h3>

        <div>
            <ul>
                                                        <li><a href="/services/myservices/index" title="Enviar Serviços - " class="tooltip">Enviar Serviços</a></a></li>
                                                        <li><a href="/services/services/index" title="Cadastrar Serviços - " class="tooltip">Cadastrar Serviços</a></a></li>
                                                        <li><a href="/services/networks/index" title="Gestão das Redes - " class="tooltip">Gestão das Redes</a></a></li>
                                                        <li><a href="/services/free-queries/index" title="Consulta livre (Query) - " class="tooltip">Consulta livre (Query)</a></a></li>
                                                        <li><a href="/services/artifacts/index" title="Cadastro de Artefatos - " class="tooltip">Cadastro de Artefatos</a></a></li>
                                                        <li><a href="/services/types-artifacts/index" title="Cadastro de Tipos de Artefatos - " class="tooltip">Cadastro de Tipos de Artefatos</a></a></li>
                                                        <li><a href="/core/cron/running" title="Processos em execução pelo CRON - " class="tooltip">Processos em execução pelo CRON</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Eventos</a></h3>

        <div>
            <ul>
                                                        <li><a href="/event/type/index" title="Eventos da Pastoral - " class="tooltip">Eventos da Pastoral</a></a></li>
                                                        <li><a href="/event/class/index" title="Classes de Eventos - " class="tooltip">Classes de Eventos</a></a></li>
                                                        <li><a href="/event/type/index" title="Tipos de Eventos - " class="tooltip">Tipos de Eventos</a></a></li>
                            </ul>
        </div>
            <h3><a href="#" style="color: #000">Preferências do Usuário</a></h3>

        <div>
            <ul>
                                                        <li><a href="/user/index/edit-preferences" title="Alterar - " class="tooltip">Alterar</a></a></li>
                            </ul>
        </div>
        </div>

<script type="text/javascript">
    $( "#menu-accordion-modal").accordion({heightStyle: "content"});
    $( "#menu-accordion-modal").dialog({
        "modal"     : true,
        "autoOpen"  : false,
        "resizable" : false,
        "width"     : 400,
        "height"    : 500,
    });
</script>
