var json = null;

var vport = ':' + window.location.port;
if(vport==':80' || vport == ':'){
    vport='';
}

var artifacts = {
    
    baseUrl     : window.location.protocol + '//' + window.location.hostname + vport,
    defaultLang : 'pt_BR',
    
    getList: function(callback, types, filtertype){

        let getTypes = (types !== undefined)? '&types=' + types : '';
        let url = this.baseUrl + '/rest/index/model/services.get_artifacts?params=[0]&lang='+ this.defaultLang + getTypes + '&limit=false&filtertype=' + filtertype;

        if(filtertype=='undefined' || filtertype==undefined){
            filtertype='';
        }

        $.post(url,{}, function(artifacts){

            callback(artifacts);
        }, 'json');
    },
    
    getImagesList: function(callback){
        
        this.getList(callback, 2);
    },
    
    arrayToTable: function(artifacts, input, deleteButton = false) {

        let self        = this;
        let table       = $(document.createElement('table')).addClass('artifactList').attr('id', 'artifactList');
        let trSearch    = $(document.createElement('tr'));
        let tdSearch    = $(document.createElement('th'));
        let inputSearch = $(document.createElement('input'))
                            .attr("placeholder", 'busca')
                            .attr("type", 'search')
                            .attr('tabindex', 0)
                            .attr('id','searchArtifact');
                    
            inputSearch.on("keyup", function(e) {
                
                let code = e.keyCode;
                
                if(code === 40) {
                    $(this).parent().parent().next().children('td')[0].focus();
                }
            });
        
            inputSearch.on("keyup search", function() {
                
                let valSearch = self.normalizeString($(this).val());
                let list      = $("#artifactList td");
                
                $.each(list, function(key, value) {
                    
                    let td = $(value);
                    let valNorm = self.normalizeString(td.text());
                    let check   = valNorm.includes(valSearch);
                    
                    if(check){
                        td.parent().show();
                    }else{
                        td.parent().hide();
                    }
                });
            });
        
        tdSearch.append(inputSearch);
        trSearch.append(tdSearch);
        table.append(trSearch);
        
        $.each(artifacts, function(key, artifact) {

            let tr       = $(document.createElement('tr'));
            let name     = (artifact.text != '')? artifact.text : artifact.name;
            let id_input = input.attr('data-input_id');
            let input_id = $("#" + id_input);
                    
                tr.on("click", function() {

                    input.val(name);
                    input_id.val(artifact.id);
                    self.showSmall(artifact, input, deleteButton);
                    popUp.fecha();
                });

            let tdName = $(document.createElement('td'));
                tdName.append(name);
                tdName.attr('tabindex', 0);
                tdName.on("keyup", function(e){
                    
                    let code = e.keyCode;

                    switch(code) {
                        case 13 :
                            
                                input.val(name);
                                input_id.val(artifact.id);
                                self.showSmall(artifact, input, deleteButton);
                                popUp.fecha();
                            break;
                        case 38:
                            
                            let prev = $(this).parent().prev().children('td')[0];
                            if(prev !== undefined) {
                                prev.focus();
                            }
                            break;
                        case 40:
                            
                            let next = $(this).parent().next().children('td')[0];
                            if(next !== undefined) {
                                next.focus();
                            }
                            break;
                    }
                });
                
                tr.append(tdName);

                table.append(tr);
        });

        return table;
    },
    
    getArtifact: function(id, lang, callback) {
        
        let url = this.baseUrl + '/rest/index/model/services.get_artifact';
        let langPost = (lang !== undefined && lang !== null)? lang : this.defaultLang;

        let input = $(this);
        let filtertype = input.data('filtertype');

        $.post(url, {lang: langPost, id: id, filtertype: this.filtertype}, function(data) {
            
            callback(data);
        },'json');
    },
    
    checkImage: function(url) {
        
        return(url.match(/\.(jpeg|jpg|gif|png|svg)$/i) != null);
    },

    checkVideo: function(url) {
        
        return(url.match(/\.(ogg|ogv|mp4|webm)$/i) != null);
    },

    checkAudio: function(url) {
        
        return(url.match(/\.(aac|mp3|wav)$/i) != null);
    },
    
    showSmall: function(artifact, element, deleteButton) {
        
        let self = this;
        if(artifact.file !== undefined) {
            
            var path      = artifact.path;
            var code      = artifact.file.code;
            var mime      = artifact.mime_type;
            var size      = artifact.file.parsedSize;
            var url       = artifact.file.url;
            
            var isImage   = this.checkImage(url);
            var isVideo   = this.checkVideo(url);
            var isAudio   = this.checkAudio(url);
            var epochTime = new Date().getTime();
            
            var oldOne = $("#" + element.data('show_small')); console.log(oldOne)
            
            if(element.length > 0 && element.attr('type') === 'text') {

                let tName = (artifact.text != undefined && artifact.text != '')? artifact.text : artifact.name;
                element.val(tName);
                var elementId = $("#" + element.data('input_id'));
            }

            if(path !== undefined && path !== ''){
                
                let fullUrl = url + '?t=' + epochTime;

                $.get(fullUrl).done(function() {
                       
                    if(oldOne.length > 0){

                        oldOne.remove();
                    }
                    
                    var div = $(document.createElement('div'));
                    var showSmallId = 'artifact_showSmall_' + artifact.id + '_' + code;
                        div.attr('id', showSmallId);
                        element.data('show_small', showSmallId);
                        div.addClass('container');
                    var icons = $(document.createElement('div'));
                        icons.addClass('icons');
                        console.log(deleteButton);
                        
                    if(deleteButton !== false) {
                        
                        var del = $(document.createElement('span'));
                        del.addClass('ico');
                        del.addClass('delete');
                        del.attr('data-code', code);
                        del.on("click", function(){
                            
                            let showSmall = $(this).parent().parent();
                            
                            switch(deleteButton) {
                                
                                case 'file'      :
                                    
                                        self.deleteArtifactFilePopUp(artifact.id ,code, showSmall, path);
                                    break;
                                case 'clearField': 
                                    
                                        showSmall.remove();
                                        element.val('');
                                        elementId.val('');
                                    break;
                            }
                        });
                        
                        icons.append(del);
                    }
 
                    if(isImage) {

                        var content = $(document.createElement('img'));
                            content.attr('src', fullUrl);
                    }
                    if(isVideo) {

                        var content = $(document.createElement('video'));
                            content.attr('controls', true);
                        var source  = $(document.createElement('source')); 
                            source.attr('src', fullUrl);
                            source.attr('type', mime);

                            content.append(source);
                    }
                    if(isAudio) {

                        var content = $(document.createElement('audio'));
                            content.attr('controls', true);
                        var source  = $(document.createElement('source')); 
                            source.attr('src', fullUrl);
                            source.attr('type', mime);

                            content.append(source);
                    }
                    
                    var pSize = $(document.createElement('p'));
                        pSize.text(size);
                    var pMime = $(document.createElement('p'));
                        pMime.text(mime);

                    div.append(icons).append(content).append(pSize).append(pMime);

                    element.after(div);
                });
            }
        }
    },
    
    deleteArtifactFilePopUp: function(id, cultureCode, showSmall, path){

        let self = this;
        let html = 'Tem certeza que deseja apagar este arquivo?<br>Essa ação não poderá ser revertida!';
        popUp.abre("delArtifactPop",
                "Apagar Arquivo",
                html,
                "CANCELAR",
                "APAGAR",
                "confirm", "p", "vermelho", "bola",
                "",
                function () {
                    popUp.fecha();
                    self.deleteArtifactFile(id, cultureCode, showSmall, path);
                },
                ''
                );
    },
    
    deleteArtifactFile: function(id, cultureCode, showAll, path){
        //let url = this.baseUrl + '/rest/index/model/services.deleteFile';

        var toDelete = document.getElementById('to_delete');
        if(toDelete.value == null || toDelete.value == ""){
            toDelete.value = "{}";
        }
        json = JSON.parse(toDelete.value);
        
        if(!!!json[id]){
            json[id] = {};
        }
        json[id][cultureCode] = path;

        toDelete.value = JSON.stringify(json);
        showAll.remove();    
        return;

        /*
        $.post(url, {'code': cultureCode, 'id': id}, function(data){

            if(data === true){
                if(showAll !== undefined) {
                    
                    showAll.remove();    
                }
            }
        },'json');
        */
    },
    
    normalizeString: function(str) {
        
        let newStr = str.replace(
          /([àáâãäå])|([ç])|([èéêë])|([ìíîï])|([ñ])|([òóôõöø])|([ß])|([ùúûü])|([ÿ])|([æ])/g, 
          function (str, a, c, e, i, n, o, s, u, y, ae) {
            if (a) return 'a';
            if (c) return 'c';
            if (e) return 'e';
            if (i) return 'i';
            if (n) return 'n';
            if (o) return 'o';
            if (s) return 's';
            if (u) return 'u';
            if (y) return 'y';
            if (ae) return 'ae';
          }
        );
        
        return newStr.toLowerCase();
    },
    
    getAvailableLanguages: function(callback) {
        var uri = '/rest/index/model/services.get_available_languages?params=';
        obj = {};
        uri += JSON.stringify(obj);
        
        var self = this;
        
        $.getJSON(uri, function(data){
            console.log(data); //Debug do resultado
           
            
        }).fail(function(err){
            console.log( "error: ", err );
        });
    }
};

$("body").ready(function() {

    $('#to_delete').val('');

    $('input[type=file]').change(function(){
        if (json != null) {
            var field_id = $(this).attr('id');
            field_id = field_id.replace('file_', '');
        
            var key = Object.keys(json);

            delete json[key][field_id];
            $('#to_delete').val(JSON.stringify(json));
        }
    });
    
    $('.input_artifact_id').each(function() {
        
        let input            = $(this);
        let inputName        = $("#" + $(this).data('input_name')); 
        let id               = input.val();
        let dataDeleteButton = inputName.data('deletebutton');
        let deleteButton     = (dataDeleteButton == '' || dataDeleteButton == undefined)? false : dataDeleteButton;
        
        if(id != '') {
            
            artifacts.getAvailableLanguages();
            
            
            artifacts.getArtifact(id, null, function(artifact) {
                
                artifacts.showSmall(artifact, inputName, deleteButton);
            });
        }
    });

    $('.limpar-artefatos').click(function() {

        let valueField     = $("#"+ $(this).data('value-field'));
        let labelField     = $("#"+ $(this).data('label-field'));

        valueField.val('');
        labelField.val('');

        console.log(valueField);
        console.log(labelField);
    });
    
    $('body').delegate('.input_artifact_path','click', function() {
        
        let input = $(this);
        input.hide();
        let dataDeleteButton = input.data('deletebutton');
        let deleteButton     = (dataDeleteButton == '' || dataDeleteButton == undefined)? false : dataDeleteButton;
        
        let filtertype = input.data('filtertype');
        
        artifacts.getList(function(artifactsArray) {
            
            let list = artifacts.arrayToTable(artifactsArray, input, deleteButton);
            
            var html = list;
            popUp.abre( "ArtifactList",
                        "artefatos",
                        html,
                        "ok",
                        "",
                        "message","400px-400px", "branco", "bola",
                        "","",
                        function(){
                            input.show();
                            setTimeout(function(){
                                $("#searchArtifact").focus();
                            },1000);
                        }
            );
        }, filtertype, filtertype);
    });
    
    $('body').delegate('.input_artifact_image_path','click', function() {
        
        let input = $(this);
        let dataDeleteButton = input.data('deletebutton');
        let deleteButton     = (dataDeleteButton == '' || dataDeleteButton == undefined)? false : dataDeleteButton;
                
        artifacts.getImagesList(function(artifactsArray) {
            
            let list = artifacts.arrayToTable(artifactsArray, input, deleteButton);
            
            var html = list;
            popUp.abre( "ArtifactList",
                        "artefatos",
                        html,
                        "ok",
                        "",
                        "message","400px-400px", "branco", "bola",
                        "","",
                        function(){
                            
                            setTimeout(function(){
                                $("#searchArtifact").focus();
                            },1000);
                        }
            );
        });
    });
});

