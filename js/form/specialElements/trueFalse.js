/* 
    Created on : 21/09/2018, 10:16:57
    Author     : Paulo Navarro
*/

var inputTrueFalse = {
    
    render: function(baseElement){
        
        var div = $(document.createElement('div')).addClass('inputTrueFalse');
        var val = baseElement.val();
       
        this.addFalse(baseElement, div);
        this.addTrue (baseElement, div);

        baseElement.prop('type', 'hidden');
        baseElement.after(div);
    },
    
    addFalse: function(baseElement, element){
       
       var self = this;
       var wrapper = $(document.createElement('div'));
        
        wrapper.on('click', function(){
            
            baseElement.val(0);
            element.removeClass('markTrue');
            element.addClass('markFalse');
            self.mark(wrapper, 0);
        });
        
        var optionFalse = this.generateSvgFalse();
        
        wrapper.append(optionFalse);
        
        element.append(wrapper);
        
        if(baseElement.val() === '0'){
            self.mark(wrapper, 0);
            element.addClass('markFalse');
        }
    },
    
    addTrue: function(baseElement, element){
       
       var self = this;
       var wrapper = $(document.createElement('div'));
        
        wrapper.on('click', function(){
            
            baseElement.val(1);
            element.removeClass('markFalse');
            element.addClass('markTrue');
            self.mark(wrapper, 1);
        });
        
        var optionTrue = this.generateSvgTrue();
    	
    	wrapper.append(optionTrue);
        
        element.append(wrapper);
        
        if(baseElement.val() === '1'){
            self.mark(wrapper, 1);
            element.addClass('markTrue');
        }
    },
    generateSvgFalse: function(){
        var xmlns = "http://www.w3.org/2000/svg";

        var svg = document.createElementNS (xmlns, "svg");
  
        svg.setAttributeNS(null,'class', 'optionFalse');

        svg.setAttributeNS(null,'viewBox','0 0 40 40');
        
        var g     = document.createElementNS('http://www.w3.org/2000/svg',"g");
            g.setAttributeNS(null, 'transform', 'translate(0,-1012.3622)');
        var path1 = document.createElementNS('http://www.w3.org/2000/svg',"path");
        var d1    = "m 20,1012.3622 a 20.000288,20.000288 0 0 0 -20,20 20.000288,20.000288 0 0 0 20,20 20.000288,20.000288 0 0 0 20,-20 20.000288,20.000288 0 0 0 -20,-20 z m 0,6.0915 a 13.907719,13.907719 0 0 1 6.36832,1.5557 L 7.641176,1038.7366 A 13.907719,13.907719 0 0 1 6.0915245,1032.3622 13.907719,13.907719 0 0 1 20,1018.4537 Z m 12.358824,7.5341 a 13.907719,13.907719 0 0 1 1.549651,6.3744 A 13.907719,13.907719 0 0 1 20,1046.2707 a 13.907719,13.907719 0 0 1 -6.36832,-1.5558 l 18.727144,-18.7271 z";
        path1.setAttributeNS(null, "d", d1);
        path1.setAttributeNS(null, "x", 0);
        path1.setAttributeNS(null, "y", 0);

    	g.appendChild(path1);

    	svg.appendChild(g);
    	
    	return $(svg);
    },
    generateSvgTrue: function(){
        var xmlns = "http://www.w3.org/2000/svg";

        var svg = document.createElementNS (xmlns, "svg");
  
        svg.setAttributeNS(null,'class', 'optionTrue');
        svg.setAttributeNS(null,'x','0px');
        svg.setAttributeNS(null,'y','0px');
        svg.setAttributeNS(null,'viewBox','0 0 40 40');
        
        var g     = document.createElementNS('http://www.w3.org/2000/svg',"g");
            g.setAttributeNS(null, 'transform', 'translate(0,-1012.3622)');
        var path1 = document.createElementNS('http://www.w3.org/2000/svg',"path");
        var d1    = "M 20,0.66601562 A 19.334263,19.334263 0 0 0 0.66601562,20 19.334263,19.334263 0 0 0 20,39.333984 19.334263,19.334263 0 0 0 39.333984,20 19.334263,19.334263 0 0 0 38.53125,14.525391 c -1.701575,1.712643 -3.419962,3.440827 -5.097656,5.134765 A 13.444581,13.444581 0 0 1 33.445312,20 13.444581,13.444581 0 0 1 20,33.445312 13.444581,13.444581 0 0 1 6.5546875,20 13.444581,13.444581 0 0 1 20,6.5546875 13.444581,13.444581 0 0 1 25.984375,7.9765625 L 30.308594,3.6523438 A 19.334263,19.334263 0 0 0 20,0.66601562 Z";
        
        path1.setAttributeNS(null, "d", d1);
        path1.setAttributeNS(null, "x", 0);
        path1.setAttributeNS(null, "y", 0);
        path1.setAttributeNS(null, 'transform','translate(0,1012.3622)')

        var path2 = document.createElementNS('http://www.w3.org/2000/svg',"path");
            path2.setAttributeNS(null, "class", 'optionV');
            path2.setAttributeNS(null, "x", 0);
            path2.setAttributeNS(null, "y", 0);
            
        var d2    = "m 30.296961,1019.3305 -10.179688,10.1797 -3.4375,-3.4375 -6.091797,6.0918 9.441407,9.4414 c 5.429039,-5.4887 10.583456,-10.721 16.201171,-16.3398 z";
        path2.setAttributeNS(null, "d", d2);
    	
    	g.appendChild(path1);
    	g.appendChild(path2);
    	
    	svg.appendChild(g);
    	
    	return $(svg);
    },
    
    mark: function(element, number) {
        
        var current = $(element);
        
        var father  = current.parent();
        
        father.children().each(function() {
            
            var item = $(this);
                item.removeClass('mark');
        });
        
        current.addClass('mark');
    }
};

$("body").ready(function(){
    
    $("input[type=true-false]").each(function(){
    
        inputTrueFalse.render($(this));
    });
});