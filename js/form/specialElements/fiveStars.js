


var input5Star = {
    render: function (baseElement) {
        var div = $(document.createElement('div')).addClass('input5Star');
        var val = baseElement.val();
        for (var i = 1; i <= 5; ++i) {
            var mark = (val == i);
            this.addStar(baseElement, div, i, mark);
        }

        baseElement.prop('type', 'hidden');
        baseElement.after(div);
    },
    addStar: function (baseElement, element, number, mark) {

        var star = this.generateSvgStar();

        var self = this;
        star.on("click", function () {
            self.mark(this);
            baseElement.val(number);
        });


        star.attr('data-number', number);
        element.append(star);

        if (mark) {
            this.mark(star);
        }


    },
    generateSvgStar: function () {
        var xmlns = "http://www.w3.org/2000/svg";

        var svg = document.createElementNS(xmlns, "svg");

        svg.setAttributeNS(null, 'class', 'star');
        svg.setAttributeNS(null, 'x', '0px');
        svg.setAttributeNS(null, 'y', '0px');
        svg.setAttributeNS(null, 'viewBox', '0 0 47.94 47.94');

        var path = document.createElementNS('http://www.w3.org/2000/svg', "path");
        var pathAttr = "M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757\n c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042 \n c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685 \n c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528 \n c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956 \n C22.602,0.567,25.338,0.567,26.285,2.486z";
        path.setAttributeNS(null, "d", pathAttr);

        svg.appendChild(path);

        return $(svg);
    },
    mark: function (element) {

        var current = $(element);
        var currentNumber = parseInt(current.attr('data-number'));

        var father = current.parent();
        
        father.children().each(function () {
            var star = $(this);
            star.attr('class','star');

        });
        
        setTimeout(function () {
            
            father.children().each(function () {
                
                var star   = $(this);
                var number = parseInt(star.attr('data-number'));

                if (number <= currentNumber) {
                    
                    star.attr('class','star mark markColor');
                } else {
                    
                    star.attr('class','star');
                    //star.removeClass('markColor');
                }
            });
        }, 1);


    }
};

$("body").ready(function () {
    $("input[type=fiveStars]").each(function () {
        input5Star.render($(this));
    });

});