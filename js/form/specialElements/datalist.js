
        var dataListFactory = {
            
            getDataList : function(id) {
                
                var datalist = {
                    
                    id_el: null,
                    element: null,
                    datalist: null,
                    tag: null,
                    loadValues : function(id){

                        var self = this;
                        this.id_el = id;
                        this.element = $(`#${id}`);
                        
                        this.element.on('change', function(){
                            
                            self.updateTag();
                        });
                        this.datalist = $(`#data_${id}`);
                        this.tag = $(`#tag_${id}`);
                    },
                    updateTag: function(){
                        
                        let current_val = this.element.val();
                        var self = this;
                        
                        if(current_val !== '') {
                            
                            let correct = false;
                            $.each(this.datalist.children('option'), function(key, option) {

                                let o_val = $(option).val();
                                if(o_val === current_val){
                                    
                                    self.tag.html($(option).html());
                                    correct = true;
                                    return;
                                }
                            });
                            
                            if(!correct) {
                                
                                this.tag.html('');
                                this.element.val('');
                            }
                        }
                    }
                };
                
                datalist.loadValues(id);
                datalist.updateTag();
                return datalist;
            }
        };
