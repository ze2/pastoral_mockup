
var inputJSON = {
    
    createLabel : function(text) {
        
        var label = $(document.createElement('label'));
            label.text(text);
            
        return label;
    },
    
    createInput : function(id, input) {

        switch(input.type) {
            
            case 'select':
                
                    return this.createInputSelect(id, input);
                break;
            case 'text':
                
                    return this.createInputElement(id, input, 'text');
                break;
            case 'number':
                
                    return this.createInputElement(id, input, 'number');
                break;
        }
    },
    
    createInputElement: function(id, input, type) {
        
        var element = $(document.createElement('input'));
            element.attr('type', type);
            element.attr('id', id);
            element.attr('data-return', input.return);
            
            if(input.placeholder !== undefined){
                element.attr('placeholder',input.placeholder);
            }
            
            if(input.default !== undefined){
                element.val(input.default);
            }
            
        return element;
    },
    
    createInputSelect: function(id, input) {
        
        var select = $(document.createElement('select'));
            select.attr('id', id);
            select.attr('data-return', input.return);
        
        $.each(input.options, function(key, element) {
            
            var option = $(document.createElement('option'));
                option.text(element.text);
                option.val(element.value);
                
                if(element.value === element.default) {
                    
                    option.attr('selected', 'selected');
                }
                
                select.append(option);
        });
            
        return select;
    },
    createAddButton: function(name, callback) {
        
        var button = $(document.createElement('span'));
            button.text(name);
            button.on("click",function() { callback(); });
        return button;
    },
    render: function(target, object) {
        
        var self = this;
        if(object.constructor === Array) {
            
            $.each(object, function(key, obj) {
                
                self.renderObject(target, obj);
            });
        } else {
            
            this.renderObject(target, object);
        }
    },
    renderObject: function(target, object) {
        
        var container     = $(document.createElement('div'));
        var list          = object.list;
        let jsonName      = object.name;
        var self          = this;
        var targetButtons = $("#" + target.attr('id') + '_buttons');
        
        switch(object.form_type) {
            
            case 'object' : 
                
                    $.each(list, function(paramName, input) {

                        container.append(inputJSON.createLabel(paramName));
                        container.append(inputJSON.createInput(paramName, input));
                    });
                break;
            case 'single_value': 
                
                        container.append(inputJSON.createLabel(object.name));
                        container.append(inputJSON.createInput(object.name, object));
                break;
        }
        
        var button = this.createAddButton(object.name, function() {
            
            let popId = 'jsonInput_' + object.name;
            popUp.abre(
                popId,
                object.name,
                container,
                "cancela",
                "confirma",
                "confirm","400px-300px", "branco", "bola", 
                "",
                function(){
                    
                    self.generateJson(target, jsonName, container, object.form_type);
                    popUp.fecha(popId);
                },
                ""
            );
            
        });
        
        
        targetButtons.append(button);
    },
    inputUpdateGraph: function(element){
        
        var targetGraph = $("#" + element.attr('id') + '_graph');
        
        if(element.val() !== ''){
            try {
               let json        = JSON.parse(element.val());
                
                targetGraph.html(inputJSON.jsonToHtml(json, true, element));
                targetGraph.removeClass('error');

            } catch(e) {
                targetGraph.addClass('error');
            }
        }
    },
    
    generateJson: function(target, jsonName, container, form_type) {
                
        let targetVal   = target.val().trim();
        var targetGraph = $("#" + target.attr('id') + '_graph');

        if(targetVal != '') {

            var json = JSON.parse(target.val());
        } else {

            var json      = {};
            json[jsonName] = [];
        }

        var newObj = {};

        container.find('select, input').each(function(key, input){

            var element = $(input);
            var ret     = element.attr('data-return');
            var preVal  = element.val();
            
            switch(ret) {

                case 'text' :
                        var val = preVal;
                    break;
                case 'number':
                        var val = parseInt(preVal);
                    break;
                case 'json' :
                        var val = JSON.parse(preVal);
                    break;
                case 'jsonArray' :
                        var val = JSON.parse('[' + preVal + ']');
                    break;
                default: var val = preVal;
            }

            switch(form_type) {
                
                case 'object' : 
                    
                    newObj[element.attr('id')] = val;
                    break;
                case 'single_value':
                    
                    newObj = val;
                    break;
            }
            
        });

        if(json[jsonName] === undefined) {

            json[jsonName] = [];    
        }
        
        switch(form_type) {

            case 'object' : 

                json[jsonName].push(newObj);
                break;
            case 'single_value':

                json[jsonName]= newObj;
                break;
        }
        

        var jsonOutput = JSON.realStringify(json, null, 2);

        targetGraph.html(this.jsonToHtml(json),true, target);
        target.val(jsonOutput);
    },
    
    jsonToHtml: function(json, showDelete, target) {
        
        let jsonHtml = $(document.createElement('div'));
            jsonHtml.addClass('jsonWrapper');
        let self     = this;
        
        $.each(json, function(key, value) {
            
            let wrapper = $(document.createElement('div'));
            let keyDiv = $(document.createElement('div'));
                keyDiv.text(key);
                keyDiv.addClass('jsonKey');
            
            let valDiv = $(document.createElement('div'));
                valDiv.addClass('jsonVal');
                
            let type = typeof(value);

            switch(type) {
   
                case 'object' :
                    
                    if(Array.isArray(value)) {
                        
                                                
                        if(showDelete !== false) {
                            
                            let keyWrapper = $(document.createElement('div'));
                            let del = $(document.createElement('span'));
                                del.addClass('ico delete');

                                del.on('click', function() {

                                    let graph   = $(this).closest( ".jsonGraph" );
                                    let json_id = graph.attr('data-parent_id');
                                    let json_field = $("#" + json_id);
                                    let html = 'Tem certeza que deseja apagar<br> esse trecho do campo json?';
                                    popUp.abre(
                                        'deleteNode',
                                        'Apagar trecho',
                                         html,
                                        "cancela",
                                        "confirma",
                                        "confirm","p", "vermelho", "bola", 
                                        "",
                                        function(){

                                            self.deleleJsonNode(json_field, key, value);
                                            popUp.fecha('deleteNode');
                                        },
                                        ""
                                    );
                                    
                                });
                            keyWrapper.append(del);
                            keyWrapper.append(key);
                            jsonHtml.append(keyWrapper);
                        }
                    }
                    
                    jsonHtml.append(self.jsonToHtml(value, false, target)); 
                break;
                
            default:
                
                    valDiv.append(value);
                    wrapper.append(keyDiv);
                    wrapper.append(valDiv);
                    jsonHtml.append(wrapper);
                break;
            }
        });
        
        return jsonHtml;
    },
    
    deleleJsonNode : function(input, key, value){
        
        let json    = JSON.parse(input.val());
        let newJson = this.removeNodeJson(json, key, value);
        
        var jsonOutput = JSON.realStringify(newJson, null, 2);
        input.val(jsonOutput);
        
        var targetGraph = $("#" + input.attr('id') + '_graph');
            targetGraph.html(inputJSON.jsonToHtml(json, true, input));
    },
    removeNodeJson : function(json, key, value){
        
        delete json[key];
                
        return json;
    }
};

var field_params = {};

//var example = {
//    name: 'validations',
//    list: {
//        validator: {
//                    'type'   : 'select',
//                    'options': [{'text': 'Data igual ou menor','value':'DateAtOrBelow'},
//                                {'text': 'Data igual ou maior','value':'DateAtOrOver'},
//                                {'text': 'Não vazio',          'value':'NotEmpty'}],
//                    'default': 'DateAtOrBelow',
//                    'return' : 'string'
//                    },
//        params   : {
//                    'type'   : 'text',
//                    'default': '2,3,4',
//                    'return' : 'jsonArray'
//                    },
//        config   : {
//                'type'   : 'text',
//                'default': 5,
//                'return' : 'json'
//                }
//        }
//};


$('body').ready(function() {
        
    $(".inputJson").each(function(){
        
        inputJSON.inputUpdateGraph($(this));
    });
    
    $(".inputJson").on('change', function(){
        
        inputJSON.inputUpdateGraph($(this));
    });
    
    $(".jsonSwitch").on("click", function(){
        
        let id        = $(this).attr('data-id');
        let input     = $("#" + id);
        let graph     = $("#" + id + '_graph');
        let buttons   = $("#" + id + '_buttons');
        let switchBot = $(this);
        
        let isVisible = (graph.css('display') === 'block')? true : false;
        
        if(isVisible){
            
            input.slideDown();
            graph.slideUp();
            buttons.slideUp();
            switchBot.addClass('opened');
        }else{
            
            graph.slideDown();
            buttons.slideDown();
            input.slideUp();
            switchBot.removeClass('opened');
        }
    });
});