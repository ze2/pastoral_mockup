/**
 *  No form de endereços do módulo rádios,
 *  ao alterar o campo de CEP, esta função é chamada para buscar,
 *  via ajax, o endereço do cep informado.
 */
function getEndereco(cep){
  $('#carregando').show();

  $.post(
    '/radios/editar/get-endereco',
    'cep='+ cep,
    function(data){
      $('#municipio_id-element').html(data);
      $('#carregando').fadeOut('fast');
      $('#municipio_id-element').effect('highlight', {}, 500);
    }
  );
}

/**
 *  No form de endereços do módulo rádios,
 *  ao alterar o campo de Estado, esta função é chamada para buscar,
 *  via ajax, os municípios do estado selecionado.
 */
function getMunicipios(uf, municipio_id){
  $('#carregando').show();

  $.post(
    '/radios/editar/get-municipios',
    'uf='+ uf +'&municipio_id='+ municipio_id,
    function(data){
      $('#municipio_id-element').html(data);
      $('#carregando').fadeOut('fast');
      $('#municipio_id-element').effect('highlight', {}, 500);
    }
  );
}

/**
 *  No form de endereços do módulo rádios,
 *  ao alterar o campo de Municipios, esta função é chamada para buscar,
 *  via ajax, os setores do municipio selecionado.
 */
function getSetores(municipio_id, setor_id){
  $('#carregando').show();

  $.post(
    '/radios/editar/get-setores',
    'municipio_id='+ municipio_id +'&setor_id='+ setor_id,
    function(data){
      $('#setor_id-element').html(data);
      $('#carregando').fadeOut('fast');
      $('#setor_id-element').effect('highlight', {}, 500);
    }
  );
}

/**
 *  Exibe / oculta os campos de filtro e legenda de ações
 *  na listagem de radios, módulo radios, controller index
 *  action index.
 */
function manageBlocoProgramaRadio(id){
  $('.bloco'+ id).each(function(index){
    elemento  = $(this);;

    display = elemento.css('display');

    if(display == 'none'){
      elemento.slideDown();
    }
    else{
      elemento.slideUp();
    }
  });

  if(display == 'none'){
    $("#filtrosShow"+ id).hide();
    $("#filtrosHide"+ id).show();
  }
  else{
    $("#filtrosHide"+ id).hide();
    $("#filtrosShow"+ id).show();
  }
}