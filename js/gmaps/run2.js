// Mensagens de feedback ao usuário, traduzidas por i18n em location/views/scripst/gmaps/index.phtml  
var msgs = $.parseJSON(json_msgs);
    
function run_google_maps_api(lat, lon) {
    var centro_comunidade = new google.maps.LatLng(lat, lon);
    var marker;
    var map;
    var mapOptions = {
     	zoom: 15,
     	mapTypeId: google.maps.MapTypeId.HYBRID,
     	center: centro_comunidade
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    geocoder = new google.maps.Geocoder();
    /*
    * Se marker_moved for igual 1, o usuário clicou em Localizar endereço
    * Então, se encontrar o endereço na base do google maps, posicinará o pino nesta localidade
    * e definirá como ponto central da comunidade as coordenadas retornadas pelo geocoder, 
    * tendo como parâmetro o campo address, o qual poderá ter recebido city_state na view, ou o resultado de gecoder anterior
    */
    latlon = new google.maps.LatLng(lat, lon);
    if((document.getElementById('marker_moved').value == 1) || ((lat == 0) && (lon == 0))) {
	endereco = document.getElementById('address').value;
	geocoder.geocode( { 'address': endereco}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: map,
                    draggable:true,
                    animation: google.maps.Animation.DROP,
                    position: results[0].geometry.location
                });
                setLatLon(results[0].geometry.location);
                codeLatLng(results[0].geometry.location, geocoder);
                google.maps.event.addListener(marker, 'mouseup', function(event) {
                    codeLatLng(event.latLng, geocoder);
                    document.getElementById('marker_moved').value=2;
                    setLatLon(event.latLng)
                });
            } 
            else {
                alert(msgs.msg1a+endereco+msgs.msg1b);
            }
	});// end geocoder.geocode( { 'address': endereco}...
    }// end if((document.getElementById('marker_moved').value == 1) || ((lat == 0) && (lon == 0)))
    else {
	marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: latlon
	});
	codeLatLng(latlon, geocoder);
	google.maps.event.addListener(marker, 'mouseup', function(event) {
            codeLatLng(event.latLng, geocoder);
            document.getElementById('marker_moved').value=3;
            setLatLon(event.latLng)
	});
    }
} //function run_google_maps_api(lat, lon)
function setLatLon (coords) {
    document.getElementById("latitude").value = coords.lat();
    document.getElementById("longitude").value = coords.lng();
}
function codeLatLng(latlon, geocoder){
    geocoder.geocode({'latLng': latlon}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
		document.getElementById("address").value = results[0].address_components[1].short_name+', '+results[0].address_components[0].short_name+' ,'+results[0].address_components[2].short_name+' ,'+results[0].address_components[3].short_name;
		document.getElementById("munic_name").value = results[0].address_components[results[0].address_components.length - 4].short_name;
		document.getElementById("neighborhood").value = results[0].address_components[results[0].address_components.length - 5].short_name;
		document.getElementById("street_address").value = results[0].address_components[1].short_name+', '+results[0].address_components[0].short_name;
		document.getElementById("zip_code").value = results[0].address_components[6].short_name;
            } 
            else {
		document.getElementById("munic_name").value = 'Não definido';
		alert(msgs.msg3);
            }
	} 
        else {
            alert(msgs.msg4 + status);
            return;
	}
    });
    
} // function codeLatLng(latlon, geocoder)

/*
 * Função para abrir popup
 */
function abre_popup(pagina, titulo) {
	window.open(pagina, titulo, 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=800,height=600');
}

function check_marker_moved_and_submit_for_save(country){
	document.getElementById('salvar_localiz_clicked').value=1;
        if (document.getElementById('marker_moved').value < 1) {
           alert(msgs.msg2);
       }
        else if (country != 1){
            alert(msgs.msg5);
        }
        else {
            document.ponto.submit();
        }
} // function check_marker_moved_and_submit_for_save()

function confirm_and_submit_for_exclude(nome,gid){
    document.getElementById('excluir_localiz_clicked').value=1;
    if ( gid == 0 ){
        alert(msgs.msg6);
    }
    else {
        
        var sure = confirm(msgs.msg7a + nome + msgs.msg7b);
        if ( sure ){
            document.ponto.submit();
        }
        else{
            alert(msgs.msg8);
        }
    }
    document.getElementById('excluir_localiz_clicked').value=0;
    
}
/*
 *	funcao para esconder / mostrar div
 */
function show_or_hide_right_column(id) 
{
	if(document.getElementById(id).style.display=="none") {
		document.getElementById(id).style.display = "block";
	}
	else 
	{
		document.getElementById(id).style.display = "none";
	}
}
/*
 * função para inibir a submissão com Enter
 */
function disableEnterKey(e)
{
     var key;
     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox
     if(key == 13)
          return false;
     else
          return true;
}

