//verificaLocationId(); 
var param_location_id = $('#location_id').val();
var param_city_gid = $('#city_gid').val();
var dias_crianca_show = $('#dias_crianca_show').val();
var dias_crianca_ativa = $('#dias_crianca_ativa').val();
var dias_gestante_show = $('#dias_gestante_show').val();
var dias_gestante_ativa = $('#dias_gestante_ativa').val();
var dias_comunidade_show = $('#dias_comunidade_show').val();
var dias_comunidade_ativa = $('#dias_comunidade_ativa').val();
var user_id = $('#user_id').val();
var temporary_table = $('#temporary_table').val();
var box_sequence = $('#box_sequence').val();
var children = ($('#children').val() * 1 == 1) ? 'crianca_visitada' : (($('#children').val() * 1 == 2) ? 'crianca_abandonada' : null);
var babies = ($('#babies').val() * 1 == 1) ? 'bebe_visitado' : (($('#babies').val() * 1 == 2) ? 'bebe_abandonado' : null);
var pregnant = ($('#pregnant').val() * 1 == 1) ? 'gestante_visitada' : (($('#pregnant').val() * 1 == 2) ? 'gestante_abandonada' : null);
var states = ($('#states').val() * 1 == 1) ? $('#states').val() : null;
var time_tmp = $('#time_reload').val() * 1;
var time_reload_tmp = $('#time_reload').val();
var time_reload = (typeof time_tmp == 'number') ? time_tmp * 1000 : 5000;
var users_id = $('#users_id').val();
var num_usuarios_ativos = (!!$('#num_usuarios_ativos').val()) ? $('#num_usuarios_ativos').val() : 0;
var layerAppvisita = null;
localStorage.setItem('temporary_table', $('#temporary_table').val());

if (param_location_id != '' && param_city_gid != '') {
	//alert(param_location_id);
	$.getScript("/js/map/layersOsm.js", function(){
		$.getScript("/js/map/functionsOsm.js", function(){
			$.getScript("/js/map/mapOsm.js", function(){
				$.getScript("/js/map/arraysOsm.js", function(){
					$.getScript("/js/map/eventsOsm.js", function(){
						getExtentCommunity(param_location_id, param_city_gid);
					});
				});
			});
		});
	});
} else {
	$.getScript("/js/map/layersOsm.js", function(){
		$.getScript("/js/map/functionsOsm.js", function(){
			$.getScript("/js/map/mapOsm.js", function(){
				$.getScript("/js/map/arraysOsm.js", function(){
					$.getScript("/js/map/eventsOsm.js", function(){
						if (box_sequence != '') {
							redirectExtent(box_sequence, children, babies, pregnant, states);
						}
					});
				});
			});
		});
	});
}

var pathname = $('#pathname').val();
if (pathname.indexOf('controllers') != -1) {
	pathname = pathname.replace('controllers', '');
}

var urlOrigin = window.location.origin;
var origin = urlOrigin;
//var localMapfile = '/wmspc?map=/var/www/application/modules/map/mapfiles/';
var localMapfile = '/wmspc?map='+pathname+'/map/mapfiles/';
var nameLayer = '&layer=';
var getLegend = '&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=png';

var extent = '';

var result = null;
var type_id = 0;
var len_search = 0;

var location_id = null;
var name_table = null;
var result_search = null;
var search_val = '';

var initialZoom = 4.7;

var layerHighlight = null;

var selCamadas = 
'<ul class="nav nav-list">'+
	'<li>'+ //Pastoral da Criança
		'<span class="spPC icon icon-minus tree-toggler header"></span>'+
		'<span class="spPC tree-toggler tree-header">  Pastoral da Criança</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spSetores icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerSetores" layerName="Setores">&thinsp;'+
				'<span class="spSetores tree-toggler">Setores</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_setores.map'+nameLayer+'setores'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spRamos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerRamos" layerName="Ramos">&thinsp;'+
				'<span class="spRamos tree-toggler">Ramos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_ramos.map'+nameLayer+'ramos'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spComAtivas icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerComunidadesPontoMedio" layerName="Comunidades ativas e ponto médio" class="zoom400">&thinsp;'+
				'<span class="spComAtivas tree-toggler zoom400">Comunidades ativas e ponto médio</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_comunidades_ponto_medio.map'+nameLayer+'comunidades_ponto_medio'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spHortaCasa icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerHortaCasa" layerName="Crianças com horta em casa" class="zoom400">&thinsp;'+
				'<span class="spHortaCasa tree-toggler zoom400">Crianças com horta em casa</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_horta_em_casa.map'+nameLayer+'horta'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spPontosCriancas icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerPontosCriancas" layerName="Pontos crianças" class="zoom4500">&thinsp;'+
				'<span class="spPontosCriancas tree-toggler zoom4500">Pontos crianças</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_pontos_criancas.map'+nameLayer+'pontos_criancas'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spPontosGestantes icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerPontosGestantes" layerName="Pontos gestantes" class="zoom4500">&thinsp;'+
				'<span class="spPontosGestantes tree-toggler zoom4500">Pontos gestantes</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_pontos_gestantes.map'+nameLayer+'pontos_gestantes'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spPontosBebes icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerPontosBebes" layerName="Pontos bebês" class="zoom4500">&thinsp;'+
				'<span class="spPontosBebes tree-toggler zoom4500">Pontos bebês</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_pontos_bebes.map'+nameLayer+'pontos_bebes'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spGraficoPontos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerGraficoPontos" layerName="Gráfico pontos crianças, bebês e gestantes" class="zoom4500">&thinsp;'+
				'<span class="spGraficoPontos tree-toggler zoom4500">Gráfico pontos crianças, bebês e gestantes</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_grafico_pontos.map'+nameLayer+'grafico_pontos'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //CNBB
		'<span class="spCNBB icon icon-minus tree-toggler header"></span>'+
		'<span class="spCNBB tree-toggler tree-header">  CNBB</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spDioceses icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerDioceses" layerName="Dioceses">&thinsp;'+
				'<span class="spDioceses tree-toggler">Dioceses</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_dioceses.map'+nameLayer+'dioceses'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spParoquias icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerParoquias" layerName="Paroquias" class="zoom4500">&thinsp;'+
				'<span class="spParoquias tree-toggler zoom4500">Paroquias</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_paroquias.map'+nameLayer+'paroquias'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spSedesParoquias icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerSedesParoquias" layerName="Sedes das Paróquias">&thinsp;'+
				'<span class="spSedesParoquias tree-toggler">Sedes das Paróquias</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_sedes_paroquias.map'+nameLayer+'sedes_paroquias'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Censo IBGE 2010
		'<span class="spCenso2010 icon icon-minus tree-toggler header"></span>'+
		'<span class="spCenso2010 tree-toggler tree-header">  Censo IBGE 2010</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spMenor1Ano2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerMenores1Ano2010" name="mutualDisable" layerName="Crianças menores de 1 ano" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spMenor1Ano2010 tree-toggler zoom400">Crianças menores de 1 ano</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_menores_1_ano_2010.map'+nameLayer+'criancas_menores_1_2010'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spCriancas0a6anos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCriancas0a6Anos2010" name="mutualDisable" layerName="Crianças de 0 a 6 anos incompletos" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spCriancas0a6anos tree-toggler zoom400">Crianças de 0 a 6 anos incompletos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_criancas_0_a_6_anos.map'+nameLayer+'criancas_0_6_2010'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spPobres0a6anos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCriancasPobres2010" name="mutualDisable" layerName="Crianças pobres de 0 a 6 anos" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spPobres0a6anos tree-toggler zoom400">Crianças pobres de 0 a 6 anos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_criancas_pobres.map'+nameLayer+'criancas_pobres_0_6_2010'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spMiseraveis0a6anos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCriancasMiseraveis2010" name="mutualDisable" layerName="Crianças miseráveis de 0 a 6 anos" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spMiseraveis0a6anos tree-toggler zoom400">Crianças miseráveis de 0 a 6 anos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<li><img src="'+origin+localMapfile+'brasil_criancas_miseraveis.map'+nameLayer+'criancas_miseraveis_0_6_2010'+getLegend+'"></li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spRendaCrianca icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerRendaCrianca2010" layerName="% de crianças de 0 a 6 anos em faixas de renda">&thinsp;'+
				'<span class="spRendaCrianca tree-toggler">% de crianças de 0 a 6 anos em faixas de renda</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_renda_crianca.map'+nameLayer+'renda_crianca_setor_censitario'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spAbastAgua2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerAbastAgua2010" name="mutualDisable" layerName="Água - Percentual de domicílios abastecidos pela rede geral">&thinsp;'+
				'<span class="spAbastAgua2010 tree-toggler">Água - Percentual de domicílios abastecidos pela rede geral</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2010.map'+nameLayer+'abastecimento_agua_2010_setor_censitario'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spAbastAguaPie2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerAbastAguaPie2010" layerName="Água - Detalhes do abastecimento em gráficos" class="zoom50">&thinsp;'+
				'<span class="spAbastAguaPie2010 tree-toggler zoom50">Água - Detalhes do abastecimento em gráficos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2010_pie.map'+nameLayer+'abastecimento_agua_2010_pie'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spEsgotSanit2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerEsgotSanit2010" layerName="Esgotamento sanitário - Indicador em áreas">&thinsp;'+
				'<span class="spEsgotSanit2010 tree-toggler">Esgotamento sanitário - Indicador em áreas</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_esgotamento_sanitario_2010.map'+nameLayer+'esgotamento_sanitario_2010_area_setor_censitario'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spEsgotSanitPie2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerEsgotSanitPie2010" layerName="Esgotamento sanitário - Detalhes em gráficos" class="zoom50">&thinsp;'+
				'<span class="spEsgotSanitPie2010 tree-toggler zoom50">Esgotamento sanitário - Detalhes em gráficos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_esgotamento_sanitario_2010.map'+nameLayer+'esgotamento_sanitario_2010_pie'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spDestLixo2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerDestLixo2010" layerName="Destino do lixo - Indicador em áreas">&thinsp;'+
				'<span class="spDestLixo2010 tree-toggler">Destino do lixo - Indicador em áreas</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_destino_lixo_2010.map'+nameLayer+'destino_lixo_2010_area_setor_censitario'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spDestLixoPie2010 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerDestLixoPie2010" layerName="Destino do lixo - Detalhes em gráficos" class="zoom50">&thinsp;'+
				'<span class="spDestLixoPie2010 tree-toggler zoom50">Destino do lixo - Detalhes em gráficos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_destino_lixo_2010_pie.map'+nameLayer+'destino_lixo_2010_pie'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spPropCatolicos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerPropCatolicos2010" layerName="% de católicos em relação a outras religiões e sem religião">&thinsp;'+
				'<span class="spPropCatolicos tree-toggler">% de católicos em relação a outras religiões e sem religião</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_prop_catolicos.map'+nameLayer+'prop_catholics_setor_censitario'+getLegend+'">'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Político-administrativo
		'<span class="spPolAdmin icon icon-minus tree-toggler header"></span>'+
		'<span class="spPolAdmin tree-toggler tree-header">  Político-administrativo</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spEstados icon icon-minus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerEstados" layerName="Estados brasileiros" checked>&thinsp;'+
				'<span class="spEstados tree-toggler">Estados brasileiros</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_estados.map'+nameLayer+'br_ufs'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spMunicipios icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerMunicipios" layerName="Municípios" class="zoom4500">&thinsp;'+
				'<span class="spMunicipios tree-toggler zoom4500">Municípios</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_municipios.map'+nameLayer+'municipios_poligono'+getLegend+'">'+
					'<li>'+
						'<span class="spSedesMunicipais icon icon-plus tree-toggler"></span>'+
						'<input type="checkbox" id="elemlayerSedesMunicipais" layerName="Sedes Municipais" class="zoom400">&thinsp;'+
						'<span class="spSedesMunicipais tree-toggler zoom400">Sedes Municipais</span>'+
						'<ul class="nav nav-list tree" style="display: none;">'+
							'<img src="'+origin+localMapfile+'brasil_sedes_municipais.map'+nameLayer+'sedes_municipais'+getLegend+'">'+
						'</ul>'+
					'</li>'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spCapitais icon icon-minus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCapitais" layerName="Capitais" checked>&thinsp;'+
				'<span class="spCapitais tree-toggler">Capitais</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_capitais.map'+nameLayer+'capitais'+getLegend+'">'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Saúde
		'<span class="spSaude icon icon-minus tree-toggler header"></span>'+
		'<span class="spSaude tree-toggler tree-header">  Saúde</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spUnidSaude icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerUnidadesSaude" layerName="Unidades de Saúde" class="zoom400">&thinsp;'+
				'<span class="spUnidSaude tree-toggler zoom400">Unidades de Saúde</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_unidades_saude.map'+nameLayer+'UnidadesSaude'+getLegend+'">'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Serviços de Mapas
		'<span class="spServMapas icon icon-minus tree-toggler header"></span>'+
		'<span class="spServMapas tree-toggler tree-header">  Serviços de Mapas</span>'+
		'<ul class="nav nav-list tree">'+
			'<li><input type="checkbox" id="elemLayerOSM" checked>OpenStreetMap</li>'+
			'<li><input type="checkbox" id="elemLayerCigex">Cigex</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Projeto 1.000.000 de cisternas
		'<span class="spProjCisternas icon icon-minus tree-toggler header"></span>'+
		'<span class="spProjCisternas tree-toggler tree-header">  Projeto 1.000.000 de cisternas</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spCisternasDentroMunic icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCisternasDentroMunic" layerName="Cisternas Dentro do Município Declarado" class="zoom400">&thinsp;'+
				'<span class="spCisternasDentroMunic tree-toggler zoom400">Cisternas Dentro do Município Declarado</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_cisternas_dentro_munic.map'+nameLayer+'cisternasDentroMunic'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spCisternasForaMunic icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCisternasForaMunic" layerName="Cisternas Fora do Município Declarado" class="zoom400">&thinsp;'+
				'<span class="spCisternasForaMunic tree-toggler zoom400">Cisternas Fora do Município Declarado</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_cisternas_fora_munic.map'+nameLayer+'cisternasForaMunic'+getLegend+'">'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
	'<li>'+ //Censo IBGE 2000
		'<span class="spCenso2000 icon icon-minus tree-toggler header"></span>'+
		'<span class="spCenso2000 tree-toggler tree-header">  Censo IBGE 2000</span>'+
		'<ul class="nav nav-list tree">'+
			'<li>'+
				'<span class="spAbastAgua2000 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerAbastAgua2000" layerName="Abastecimento de água" class="zoom400">&thinsp;'+
				'<span class="spAbastAgua2000 tree-toggler zoom400">Abastecimento de água</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2000.map'+nameLayer+'abastecimento_agua_2000'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spMenor1Ano2000 icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerMenores1Ano2000" name="mutualDisable" layerName="Criancas menores de 1 ano" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spMenor1Ano2000 tree-toggler zoom400">Criancas menores de 1 ano</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_menores_1_ano_2000.map'+nameLayer+'criancasmenos1'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spCriancas1a5anos icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerCriancas1a5Anos2000" name="mutualDisable" layerName="Criancas de 1 a 5 anos" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spCriancas1a5anos tree-toggler zoom400">Criancas de 1 a 5 anos</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_criancas_1_a_5_anos.map'+nameLayer+'criancas1a5'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spAnosEstudo icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerAnosEstudo2000" name="mutualDisable" layerName="Média de anos de estudo" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spAnosEstudo tree-toggler zoom400">Média de anos de estudo</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_anos_estudo.map'+nameLayer+'anosestudo'+getLegend+'">'+
				'</ul>'+
			'</li>'+
			'<li>'+
				'<span class="spRendaFamilia icon icon-plus tree-toggler"></span>'+
				'<input type="checkbox" id="elemlayerRendaFamilia2000" name="mutualDisable" layerName="Media de renda do chefe da familia (salário mínimo R$151,00)" class="zoom400">&thinsp;'+ //atributo name
				'<span class="spRendaFamilia tree-toggler zoom400">Media de renda do chefe da familia (salário mínimo R$151,00)</span>'+
				'<ul class="nav nav-list tree" style="display: none;">'+
					'<img src="'+origin+localMapfile+'brasil_renda_familia.map'+nameLayer+'rendafamilia'+getLegend+'">'+
				'</ul>'+
			'</li>'+
		'</ul>'+
	'</li>'+
 '</ul>';
