//Criação de array com todos os layers 
var arrayLayers = [layerOSM,layerEstados,layerCigex,layerMunicipios,layerSedesMunicipais,layerDioceses,layerParoquias,layerSedesParoquias,
				   layerSetores,layerCapitais,layerRamos,layerComunidadesPontoMedio,layerHortaCasa,layerPontosCriancas,layerPontosGestantes,
				   layerPontosBebes,layerGraficoPontos,layerMenores1Ano2010,layerCriancas0a6Anos2010,layerCriancasPobres2010,
				   layerCriancasMiseraveis2010,layerRendaCrianca2010,layerAbastAgua2010,layerAbastAguaPie2010,layerEsgotSanit2010,
				   layerEsgotSanitPie2010,layerDestLixo2010,layerDestLixoPie2010,layerPropCatolicos2010,layerUnidadesSaude,
				   layerCisternasDentroMunic,layerCisternasForaMunic,layerAbastAgua2000,layerMenores1Ano2000,layerCriancas1a5Anos2000,
				   layerAnosEstudo2000,layerRendaFamilia2000];

//Criação de array com todos os ids dos inputs
var arrayElem = ['elemLayerOSM','elemlayerEstados','elemLayerCigex','elemlayerMunicipios','elemlayerSedesMunicipais','elemlayerDioceses',
				 'elemlayerParoquias','elemlayerSedesParoquias','elemlayerSetores','elemlayerCapitais','elemlayerRamos',
				 'elemlayerComunidadesPontoMedio','elemlayerHortaCasa','elemlayerPontosCriancas','elemlayerPontosGestantes',
				 'elemlayerPontosBebes','elemlayerGraficoPontos','elemlayerMenores1Ano2010','elemlayerCriancas0a6Anos2010',
				 'elemlayerCriancasPobres2010','elemlayerCriancasMiseraveis2010','elemlayerRendaCrianca2010','elemlayerAbastAgua2010',
				 'elemlayerAbastAguaPie2010','elemlayerEsgotSanit2010','elemlayerEsgotSanitPie2010','elemlayerDestLixo2010',
				 'elemlayerDestLixoPie2010','elemlayerPropCatolicos2010','elemlayerUnidadesSaude','elemlayerCisternasDentroMunic',
				 'elemlayerCisternasForaMunic','elemlayerAbastAgua2000','elemlayerMenores1Ano2000','elemlayerCriancas1a5Anos2000',
				 'elemlayerAnosEstudo2000','elemlayerRendaFamilia2000'];

//Criação de array com todos os tiles
var arrayTiles = [null,tileEstados,null,tileMunicipios,tileSedesMunicipais,tileDioceses,tileParoquias,tileSedesParoquias,tileSetores,
				  tileCapitais,tileRamos,tileComunidadesPontoMedio,tileHortaCasa,tilePontosCriancas,tilePontosGestantes,tilePontosBebes,
				  tileGraficoPontos,tileMenores1Ano2010,tileCriancas0a6Anos2010,tileCriancasPobres2010,tileCriancasMiseraveis2010,
				  tileRendaCrianca2010Munic,tileAbastAguaMunic,null,tileEsgotSanitAreaMunic,null,tileDestLixo2010Munic,null,
				  tilePropCatolicos2010Munic,tileUnidadesSaude,tileCisternasDentroMunic,tileCisternasForaMunic,tileAbastAgua2000,
				  tileMenores1Ano2000,tileCriancas1a5Anos2000,tileAnosEstudo2000,tileRendaFamilia2000];

//Array com todos os códigos UFs
var arrayCodUf = [{codUf:11, uf:"RO"}, {codUf:12, uf:"AC"}, {codUf:13, uf:"AM"}, {codUf:14, uf:"RR"}, {codUf:15, uf:"PA"}, 
				  {codUf:16, uf:"AP"}, {codUf:17, uf:"TO"}, {codUf:21, uf:"MA"}, {codUf:22, uf:"PI"}, {codUf:23, uf:"CE"}, 
				  {codUf:24, uf:"RN"}, {codUf:25, uf:"PB"}, {codUf:26, uf:"PE"}, {codUf:27, uf:"AL"}, {codUf:28, uf:"SE"}, 
				  {codUf:29, uf:"BA"}, {codUf:31, uf:"MG"}, {codUf:32, uf:"ES"}, {codUf:33, uf:"RJ"}, {codUf:35, uf:"SP"}, 
				  {codUf:41, uf:"PR"}, {codUf:42, uf:"SC"}, {codUf:43, uf:"RS"}, {codUf:50, uf:"MS"}, {codUf:51, uf:"MT"}, 
				  {codUf:52, uf:"GO"}, {codUf:53, uf:"DF"}
				 ];

//Array com os nomes dos layers e nome de apenas 1 coluna
var arrayLayers1Col = [{layer:"municipios_poligono", colName:"Nome"}, {layer:"sedes_municipais", colName:"Nome"}, {layer:"paroquias", colName:"Nome"}, {layer:"dioceses", colName:"Nome"}, 
					   {layer:"sedes_paroquias", colName:"Nome"}, {layer:"setores", colName:"Nome"}, {layer:"capitais", colName:"Nome da capital"}, 
					   {layer:"ramos", colName:"Nome"}, {layer:"pontos_criancas", colName:"Nome da criança"}, {layer:"pontos_gestantes", colName:"Nome da gestante"}, 
					   {layer:"pontos_bebes", colName:"Nome do bebê"}, 
					   {layer:"abastecimento_agua_2000", colName:"Valor em %"}, {layer:"criancasmenos1", colName:"Número de crianças"}, 
					   {layer:"criancas1a5", colName:"Número de crianças"}, {layer:"anosestudo", colName:"Valor"}, {layer:"rendafamilia", colName:"Valor"}
					  ];

//Array com os nomes dos layers e nome de N colunas
var arrayLayersNcol = [{layer:"comunidades_ponto_medio", colName: [{col1: "Nome da comunidade"}, {col2: "Número de crianças"}, {col3: "Meses com informação"}]},
					   {layer:"grafico_pontos", colName: [{col1: "Nome da comunidade"}, {col2: "% de crianças"}, {col3: "% de bebês"}, {col4: "% de gestantes"}, {col5: "% de crianças, bebês e gestantes abandonados"}]},
					   {layer:"abastecimento_agua_2010_setor_censitario", colName: [{col1: "Nome ou código"}, {col2: "Nº total de domicílios"}, {col3: "Nº de domicílios abastecidos por rede geral"}, {col4: "% com abastecimento por rede geral"}, {col5: "% com água de poço ou nascente"}, {col6: "% com água de chuva em cisterna"}, {col7: "% com outra forma de abastecimento"}] }, 
					   {layer:"abastecimento_agua_2010_municipio", colName: [{col1: "Nome ou código"}, {col2: "Nº total de domicílios"}, {col3: "Nº de domicílios abastecidos por rede geral"}, {col4: "% com abastecimento por rede geral"}, {col5: "% com água de poço ou nascente"}, {col6: "% com água de chuva em cisterna"}, {col7: "% com outra forma de abastecimento"}] }, 
					   //{layer:"abastecimento_agua_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}] }, 
					   {layer:"cisternasDentroMunic", colName: [{col1: "Nº da Cisterna"}, {col2: "Nome da Comunidade"}, {col3: "Nome do Município"}, {col4: "UF"}] }, 
					   {layer:"cisternasForaMunic", colName: [{col1: "Nº da Cisterna"}, {col2: "Nome da Comunidade"}, {col3: "Nome do Município"}, {col4: "UF"}] }, 
					   //{layer:"comunidades", colName: [{col1: "Nome da Comunidade"}, {col2: "Crianças de 0 a 6 anos cadastradas"}, {col3: "Crianças de 0 a 1 ano cadastradas"}] }, 
					   {layer:"criancas_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de crianças"}] }, 
					   {layer:"criancas_miseraveis_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças Miseráveis"}] }, 
					   {layer:"criancas_pobres_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças Pobres"}] }, 
					   {layer:"renda_crianca_setor_censitario", colName: [{col1: "de 0 a 1/4 s.m."}, {col2: "de 1/4 a 1/2 s.m."}, {col3: "de 1/4 a 1/2 s.m."}, {col4: "de 1 a 10 ou mais s.m."}] }, 
					   {layer:"renda_crianca_municipio", colName: [{col1: "de 0 a 1/4 s.m."}, {col2: "de 1/4 a 1/2 s.m."}, {col3: "de 1/4 a 1/2 s.m."}, {col4: "de 1 a 10 ou mais s.m."}] }, 
					   {layer:"destino_lixo_2010_area_setor_censitario", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% Coletado por serviço de limpeza"}, {col4: "% Coletado em caçamba"}, {col5: "% Queimado"}, {col6: "% Enterrado"}, {col7: "% Jogado em terreno baldio"}, {col8: "% Jogado em rio&#44; lago ou mar"}, {col9: "% Outro destino"}] }, 
					   {layer:"destino_lixo_2010_area_municipio", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% Coletado por serviço de limpeza"}, {col4: "% Coletado em caçamba"}, {col5: "% Queimado"}, {col6: "% Enterrado"}, {col7: "% Jogado em terreno baldio"}, {col8: "% Jogado em rio&#44; lago ou mar"}, {col9: "% Outro destino"}] }, 
					   //{layer:"destino_lixo_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}, {col6: ""}, {col7: ""}, {col8: ""}, {col9: ""}] }, 
					   {layer:"esgotamento_sanitario_2010_area_setor_censitario", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% rede geral de esgoto ou pluvial"}, {col4: "% fossa séptica"}, {col5: "% fossa rudimentar"}, {col6: "vala"}, {col7: "% rio&#44; lago ou mar"}, {col8: "% outro escoadouro"}] }, 
					   {layer:"esgotamento_sanitario_2010_area_municipio", colName: [{col1: "Nome do município"}, {col2: "Qualidade"}, {col3: "% rede geral de esgoto ou pluvial"}, {col4: "% fossa séptica"}, {col5: "% fossa rudimentar"}, {col6: "vala"}, {col7: "% rio&#44; lago ou mar"}, {col8: "% outro escoadouro"}] }, 
					   //{layer:"esgotamento_sanitario_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}, {col6: ""}, {col7: ""}] }, 
					   {layer:"horta", colName: [{col1: "Nome da comunidade"}, {col2: "Mês inicial"}, {col3: "Mês final"}, {col4: "Crianças com horta"}] }, 
					   {layer:"criancas_menores_1_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças"}] }, 
					   {layer:"prop_catholics_setor_censitario", colName: [{col1: "Código do Setor Censitário"}, {col2: "% de Católicos"}] }, 
					   {layer:"prop_catholics_municipio", colName: [{col1: "Código do Município"}, {col2: "% de Católicos"}] }, 
					   {layer:"UnidadesSaude", colName: [{col1: "Código"}, {col2: "Nome da US"}, {col3: "Nome do Município"}, {col4: "UF"}] }
					  ];

var arrayPrior = [{layerName: layerOSM, zindex: 0}, {layerName: layerEstados, zindex: 2},
				  {layerName: layerCigex, zindex: 4}, {layerName: layerMunicipios, zindex: 2}, 
				  {layerName: layerSedesMunicipais, zindex: 4}, {layerName: layerDioceses, zindex: 2}, 
				  {layerName: layerParoquias, zindex: 2}, {layerName: layerSedesParoquias, zindex: 4}, 
				  {layerName: layerSetores, zindex: 2}, {layerName: layerCapitais, zindex: 4}, 
				  {layerName: layerRamos, zindex: 2}, {layerName: layerComunidadesPontoMedio, zindex: 5}, 
				  {layerName: layerHortaCasa, zindex: 4}, {layerName: layerPontosCriancas, zindex: 4}, 
				  {layerName: layerPontosGestantes, zindex: 4}, {layerName: layerPontosBebes, zindex: 4}, 
				  {layerName: layerGraficoPontos, zindex: 4}, {layerName: layerMenores1Ano2010, zindex: 2}, 
				  {layerName: layerCriancas0a6Anos2010, zindex: 2}, {layerName: layerCriancasPobres2010, zindex: 2}, 
				  {layerName: layerCriancasMiseraveis2010, zindex: 2}, {layerName: layerRendaCrianca2010, zindex: 1}, 
				  {layerName: layerAbastAgua2010, zindex: 1}, {layerName: layerAbastAguaPie2010, zindex: 4}, 
				  {layerName: layerEsgotSanit2010, zindex: 1}, {layerName: layerEsgotSanitPie2010, zindex: 4}, 
				  {layerName: layerDestLixo2010, zindex: 1}, {layerName: layerDestLixoPie2010, zindex: 4}, 
				  {layerName: layerPropCatolicos2010, zindex: 1}, {layerName: layerUnidadesSaude, zindex: 4}, 
				  {layerName: layerCisternasDentroMunic, zindex: 4}, {layerName: layerCisternasForaMunic, zindex: 4}, 
				  {layerName: layerAbastAgua2000, zindex: 1}, {layerName: layerMenores1Ano2000, zindex: 2}, 
				  {layerName: layerCriancas1a5Anos2000, zindex: 2}, {layerName: layerAnosEstudo2000, zindex: 1}, 
				  {layerName: layerRendaFamilia2000, zindex: 1}
				 ];
