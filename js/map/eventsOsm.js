$(document).ready(function(){
	
	//Seta zindex
	for (var cont = 0; cont < arrayPrior.length; cont++) {
		var zindex = arrayPrior[cont]['zindex'];
		arrayPrior[cont]['layerName'].setZIndex(zindex);
	}
	
	map.on('singleclick', function(evt) {
		$('body').css('cursor', 'wait');
		
		var viewResolution = (view.getResolution());
		
		var resTable = '';
		
		$('input[type="checkbox"]').each(function(){
			if ($(this).is(':checked')) {
				var id_item = $(this).attr('id');
			}
			var layer = '';
			
			//Verificação de qual input selecionou
			for (var i = 0; i < arrayElem.length; i++) {
				var count = 0;
				
				if ((arrayElem[i] != 'elemLayerOSM' && arrayElem[i] != 'elemLayerCigex') && arrayElem[i] == id_item) {
					tile = arrayTiles[i];
					
					if (tile != null) {
						//viewResolution < 0.001040759971511351 = quando camadas (% de crianças de 0 a 6 anos em faixas de renda, Água - Percentual de domicílios abastecidos pela rede geral, Esgotamento sanitário - Indicador em áreas e Destino do lixo - Indicador em áreas), mudam de layer
						if (viewResolution < 0.001040759971511351) {
							if (arrayElem[i] == 'elemlayerRendaCrianca2010') {
								tile = tileRendaCrianca2010SetorCens;
							} else if (arrayElem[i] == 'elemlayerPropCatolicos2010') {
								tile = tilePropCatolicos2010SetorCens;
							} else if (arrayElem[i] == 'elemlayerAbastAgua2010') {
								tile = tileAbastAguaSetorCens;
							} else if (arrayElem[i] == 'elemlayerEsgotSanit2010') {
								tile = tileEsgotSanitSetorCens;
							} else if (arrayElem[i] == 'elemlayerDestLixo2010') {
								tile = tileDestLixo2010SetorCens;
							}
							
							var url = tile.getGetFeatureInfoUrl(
								evt.coordinate, viewResolution, 'EPSG:4326'
							);
						} else {
							var url = tile.getGetFeatureInfoUrl(
								evt.coordinate, viewResolution, 'EPSG:4326'
							);
						}
					}
					
					var layerName = $(this).attr('layerName');
					
					if (url) {
						$.ajax({ 
							url:        url,
							dataType:   'text',
							success: function(data) {
								if (data.indexOf('Search returned no results') <= -1) {
									var dataSplit = data.split(" = '");
									
									if (dataSplit.length > 1) {
										var line1 = '<tr style="color: #003333; background-color: #EBEADB;"><td>@</td>';
										var line2 = '<tr><td>@</td>';
										
										for (var i = 1; i < dataSplit.length; i++) {
											if (i == (dataSplit.length - 1)) {
												var valCol = dataSplit[i].replace("'","").trim();
											} else {
												var valCol = dataSplit[i].split("'")[0].trim();
											}
											
											for (var cont = 0; cont < arrayCodUf.length; cont++) {
												if (arrayCodUf[cont]['uf'] == valCol && data.indexOf('br_ufs') > -1) {
													var codUf = arrayCodUf[cont]['codUf'];
													line1 += '<td style="width: 50%;">Código</td>';
													line2 += '<td>'+codUf+'</td>';
												}
											}
											
											if (codUf) {
												line1 += '<td style="width: 50%;">UF</td>';
												line2 += '<td>'+valCol+'</td>';
											} else {
												if (data.indexOf('br_ufs') <= -1) {
													if (dataSplit.length == 2) {
														for (var j = 0; j < arrayLayers1Col.length; j++) {
															if (data.indexOf("'"+arrayLayers1Col[j]['layer']+"'") > -1) {
																line1 += '<td style="width: 100%;">'+arrayLayers1Col[j]['colName']+'</td>';
																line2 += '<td>'+valCol+'</td>';
															}
														}
													} else {
														for (var j = 0; j < arrayLayersNcol.length; j++) {
															if (data.indexOf(arrayLayersNcol[j]['layer']) > -1) {
																for (var k = 0; k < arrayLayersNcol[j]['colName'].length; k++) {
																	
																	if (count == k) {
																		var col = 'col'+(k+1);
																		
																		var nomeColuna = arrayLayersNcol[j]['colName'][k][col];
																		var tamColuna = parseInt(nomeColuna.length) * 5;
																		var tamColunaVal = parseInt(valCol.length) * 7.5;
																		
																		if (tamColuna > tamColunaVal) {
																			line1 += '<td style="min-width: '+tamColuna+'px; width: '+tamColuna+'px;">'+nomeColuna+'</td>';
																		} else {
																			line1 += '<td style="min-width: '+tamColunaVal+'px;">'+nomeColuna+'</td>';
																		}
																	}
																	
																	count++;
																}
																
																line2 += '<td>'+valCol+'</td>';
															}
														}
													}
												}
											}
										}
										line1 += '</tr>';
										line2 += '</tr>';
										
										resTable += '<b style="font-size: 13px;">'+layerName+'</b>';
										resTable += '<table cellspacing="0" border="1" class="tblResultados">';
										resTable += line1;
										resTable += line2;
										resTable += '</table>';
									}
									
									setTimeout(function(){
										$('#resDialog').html(resTable);
										
										$('#resDialog').dialog({
											width: 500,
											height: 250,
											open: function() {
												$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close');
											}
										}).prev('.ui-dialog-titlebar').css('background','#000000');
										
										$('.ui-dialog-title').css('font-size','15px');
										
										$('body').css('cursor', 'default');
									}, 3000);
								} else {
									setTimeout(function(){
										$('body').css('cursor', 'default');
									}, 3000);
								}
								
							}
						});
					}
				}
			}
		});
	});
	
	$('.td_search').hide();
	
	$('#sel_busca_por').change(function(){
		len_search = 3;
		
		$('#map_search').val('');
		
		if ($(this).val() != "0") {
			type_id = $(this).val();
			$('.td_search').show();
		} else {
			$('.td_search').hide();
		}
	});
	
	$('#btn_consulta').click(function(){
		if (search_val.length >= len_search) {
			if (location_id != null && name_table != null) {
				getExtent(location_id, name_table);
			}
		}
	});
	
	$('.sidepanel').append(selCamadas);
	
	$('.zoom1000').attr('disabled', 'disabled');
	$('.zoom750').attr('disabled', 'disabled');
	$('.zoom400').attr('disabled', 'disabled');
	$('.zoom50').attr('disabled', 'disabled');
	
	$('.zoom4500').css('color', '#CCC');
	$('.zoom1000').css('color', '#CCC');
	$('.zoom750').css('color', '#CCC');
	$('.zoom400').css('color', '#CCC');
	$('.zoom50').css('color', '#CCC');
	
	//Habilita a camada conforme o nível de zoom que o usuário está
	map.on('moveend', function(evt){
		var viewResolution = (view.getResolution());
		
		if (viewResolution < 0.0002113399452229356) {
			$('.zoom50').removeAttr('disabled');
			$('.zoom50').css('color', '');
			$('.zoom400').removeAttr('disabled');
			$('.zoom400').css('color', '');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.0002113399452229356 && viewResolution < 0.0016907195617834848) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').removeAttr('disabled');
			$('.zoom400').css('color', '');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution == 0.0016907195617834848) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution > 0.0016907195617834848 && viewResolution < 0.006762878247133939) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.006762878247133939 && viewResolution < 0.013525756494267878) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').attr('disabled', 'disabled');
			$('.zoom1000').css('color', '#CCC');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.013525756494267878) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').attr('disabled', 'disabled');
			$('.zoom1000').css('color', '#CCC');
			$('.zoom4500').attr('disabled', 'disabled');
			$('.zoom4500').css('color', '#CCC');
		}
	});
	
	//Seta como 500px para remover o atributo que o bootstrap impôs as imagens (max-width: 100%) e, 
	//nesse caso, estava limitando o tamanho da imagem não conseguindo ver nada na legenda
	$('img').css('max-width', '500px');
	
	//Verifica quais inputs estão selecionados
	$('input').each(function(){
		if ($(this).is(':checked')) {
			$(this).parent().children('ul.tree').toggle(300);
		}
	});
	
	//$('.icon').addClass('icon-minus');
	$('.tree-toggler').click(function (event) {
		//Pega a classe completa que o usuário selecionou
		var classTarget = $(this).attr('class');
		
		//Separa a classe do elemento que usuário clicou
		var classSpElemArray = classTarget.split(' ');
		//Passa somente a classe de início 'sp'
		classSpElem = classSpElemArray[0];
		
		//Loop da classe icon para pegar a classe corresponte ao que o usuário clicou
		$('.icon').each(function(){
			
			//Classe que contém icon
			var classSpanIcon = $(this).attr('class');
			
			//Verificação se a classe que contém icon possui a classe que começa com 'sp' 
			//(classe de início 'sp' se refere ao nome do layer que a pessoa selecionou)
			if (classSpanIcon.indexOf(classSpElem) != -1) {
				//Passa para variável a classe de início 'sp' + a clase icon
				classIcon = '.' + classSpElem + '.icon';
			}
		});
		
		//Abre ou esconde o 'corpo' da tree
		$(this).parent().children('ul.tree').toggle(300, function(){
			
			if (classTarget.indexOf('header') != -1) {
				//Ao clicar no header da tree, verifica se o 'corpo' está visível
				if ($(this).is(':hidden')) {
					$(classIcon).removeClass('icon-minus');
					$(classIcon).addClass('icon-plus');
				} else {
					$(classIcon).removeClass('icon-plus');
					$(classIcon).addClass('icon-minus');
				}
			} else {
				//Clicou em alguma das opções do 'corpo' da tree para mostrar a legenda
				if($(this).is(':hidden'))
				{
					$(classIcon).removeClass('icon-minus');
					$(classIcon).addClass('icon-plus');
				}
				else
				{
					$(classIcon).removeClass('icon-plus');
					$(classIcon).addClass('icon-minus');
				}
			}
		});
		
	});
	
	$('input[type="checkbox"]').click(function(){
		
		if ($(this).attr('name') != undefined) {
			var group = 'input[name="' + $(this).attr('name') + '"]';
			
			$('ul').find(group).not(this).attr('checked', false).prop('checked', false);
			
			$(group).each(function(){
				if (!$(this).is(':checked')) {
					var groupItem = $(this).attr('id');
					//Verificação de qual input selecionou
					for (var i = 0; i < arrayElem.length; i++) {
						if (arrayElem[i] == groupItem) {
							layer = arrayLayers[i];
						}
					}
					map.removeLayer(layer);
				}
			});
		}
		
		var layerChecked = $(this).prop('checked');
		var id_item = $(this).attr('id');
		var layer = '';
		
		//Verificação de qual input selecionou
		for (var i = 0; i < arrayElem.length; i++) {
			if (arrayElem[i] == id_item) {
				layer = arrayLayers[i];
			}
		}
		
		if (layerChecked) { //Input está selecionado, adicionar camada no mapa
			map.addLayer(layer);
		} else { //Input não está selecionado, remover camada do mapa
			map.removeLayer(layer);
		}
	});
	
});
