//variablesOsm
var param_location_id = $('#location_id').val();
var param_city_gid = $('#city_gid').val();
var dias_crianca_show = $('#dias_crianca_show').val();
var dias_crianca_ativa = $('#dias_crianca_ativa').val();
var dias_gestante_show = $('#dias_gestante_show').val();
var dias_gestante_ativa = $('#dias_gestante_ativa').val();
var dias_comunidade_show = $('#dias_comunidade_show').val();
var dias_comunidade_ativa = $('#dias_comunidade_ativa').val();
var user_id = $('#user_id').val();
var temporary_table = $('#temporary_table').val();
var box_sequence = $('#box_sequence').val();
var children = ($('#children').val() * 1 == 1) ? 'crianca_visitada' : (($('#children').val() * 1 == 2) ? 'crianca_abandonada' : null);
var babies = ($('#babies').val() * 1 == 1) ? 'bebe_visitado' : (($('#babies').val() * 1 == 2) ? 'bebe_abandonado' : null);
var pregnant = ($('#pregnant').val() * 1 == 1) ? 'gestante_visitada' : (($('#pregnant').val() * 1 == 2) ? 'gestante_abandonada' : null);
var states = ($('#states').val() * 1 == 1) ? $('#states').val() : null;
var time_tmp = $('#time_reload').val() * 1;
var time_reload_tmp = $('#time_reload').val();
var time_reload = (typeof time_tmp == 'number') ? time_tmp * 1000 : 5000;
var users_id = $('#users_id').val();
var num_usuarios_ativos = (!!$('#num_usuarios_ativos').val()) ? $('#num_usuarios_ativos').val() : 0;
var layerAppvisita = null;
localStorage.setItem('temporary_table', $('#temporary_table').val());
var interval_days = '';
var age = '';
var geo_point = $('#geo_point').val();
var box_geo_point = $('#box_geo_point').val();
var reference_time = '';

var pathname = $('#pathname').val();
if (pathname.indexOf('controllers') != -1) {
    pathname = pathname.replace('controllers', '');
}

var urlOrigin = window.location.origin;
var origin = urlOrigin;
//var localMapfile = '/wmspc?map=/var/www/application/modules/map/mapfiles/';
var localMapfile = '/wmspc?map='+pathname+'/map/mapfiles/';
var nameLayer = '&layer=';
var getLegend = '&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=png';

var extent = '';

var result = null;
var type_id = 0;
var len_search = 0;

var location_id = null;
var name_table = null;
var result_search = null;
var search_val = '';

var initialZoom = 4.7;

var layerHighlight = null;

var selCamadas = 
'<ul class="nav nav-list">'+
'<li>'+ //Pastoral da Criança
    '<span class="spPC icon icon-minus tree-toggler header"></span>'+
    '<span class="spPC tree-toggler tree-header">  Pastoral da Criança</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spSetores icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerSetores" layerName="Setores">&thinsp;'+
            '<span class="spSetores tree-toggler">Setores</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_setores.map'+nameLayer+'setores'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spRamos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerRamos" layerName="Ramos">&thinsp;'+
            '<span class="spRamos tree-toggler">Ramos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_ramos.map'+nameLayer+'ramos'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spComAtivas icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerComunidadesPontoMedio" layerName="Comunidades ativas e ponto médio" class="zoom400">&thinsp;'+
            '<span class="spComAtivas tree-toggler zoom400">Comunidades ativas e ponto médio</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_comunidades_ponto_medio.map'+nameLayer+'comunidades_ponto_medio'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spHortaCasa icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerHortaCasa" layerName="Crianças com horta em casa" class="zoom400">&thinsp;'+
            '<span class="spHortaCasa tree-toggler zoom400">Crianças com horta em casa</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_horta_em_casa.map'+nameLayer+'horta'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spPontosCriancas icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerPontosCriancas" layerName="Pontos crianças" class="zoom4500">&thinsp;'+
            '<span class="spPontosCriancas tree-toggler zoom4500">Pontos crianças</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_pontos_criancas.map'+nameLayer+'pontos_criancas'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spPontosGestantes icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerPontosGestantes" layerName="Pontos gestantes" class="zoom4500">&thinsp;'+
            '<span class="spPontosGestantes tree-toggler zoom4500">Pontos gestantes</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_pontos_gestantes.map'+nameLayer+'pontos_gestantes'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spPontosBebes icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerPontosBebes" layerName="Pontos bebês" class="zoom4500">&thinsp;'+
            '<span class="spPontosBebes tree-toggler zoom4500">Pontos bebês</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_pontos_bebes.map'+nameLayer+'pontos_bebes'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+ 
            '<span class="spGraficoPontos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerGraficoPontos" layerName="Crianças, bebês e gestantes - detalhes em gráficos">&thinsp;'+
            '<span class="spGraficoPontos tree-toggler">Crianças, bebês e gestantes - detalhes em gráficos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_grafico_pontos.map'+nameLayer+'grafico_pontos_estado'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //CNBB
    '<span class="spCNBB icon icon-minus tree-toggler header"></span>'+
    '<span class="spCNBB tree-toggler tree-header">  CNBB</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spDioceses icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerDioceses" layerName="Dioceses">&thinsp;'+
            '<span class="spDioceses tree-toggler">Dioceses</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_dioceses.map'+nameLayer+'dioceses'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spParoquias icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerParoquias" layerName="Paroquias" class="zoom4500">&thinsp;'+
            '<span class="spParoquias tree-toggler zoom4500">Paroquias</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_paroquias.map'+nameLayer+'paroquias'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spSedesParoquias icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerSedesParoquias" layerName="Sedes das Paróquias">&thinsp;'+
            '<span class="spSedesParoquias tree-toggler">Sedes das Paróquias</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_sedes_paroquias.map'+nameLayer+'sedes_paroquias'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Indicadores da Pastoral
    '<span class="spIndicPastoral icon icon-minus tree-toggler header"></span>'+
    '<span class="spIndicPastoral tree-toggler tree-header">  Indicadores da Pastoral</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spSulfatoFerroso icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerSulfatoFerroso" layerName="Suplemento de Sulfato Ferroso" class="zoom4500">&thinsp;'+
            '<span class="spSulfatoFerroso tree-toggler zoom4500">Suplemento de Sulfato Ferroso</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_supl_sulfato_ferroso.map'+nameLayer+'sulfato_ferroso_municipio'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spAcidoFolico icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerAcidoFolico" layerName="Suplemento de Ácido Fólico" class="zoom4500">&thinsp;'+
            '<span class="spAcidoFolico tree-toggler zoom4500">Suplemento de Ácido Fólico</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_supl_acido_folico.map'+nameLayer+'acido_folico_municipio'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Censo IBGE 2010
    '<span class="spCenso2010 icon icon-minus tree-toggler header"></span>'+
    '<span class="spCenso2010 tree-toggler tree-header">  Censo IBGE 2010</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spMenor1Ano2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerMenores1Ano2010" name="mutualDisable" layerName="Crianças menores de 1 ano" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spMenor1Ano2010 tree-toggler zoom400">Crianças menores de 1 ano</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_menores_1_ano_2010.map'+nameLayer+'criancas_menores_1_2010'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spCriancas0a6anos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCriancas0a6Anos2010" name="mutualDisable" layerName="Crianças de 0 a 6 anos incompletos" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spCriancas0a6anos tree-toggler zoom400">Crianças de 0 a 6 anos incompletos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_criancas_0_a_6_anos.map'+nameLayer+'criancas_0_6_2010'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spPobres0a6anos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCriancasPobres2010" name="mutualDisable" layerName="Crianças pobres de 0 a 6 anos" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spPobres0a6anos tree-toggler zoom400">Crianças pobres de 0 a 6 anos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_criancas_pobres.map'+nameLayer+'criancas_pobres_0_6_2010'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spMiseraveis0a6anos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCriancasMiseraveis2010" name="mutualDisable" layerName="Crianças miseráveis de 0 a 6 anos" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spMiseraveis0a6anos tree-toggler zoom400">Crianças miseráveis de 0 a 6 anos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<li><img src="'+origin+localMapfile+'brasil_criancas_miseraveis.map'+nameLayer+'criancas_miseraveis_0_6_2010'+getLegend+'"></li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spRendaCrianca icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerRendaCrianca2010" layerName="% de crianças de 0 a 6 anos em faixas de renda">&thinsp;'+
            '<span class="spRendaCrianca tree-toggler">% de crianças de 0 a 6 anos em faixas de renda</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_renda_crianca.map'+nameLayer+'renda_crianca_setor_censitario'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spAbastAgua2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerAbastAgua2010" name="mutualDisable" layerName="Água - Percentual de domicílios abastecidos pela rede geral">&thinsp;'+
            '<span class="spAbastAgua2010 tree-toggler">Água - Percentual de domicílios abastecidos pela rede geral</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2010.map'+nameLayer+'abastecimento_agua_2010_setor_censitario'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spAbastAguaPie2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerAbastAguaPie2010" layerName="Água - Detalhes do abastecimento em gráficos" class="zoom50">&thinsp;'+
            '<span class="spAbastAguaPie2010 tree-toggler zoom50">Água - Detalhes do abastecimento em gráficos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2010_pie.map'+nameLayer+'abastecimento_agua_2010_pie'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spEsgotSanit2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerEsgotSanit2010" layerName="Esgotamento sanitário - Indicador em áreas">&thinsp;'+
            '<span class="spEsgotSanit2010 tree-toggler">Esgotamento sanitário - Indicador em áreas</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_esgotamento_sanitario_2010.map'+nameLayer+'esgotamento_sanitario_2010_area_setor_censitario'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spEsgotSanitPie2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerEsgotSanitPie2010" layerName="Esgotamento sanitário - Detalhes em gráficos" class="zoom50">&thinsp;'+
            '<span class="spEsgotSanitPie2010 tree-toggler zoom50">Esgotamento sanitário - Detalhes em gráficos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_esgotamento_sanitario_2010.map'+nameLayer+'esgotamento_sanitario_2010_pie'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spDestLixo2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerDestLixo2010" layerName="Destino do lixo - Indicador em áreas">&thinsp;'+
            '<span class="spDestLixo2010 tree-toggler">Destino do lixo - Indicador em áreas</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_destino_lixo_2010.map'+nameLayer+'destino_lixo_2010_area_setor_censitario'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spDestLixoPie2010 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerDestLixoPie2010" layerName="Destino do lixo - Detalhes em gráficos" class="zoom50">&thinsp;'+
            '<span class="spDestLixoPie2010 tree-toggler zoom50">Destino do lixo - Detalhes em gráficos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_destino_lixo_2010_pie.map'+nameLayer+'destino_lixo_2010_pie'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spPropCatolicos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerPropCatolicos2010" layerName="% de católicos em relação a outras religiões e sem religião">&thinsp;'+
            '<span class="spPropCatolicos tree-toggler">% de católicos em relação a outras religiões e sem religião</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_prop_catolicos.map'+nameLayer+'prop_catholics_setor_censitario'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Político-administrativo
    '<span class="spPolAdmin icon icon-minus tree-toggler header"></span>'+
    '<span class="spPolAdmin tree-toggler tree-header">  Político-administrativo</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spEstados icon icon-minus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerEstados" layerName="Estados brasileiros" checked>&thinsp;'+
            '<span class="spEstados tree-toggler">Estados brasileiros</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_estados.map'+nameLayer+'br_ufs'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spMunicipios icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerMunicipios" layerName="Municípios" class="zoom4500">&thinsp;'+
            '<span class="spMunicipios tree-toggler zoom4500">Municípios</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_municipios.map'+nameLayer+'municipios_poligono'+getLegend+'">'+
                '<li>'+
                    '<span class="spSedesMunicipais icon icon-plus tree-toggler"></span>'+
                    '<input type="checkbox" id="elemlayerSedesMunicipais" layerName="Sedes Municipais" class="zoom400">&thinsp;'+
                    '<span class="spSedesMunicipais tree-toggler zoom400">Sedes Municipais</span>'+
                    '<ul class="nav nav-list tree" style="display: none;">'+
                        '<img src="'+origin+localMapfile+'brasil_sedes_municipais.map'+nameLayer+'sedes_municipais'+getLegend+'">'+
                    '</ul>'+
                '</li>'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spCapitais icon icon-minus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCapitais" layerName="Capitais" checked>&thinsp;'+
            '<span class="spCapitais tree-toggler">Capitais</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_capitais.map'+nameLayer+'capitais'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Saúde
    '<span class="spSaude icon icon-minus tree-toggler header"></span>'+
    '<span class="spSaude tree-toggler tree-header">  Saúde</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spUnidSaude icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerUnidadesSaude" layerName="Unidades de Saúde" class="zoom400">&thinsp;'+
            '<span class="spUnidSaude tree-toggler zoom400">Unidades de Saúde</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_unidades_saude.map'+nameLayer+'UnidadesSaude'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Serviços de Mapas
    '<span class="spServMapas icon icon-minus tree-toggler header"></span>'+
    '<span class="spServMapas tree-toggler tree-header">  Serviços de Mapas</span>'+
    '<ul class="nav nav-list tree">'+
        '<li><input type="checkbox" id="elemLayerOSM" checked>OpenStreetMap</li>'+
        '<li><input type="checkbox" id="elemLayerCigex">Cigex</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Projeto 1.000.000 de cisternas
    '<span class="spProjCisternas icon icon-minus tree-toggler header"></span>'+
    '<span class="spProjCisternas tree-toggler tree-header">  Projeto 1.000.000 de cisternas</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spCisternasDentroMunic icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCisternasDentroMunic" layerName="Cisternas Dentro do Município Declarado" class="zoom400">&thinsp;'+
            '<span class="spCisternasDentroMunic tree-toggler zoom400">Cisternas Dentro do Município Declarado</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_cisternas_dentro_munic.map'+nameLayer+'cisternasDentroMunic'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spCisternasForaMunic icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCisternasForaMunic" layerName="Cisternas Fora do Município Declarado" class="zoom400">&thinsp;'+
            '<span class="spCisternasForaMunic tree-toggler zoom400">Cisternas Fora do Município Declarado</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_cisternas_fora_munic.map'+nameLayer+'cisternasForaMunic'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'<li>'+ //Censo IBGE 2000
    '<span class="spCenso2000 icon icon-minus tree-toggler header"></span>'+
    '<span class="spCenso2000 tree-toggler tree-header">  Censo IBGE 2000</span>'+
    '<ul class="nav nav-list tree">'+
        '<li>'+
            '<span class="spAbastAgua2000 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerAbastAgua2000" layerName="Abastecimento de água" class="zoom400">&thinsp;'+
            '<span class="spAbastAgua2000 tree-toggler zoom400">Abastecimento de água</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_abastecimento_agua_2000.map'+nameLayer+'abastecimento_agua_2000'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spMenor1Ano2000 icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerMenores1Ano2000" name="mutualDisable" layerName="Criancas menores de 1 ano" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spMenor1Ano2000 tree-toggler zoom400">Criancas menores de 1 ano</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_menores_1_ano_2000.map'+nameLayer+'criancasmenos1'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spCriancas1a5anos icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerCriancas1a5Anos2000" name="mutualDisable" layerName="Criancas de 1 a 5 anos" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spCriancas1a5anos tree-toggler zoom400">Criancas de 1 a 5 anos</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_criancas_1_a_5_anos.map'+nameLayer+'criancas1a5'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spAnosEstudo icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerAnosEstudo2000" name="mutualDisable" layerName="Média de anos de estudo" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spAnosEstudo tree-toggler zoom400">Média de anos de estudo</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_anos_estudo.map'+nameLayer+'anosestudo'+getLegend+'">'+
            '</ul>'+
        '</li>'+
        '<li>'+
            '<span class="spRendaFamilia icon icon-plus tree-toggler"></span>'+
            '<input type="checkbox" id="elemlayerRendaFamilia2000" name="mutualDisable" layerName="Media de renda do chefe da familia (salário mínimo R$151,00)" class="zoom400">&thinsp;'+ //atributo name
            '<span class="spRendaFamilia tree-toggler zoom400">Media de renda do chefe da familia (salário mínimo R$151,00)</span>'+
            '<ul class="nav nav-list tree" style="display: none;">'+
                '<img src="'+origin+localMapfile+'brasil_renda_familia.map'+nameLayer+'rendafamilia'+getLegend+'">'+
            '</ul>'+
        '</li>'+
    '</ul>'+
'</li>'+
'</ul>';


//layersOsm
/** PASTORAL DA CRIANÇA **/

var tileSetores = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_setores.map',
    params: {
        'LAYERS': 'setores',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileRamos = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_ramos.map',
    params: {
        'LAYERS': 'ramos',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileComunidadesPontoMedio = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_comunidades_ponto_medio.map',
    params: {
        'LAYERS': 'comunidades_ponto_medio',
        'dias_comunidade_show': (dias_comunidade_show == '' ? '' : dias_comunidade_show),
        'dias_comunidade_ativa': (dias_comunidade_ativa == '' ? '' : dias_comunidade_ativa),
        'user_id': (user_id == '' ? '' : user_id),
        'temporary_table': (localStorage.getItem('temporary_table') == '' ? '' : localStorage.getItem('temporary_table')),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileHortaCasa = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_horta_em_casa.map',
    params: {
        'LAYERS': 'horta',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tilePontosCriancas = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_pontos_criancas.map',
    params: {
        'LAYERS': 'pontos_criancas',
        'dias_crianca_show': (dias_crianca_show == '' ? '' : dias_crianca_show),
        'dias_crianca_ativa': (dias_crianca_ativa == '' ? '' : dias_crianca_ativa),
        'user_id': (user_id == '' ? '' : user_id),
        'location_id': (param_location_id == '' ? '' : param_location_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tilePontosGestantes = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_pontos_gestantes.map',
    params: {
        'LAYERS': 'pontos_gestantes',
        'dias_gestante_show': (dias_gestante_show == '' ? '' : dias_gestante_show),
        'dias_gestante_ativa': (dias_gestante_ativa == '' ? '' : dias_gestante_ativa),
        'user_id': (user_id == '' ? '' : user_id),
        'location_id': (param_location_id == '' ? '' : param_location_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tilePontosBebes = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_pontos_bebes.map',
    params: {
        'LAYERS': 'pontos_bebes',
        'dias_crianca_show': (dias_crianca_show == '' ? '' : dias_crianca_show),
        'dias_crianca_ativa': (dias_crianca_ativa == '' ? '' : dias_crianca_ativa),
        'user_id': (user_id == '' ? '' : user_id),
        'location_id': (param_location_id == '' ? '' : param_location_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileGraficoPontosComunidade = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_grafico_pontos.map',
    params: {
        'LAYERS': 'grafico_pontos_comunidade',
        'interval_days': (interval_days == '' ? '' : interval_days),
        'age': (age == '' ? '' : age),
        'user_id': (user_id == '' ? '' : user_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileGraficoPontosEstado = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_grafico_pontos.map',
    params: {
        'LAYERS': 'grafico_pontos_estado',
        'interval_days': (interval_days == '' ? '' : interval_days),
        'age': (age == '' ? '' : age),
        'user_id': (user_id == '' ? '' : user_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileGraficoPontosSetor = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_grafico_pontos.map',
    params: {
        'LAYERS': 'grafico_pontos_setor',
        'interval_days': (interval_days == '' ? '' : interval_days),
        'age': (age == '' ? '' : age),
        'user_id': (user_id == '' ? '' : user_id),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** CNBB **/

var tileDioceses = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_dioceses.map',
    params: {
        'LAYERS': 'dioceses',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileParoquias = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_paroquias.map',
    params: {
        'LAYERS': 'paroquias',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileSedesParoquias = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_sedes_paroquias.map',
    params: {
        'LAYERS': 'sedes_paroquias',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** INDICADORES DA PASTORAL **/

var tileSulfatoFerrosoMunicipio = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_supl_sulfato_ferroso.map',
    params: {
        'LAYERS': 'sulfato_ferroso_municipio',
        'reference_time': (reference_time == '' ? '' : reference_time),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileSulfatoFerrosoParoquia = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_supl_sulfato_ferroso.map',
    params: {
        'LAYERS': 'sulfato_ferroso_paroquia',
        'reference_time': (reference_time == '' ? '' : reference_time),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileAcidoFolicoMunicipio = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_supl_acido_folico.map',
    params: {
        'LAYERS': 'acido_folico_municipio',
        'reference_time': (reference_time == '' ? '' : reference_time),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileAcidoFolicoParoquia = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_supl_acido_folico.map',
    params: {
        'LAYERS': 'acido_folico_paroquia',
        'reference_time': (reference_time == '' ? '' : reference_time),
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** CENSO IBGE 2010 **/

var tileMenores1Ano2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_menores_1_ano_2010.map',
    params: {
        'LAYERS': 'criancas_menores_1_2010',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCriancas0a6Anos2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_criancas_0_a_6_anos.map',
    params: {
        'LAYERS': 'criancas_0_6_2010',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCriancasPobres2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_criancas_pobres.map',
    params: {
        'LAYERS': 'criancas_pobres_0_6_2010',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCriancasMiseraveis2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_criancas_miseraveis.map',
    params: {
        'LAYERS': 'criancas_miseraveis_0_6_2010',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileRendaCrianca2010Munic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_renda_crianca.map',
    params: {
        'LAYERS': 'renda_crianca_municipio',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileRendaCrianca2010SetorCens = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_renda_crianca.map',
    params: {
        'LAYERS': 'renda_crianca_setor_censitario', //MAXSCALEDENOM
        'FORMAT': 'image/png'
    },
    serverType: 'mapserver'
});

var tileAbastAguaMunic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010.map',
    params: {
        'LAYERS': 'abastecimento_agua_2010_municipio',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileAbastAguaSetorCens = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010.map',
    params: {
        'LAYERS': 'abastecimento_agua_2010_setor_censitario', //MAXSCALEDENOM
        'FORMAT': 'image/png'
    },
    serverType: 'mapserver'
});

var tileAbastAguaPie2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010_pie.map',
    params: {
        'LAYERS': 'abastecimento_agua_2010_pie',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileEsgotSanitAreaMunic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
    params: {
        'LAYERS': 'esgotamento_sanitario_2010_area_municipio',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileEsgotSanitSetorCens = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
    params: {
        'LAYERS': 'esgotamento_sanitario_2010_area_setor_censitario', //MAXSCALEDENOM
        'FORMAT': 'image/png'
    },
    serverType: 'mapserver'
});

var tileEsgotSanitPie2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
    params: {
        'LAYERS': 'esgotamento_sanitario_2010_pie',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileDestLixo2010Munic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_destino_lixo_2010.map',
    params: {
        'LAYERS': 'destino_lixo_2010_area_municipio',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileDestLixo2010SetorCens = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_destino_lixo_2010.map',
    params: {
        'LAYERS': 'destino_lixo_2010_area_setor_censitario', //MAXSCALEDENOM
        'FORMAT': 'image/png'
    },
    serverType: 'mapserver'
});

var tileDestLixoPie2010 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_destino_lixo_2010_pie.map',
    params: {
        'LAYERS': 'destino_lixo_2010_pie',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tilePropCatolicos2010Munic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_prop_catolicos.map',
    params: {
        'LAYERS': 'prop_catholics_municipio',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tilePropCatolicos2010SetorCens = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_prop_catolicos.map',
    params: {
        'LAYERS': 'prop_catholics_setor_censitario', //MAXSCALEDENOM
        'FORMAT': 'image/png'
    },
    serverType: 'mapserver'
});

/** POLÍTICO-ADMINISTRATIVO **/

var tileEstados = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_estados.map',
    params: {
        'LAYERS': 'br_ufs',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileMunicipios = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_municipios.map',
    params: {
        'LAYERS': 'municipios_poligono',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileSedesMunicipais = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_sedes_municipais.map',
    params: {
        'LAYERS': 'sedes_municipais',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCapitais = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_capitais.map',
    params: {
        'LAYERS': 'capitais',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** SAÚDE **/

var tileUnidadesSaude = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_unidades_saude.map',
    params: {
        'LAYERS': 'UnidadesSaude',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** PROJETO 1.000.000 DE CISTERNAS **/

var tileCisternasDentroMunic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_cisternas_dentro_munic.map',
    params: {
        'LAYERS': 'cisternasDentroMunic',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCisternasForaMunic = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_cisternas_fora_munic.map',
    params: {
        'LAYERS': 'cisternasForaMunic',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

/** CENSO IBGE 2000 **/

var tileAbastAgua2000 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2000.map',
    params: {
        'LAYERS': 'abastecimento_agua_2000',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileMenores1Ano2000 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_menores_1_ano_2000.map',
    params: {
        'LAYERS': 'criancasmenos1',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileCriancas1a5Anos2000 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_criancas_1_a_5_anos.map',
    params: {
        'LAYERS': 'criancas1a5',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileAnosEstudo2000 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_anos_estudo.map',
    params: {
        'LAYERS': 'anosestudo',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});

var tileRendaFamilia2000 = new ol.source.TileWMS({
    url: urlOrigin+localMapfile+'brasil_renda_familia.map',
    params: {
        'LAYERS': 'rendafamilia',
        'FORMAT': 'image/png',
        TRANSPARENT: true,
        'TILED': true
    },
    serverType: 'mapserver'
});


//Serviço de Mapas (OpenStreetMap)
var layerOSM = new ol.layer.Tile({
    source: new ol.source.OSM(),
    preload: Infinity
});

//Pastoral da Criança (Setores)
var layerSetores = new ol.layer.Tile({
    source: tileSetores,
    preload: Infinity
});

//Pastoral da Criança (Ramos)
var layerRamos = new ol.layer.Tile({
    source: tileRamos,
    preload: Infinity
});

//Pastoral da Criança (Comunidades ativas)
var layerComunidadesPontoMedio = new ol.layer.Tile({
    source: tileComunidadesPontoMedio,
    preload: Infinity
});

//Pastoral da Criança (Crianças com horta em casa)
var layerHortaCasa = new ol.layer.Tile({
    source: tileHortaCasa,
    preload: Infinity
});

//Pastoral da Criança (Pontos crianças)
var layerPontosCriancas = new ol.layer.Tile({
    source: tilePontosCriancas,
    preload: Infinity
});

//Pastoral da Criança (Pontos gestantes)
var layerPontosGestantes = new ol.layer.Tile({
    source: tilePontosGestantes,
    preload: Infinity
});

//Pastoral da Criança (Pontos bebês)
var layerPontosBebes = new ol.layer.Tile({
    source: tilePontosBebes,
    preload: Infinity
});

//Pastoral da Criança (Crianças, bebês e gestantes - detalhes em gráficos)
var layerGraficoPontos = new ol.layer.Group({
    title: 'GraficoPontos',
    layers: [new ol.layer.Tile({
            source: tileGraficoPontosComunidade,
            preload: Infinity
        }),
        new ol.layer.Tile({
            source: tileGraficoPontosSetor,
            preload: Infinity
        }),
        new ol.layer.Tile({
            source: tileGraficoPontosEstado,
            preload: Infinity
        })
    ]
});

//Pastoral da Criança (Indicadores)
var layerSulfatoFerroso = new ol.layer.Group({
    title: 'SulfatoFerroso',
    layers: [new ol.layer.Tile({
            source: tileSulfatoFerrosoMunicipio,
            preload: Infinity
        }),
        new ol.layer.Tile({
            source: tileSulfatoFerrosoParoquia,
            preload: Infinity
        })
    ]
});

var layerAcidoFolico = new ol.layer.Group({
    title: 'AcidoFolico',
    layers: [new ol.layer.Tile({
            source: tileAcidoFolicoMunicipio,
            preload: Infinity
        }),
        new ol.layer.Tile({
            source: tileAcidoFolicoParoquia,
            preload: Infinity
        })
    ]
});

//CNBB (Dioceses)
var layerDioceses = new ol.layer.Tile({
    source: tileDioceses,
    preload: Infinity
});

//CNBB (Paroquias)
var layerParoquias = new ol.layer.Tile({
    source: tileParoquias,
    preload: Infinity
});

//CNBB (Sedes das Paroquias)
var layerSedesParoquias = new ol.layer.Tile({
    source: tileSedesParoquias,
    preload: Infinity
});

//Censo IBGE 2010 (Crianças menores de 1 ano)
var layerMenores1Ano2010 = new ol.layer.Tile({
    source: tileMenores1Ano2010,
    preload: Infinity
});

//Censo IBGE 2010 (Crianças de 0 a 6 anos incompletos)
var layerCriancas0a6Anos2010 = new ol.layer.Tile({
    source: tileCriancas0a6Anos2010,
    preload: Infinity
});

//Censo IBGE 2010 (Crianças Pobres de 0 a 6 anos)
var layerCriancasPobres2010 = new ol.layer.Tile({
    source: tileCriancasPobres2010,
    preload: Infinity
});

//Censo IBGE 2010 (Crianças miseráveis de 0 a 6 anos)
var layerCriancasMiseraveis2010 = new ol.layer.Tile({
    source: tileCriancasMiseraveis2010,
    preload: Infinity
});
                    
//Censo IBGE 2010 (% de crianças de 0 a 6 anos em faixas de renda)
var layerRendaCrianca2010 = new ol.layer.Group({
    title: 'RendaCrianca2010',
    layers: [new ol.layer.Tile({
        source: tileRendaCrianca2010Munic,
        preload: Infinity
        }),
        new ol.layer.Tile({
        source: tileRendaCrianca2010SetorCens,
        preload: Infinity
        })
    ]
});

//Censo IBGE 2010 (Água - Percentual de domicílios abastecidos pela rede geral)
var layerAbastAgua2010 = new ol.layer.Group({
    title: 'AbastAgua2010',
    layers: [new ol.layer.Tile({
        source: tileAbastAguaMunic,
        preload: Infinity
        }),
        new ol.layer.Tile({
        source: tileAbastAguaSetorCens,
        preload: Infinity
        })
    ]
});

//Censo IBGE 2010 (Água - Detalhes do abastecimento em gráficos)
var layerAbastAguaPie2010 = new ol.layer.Tile({
    source: tileAbastAguaPie2010,
    preload: Infinity
});

//Censo IBGE 2010 (Esgotamento sanitário - Indicador em áreas)
var layerEsgotSanit2010 = new ol.layer.Group({
    title: 'EsgotSanit2010',
    layers: [new ol.layer.Tile({
        source: tileEsgotSanitAreaMunic,
        preload: Infinity
        }),
        new ol.layer.Tile({
        source: tileEsgotSanitSetorCens,
        preload: Infinity
        })
    ]
});

//Censo IBGE 2010 (Esgotamento sanitário - Detalhes em gráficos)
var layerEsgotSanitPie2010 = new ol.layer.Tile({
    source: tileEsgotSanitPie2010
});

//Censo IBGE 2010 (Destino do lixo - Indicador em áreas)
var layerDestLixo2010 = new ol.layer.Group({
    title: 'DestLixo2010',
    layers: [new ol.layer.Tile({
        source: tileDestLixo2010Munic,
        preload: Infinity
        }),
        new ol.layer.Tile({
        source: tileDestLixo2010SetorCens,
        preload: Infinity
        })
    ]
});

//Censo IBGE 2010 (Destino do lixo - Detalhes em gráficos)
var layerDestLixoPie2010 = new ol.layer.Tile({
    source: tileDestLixoPie2010
});

//Censo IBGE 2010 (% de católicos em relação a outras religiões e sem religião)
var layerPropCatolicos2010 = new ol.layer.Group({
    title: 'PropCatolicos2010',
    layers: [new ol.layer.Tile({
        source: tilePropCatolicos2010Munic,
        preload: Infinity
        }),
        new ol.layer.Tile({
        source: tilePropCatolicos2010SetorCens,
        preload: Infinity
        })
    ]
});

//Politico-administrativo (Estados brasileiros)
var layerEstados = new ol.layer.Tile({
    source: tileEstados,
    preload: Infinity
});

//Politico-administrativo (Municipios)
var layerMunicipios = new ol.layer.Tile({
    source: tileMunicipios,
    preload: Infinity
});

//Politico-administrativo (Sedes municipais)
var layerSedesMunicipais = new ol.layer.Tile({
    source: tileSedesMunicipais,
    preload: Infinity
});

//Politico-administrativo (Capitais)
var layerCapitais = new ol.layer.Tile({
    source: tileCapitais,
    preload: Infinity
});

//Saúde (Unidades de Saúde)
var layerUnidadesSaude = new ol.layer.Tile({
    source: tileUnidadesSaude,
    preload: Infinity
});

//Serviço de Mapas (Cigex)
var layerCigex = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        url: 'http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/baseraster.map',
        params: {
            'LAYERS': 'baseraster',
            'FORMAT': 'image/png'
        },
        serverType: 'mapserver'
    }),
    preload: Infinity
});

//Projeto 1.000.000 de cisternas (Cisternas Dentro do Município Declarado)
var layerCisternasDentroMunic = new ol.layer.Tile({
    source: tileCisternasDentroMunic,
    preload: Infinity
});

//Projeto 1.000.000 de cisternas (Cisternas Fora do Município Declarado)
var layerCisternasForaMunic = new ol.layer.Tile({
    source: tileCisternasForaMunic,
    preload: Infinity
});

//Censo IBGE 2000 (Abastecimento de água)
var layerAbastAgua2000 = new ol.layer.Tile({
    source: tileAbastAgua2000,
    preload: Infinity
});

//Censo IBGE 2000 (Criancas menores de 1 ano)
var layerMenores1Ano2000 = new ol.layer.Tile({
    source: tileMenores1Ano2000,
    preload: Infinity
});

//Censo IBGE 2000 (Criancas de 1 a 5 anos)
var layerCriancas1a5Anos2000 = new ol.layer.Tile({
    source: tileCriancas1a5Anos2000,
    preload: Infinity
});

//Censo IBGE 2000 (Média de anos de estudo)
var layerAnosEstudo2000 = new ol.layer.Tile({
    source: tileAnosEstudo2000,
    preload: Infinity
});

//Censo IBGE 2000 (Media de renda do chefe da familia (salário mínimo R$151,00))
var layerRendaFamilia2000 = new ol.layer.Tile({
    source: tileRendaFamilia2000,
    preload: Infinity
});

//functionsOsm
function delay(callback, ms) {
	var timer = 0;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
			callback.apply(context, args);
		}, ms || 0);
	};
}

function getExtent(location_id, name_table) {
	
	$.ajax({ 
		url:        '/map/ol52/getlocal', 
		type:       'POST',
		dataType:   'text',
		data:		{ location_id: location_id, name_table: name_table },
		success: function(data) { 
			var retorno = data.split('@');
			data = retorno[0];
			var sequence_map = retorno[1];
			
			//Retira 'BOX', '(', ')' e ',' do resultado 
			data = data.replace('BOX','');
			data = data.replace('(','');
			data = data.replace(')','');
			data = data.replace(',',' ');
			
			extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
			extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
			extent = JSON.parse(extent); //Transforma extent em array
			
			if (layerHighlight) {
				map.removeLayer(layerHighlight);
				layerHighlight = null;
			}
			
			layerHighlight = new ol.layer.Tile({
				source: new ol.source.TileWMS({
					url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
					params: {
						'LAYERS': 'highlight_element',
						'FORMAT': 'image/png',
						TRANSPARENT: true,
						'TILED': true
					},
					serverType: 'mapserver'
				}),
				preload: Infinity
			});
			
			//Seta index
			layerHighlight.setZIndex(3);
			
			map.addLayer(layerHighlight);
			
			view.fit(extent); //Ajusta a extent na borda do mapa
		}, 
		error : function(xhr, textStatus, errorThrown) { 
			console.log('Ajax request failed.'); 
		}
	});
	
}

function redirectExtent(box_sequence, layer_children, layer_babies, layer_pregnant, states) {
	var retorno = box_sequence.split('@');
	box_sequence = retorno[0];
	var sequence_map = retorno[1];
	
	//Retira 'BOX', '(', ')' e ',' do resultado 
	box_sequence = box_sequence.replace('BOX','');
	box_sequence = box_sequence.replace('(','');
	box_sequence = box_sequence.replace(')','');
	box_sequence = box_sequence.replace(',',' ');
	
	extent = box_sequence.replace(/ /g,', '); //Troca todos os espaços por ', '
	extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
	extent = JSON.parse(extent); //Transforma extent em array
	
	if (layerHighlight) {
		map.removeLayer(layerHighlight);
		layerHighlight = null;
	}
	
	layerHighlight = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
			params: {
				'LAYERS': 'highlight_element',
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	
	if (layer_children != null) {
		var layerCriancas = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_criancas.map',
				params: {
					'LAYERS': layer_children,
                    'location_id': (param_location_id == '' ? '' : param_location_id),
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerCriancas.setZIndex(4);
	}
	
	if (layer_babies != null) {
		var layerBebes = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_bebes.map',
				params: {
					'LAYERS': layer_babies,
                    'location_id': (param_location_id == '' ? '' : param_location_id),
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerBebes.setZIndex(4);
	}
	
	if (layer_pregnant != null) {
		var layerGestantes = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_gestantes.map',
				params: {
					'LAYERS': layer_pregnant,
                    'location_id': (param_location_id == '' ? '' : param_location_id),
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerGestantes.setZIndex(4);
	}
		
	//Seta index
	layerHighlight.setZIndex(3);
	
	map.addLayer(layerHighlight);
	if (layer_pregnant != null) 
		map.addLayer(layerGestantes);
	
	if (layer_children != null) 
		map.addLayer(layerCriancas);
	
	if (layer_babies != null) 
		map.addLayer(layerBebes);
	
	if (states == 1) 
		map.addLayer(layerEstados);

	view.fit(extent); //Ajusta a extent na borda do mapa
}

function getExtentCommunity(location_id, city_gid) {
	
	$.ajax({ 
		url:        '/map/ol52/getlocal-community', 
		type:       'POST',
		dataType:   'text',
		data:		{ location_id: location_id, city_gid: city_gid },
		success: function(data) { 
			if (data.charAt(0) != '@') {
				var retorno = data.split('@');
				data = retorno[0];
				var sequence_map = retorno[1];
				
				//Retira 'BOX', '(', ')' e ',' do resultado 
				data = data.replace('BOX','');
				data = data.replace('(','');
				data = data.replace(')','');
				data = data.replace(',',' ');
				
				extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
				extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
				extent = JSON.parse(extent); //Transforma extent em array
				
				var datas = data.split(' ');
				
				var new_extent = '';
				
				//var geo1 = (parseFloat(datas[0]) - 0.008).toPrecision(8);
				var geo1 = datas[0];
				var geo2 = datas[1];
				var geo3 = datas[2];
				var geo4 = datas[3];
				
				new_extent += geo1 + ', ';
				new_extent += geo2 + ', ';
				new_extent += geo3 + ', ';
				new_extent += geo4;
				
				extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
				extent = '['+new_extent+']'; //Adiciona '[' e ']' para depois tranformar em array
				extent = JSON.parse(extent); //Transforma extent em array
				
				if (layerHighlight) {
					map.removeLayer(layerHighlight);
					layerHighlight = null;
				}
				
				layerHighlight = new ol.layer.Tile({
					source: new ol.source.TileWMS({
						url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
						params: {
							'LAYERS': 'highlight_element',
							'FORMAT': 'image/png',
							TRANSPARENT: true,
							'TILED': true
						},
						serverType: 'mapserver'
					}),
					preload: Infinity
				});
				
				//Seta index
				layerHighlight.setZIndex(3);
				
				map.addLayer(layerHighlight);
				map.addLayer(layerPontosBebes);
				map.addLayer(layerPontosGestantes);
				map.addLayer(layerPontosCriancas);
				map.addLayer(layerComunidadesPontoMedio);
				
				$('#elemlayerPontosCriancas').prop('checked', true);
				$('#elemlayerPontosGestantes').prop('checked', true);
				$('#elemlayerPontosBebes').prop('checked', true);
				$('#elemlayerComunidadesPontoMedio').prop('checked', true);
				
				view.fit(extent); //Ajusta a extent na borda do mapa
			} else {
				alert('Extent desse location_id não existe');
			}
		}, 
		error : function(xhr, textStatus, errorThrown) { 
			console.log('Ajax request failed.'); 
		}
	});
	
}

function verif_digitado(){
	setTimeout(function(){
		search_val = $('#map_search').val();
		
		if(search_val.length >= len_search){
			$.ajax({
				url:        '/map/ol52/search', 
				type:       'POST',
				dataType:   'text',
				data:		{ type_id: type_id, search_val: search_val},
				success: function(data) { 
					if (data != "Não tem dados") {
						data = data.split(',');
						
						$('.dropdown .dropdown-menu  li a').parent().remove();
						
						for (var i = 0; i < data.length; i++) {
							var dataList = data[i].split('|');
							
							//dataList[0] = location_id, dataList[1] = nome do estado/municipio/disocese/..., dataList[2] = nome da tabela
							$('.dropdown .dropdown-menu').append('<li role="presentation" style="cursor: pointer;"><a class="search_list" role="menuitem" tabindex="-1" id="'+dataList[0]+'" name="'+dataList[0]+'" name_table="'+dataList[2]+'">' + dataList[1] +'</a></li>');
						}
						
						$('.dropdown-menu').show();
						
					} else {
						location_id = null;
						name_table = null;
						
						$('.dropdown .dropdown-menu  li a').parent().remove();
						$('.dropdown .dropdown-menu').append('<li><a><em>Nenhuma resultado encontrado.</em></a></li>');
					}
					
					$('.search_list').click(function(){
						$('.dropdown .dropdown-menu  li a').parent().remove();
						$('.dropdown-menu').hide();
						
						location_id = $(this).attr('id');
						name_table = $(this).attr('name_table');
						result_search = $(this).text();
						
						$('#map_search').val(result_search);
						
						getExtent(location_id, name_table);
					});
					$(document).ready(function(){
						$('.dropdown-toggle').dropdown('toggle');
					});
				},

			});
		} else {
			$('.dropdown .dropdown-menu  li a').parent().remove();
			$('.dropdown-menu').hide();
		}
	}, 800 );
	$('body').click(function(e) {
		if ($(e.target).closest('.dropdown .dropdown-menu  li a').length === 0) {
			$('.dropdown .dropdown-menu  li a').parent().remove();
			$('.dropdown-menu').hide();
		}
	});
}

function KeyCheck(event) {
   var KeyID = event.keyCode;
   switch(KeyID)
   {
	  case 8:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
		  verif_digitado();
	  break; 
	  case 27:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
	  break;
	  case 46:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
		  verif_digitado();
	  break;
	  case 90:
		  $('.dropdown .dropdown-menu  li a').show();
		  verif_digitado();
	  break;
	  default:
	  break;
   }
}

function click_input() {
	if (verif_digitado() == true) {
		$('.dropdown .dropdown-menu').show();
		verif_digitado();
	}
}

function getMinZoom() {
	var height = $('.map').height();
	var result = (Math.LOG2E * Math.log(height / 30)).toFixed(1);
	return parseFloat(result);
}


async function getLayerPessoas()
{
	var users_id = await refreshUsersAppVisita();

	var layerComunAppvisita = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'brasil_comunidades_appvisita.map',
			params: {
				'LAYERS': 'comun_appvisita',
				'users_id': (users_id == '' ? '' : users_id),
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	layerComunAppvisita.setZIndex(5);

	return Promise.resolve(layerComunAppvisita);
}

function refreshUsersAppVisita() {
	return new Promise((resolve, reject) => {
		$.ajax({
			url:        '/map/ol52/refresh-users', 
			type:       'POST',
			dataType:   'text',
			success: function(data) {
				var users_id = data.split('||')[0];
				// console.log(users_id);
				num_usuarios_ativos = data.split('||')[1];
				$("canvas").attr("id", "canvasMap");
				return resolve(users_id);
			}
		});
	});	
}

var dely = time_reload;
var id_sensor = null;
var sensor = function(callback){
	clearTimeout(id_sensor);
	id_sensor = setTimeout(callback, dely);
	var c = document.getElementById("canvasMap");
	var ctx = c.getContext("2d");
	ctx.font = "20px Arial";
	ctx.fillStyle = "#ba1313";
	ctx.fillText(num_usuarios_ativos+" pessoas usando o AppVisita",c.width*0.77,25);
}

var last_layer = null;
var new_layer  = null;
var recursive = async function(){
	if(!!last_layer) {		
		map.removeLayer(last_layer);
		last_layer = null;
		dely = time_reload;
	}else{
		if (!!layerAppvisita) {
			map.removeLayer(layerAppvisita);
			layerAppvisita = null;
		}
		last_layer = new_layer;
		new_layer  = await getLayerPessoas();
		map.addLayer(new_layer);
		dely = 1000;
	}	
}

if (time_reload) {
	setTimeout(() => {
		map.on('postrender', function(event){
			sensor(recursive);
		});
	}, 1000);
}

//mapOsm
initialZoom = getMinZoom();

if (box_sequence != '') {
	var map = new ol.Map({
		target: 'map',
		controls: [],
		view: new ol.View({
			projection: 'EPSG:4326',
			center: [-51.9253, -14.235],
			minZoom: initialZoom, //Zoom minimo no mapa
			maxZoom: 20, //Zoom minimo no mapa
			zoom: initialZoom
		})
	});
} else {
	var map = new ol.Map({
		target: 'map',
		controls: ol.control.defaults().extend([
			new ol.control.FullScreen({
				source: 'fullscreen' //Side panel continua com fullscreen
			})
		]),
		view: new ol.View({
			projection: 'EPSG:4326',
			center: [-51.9253, -14.235],
			minZoom: initialZoom, //Zoom minimo no mapa
			maxZoom: 20, //Zoom minimo no mapa
			zoom: initialZoom
		})
	});

	map.addLayer(layerEstados);
	map.addLayer(layerCapitais);
}
map.addLayer(layerOSM);

var view = map.getView();
$("canvas").attr("id", "canvasMap");

if (!!time_reload_tmp) {
	layerAppvisita = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'brasil_comunidades_appvisita.map',
			params: {
				'LAYERS': 'comun_appvisita',
				'users_id': (users_id == '' ? '' : users_id),
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	layerAppvisita.setZIndex(5);
	map.addLayer(layerAppvisita);
}

//layersOsm
//Criação de array com todos os layers
var arrayLayers = [layerOSM,layerEstados,layerCigex,layerMunicipios,layerSedesMunicipais,layerDioceses,layerParoquias,layerSedesParoquias,
    layerSetores,layerCapitais,layerRamos,layerComunidadesPontoMedio,layerHortaCasa,layerPontosCriancas,layerPontosGestantes,
    layerPontosBebes,layerGraficoPontos,layerSulfatoFerroso,layerAcidoFolico,layerMenores1Ano2010,layerCriancas0a6Anos2010,layerCriancasPobres2010,
    layerCriancasMiseraveis2010,layerRendaCrianca2010,layerAbastAgua2010,layerAbastAguaPie2010,layerEsgotSanit2010,
    layerEsgotSanitPie2010,layerDestLixo2010,layerDestLixoPie2010,layerPropCatolicos2010,layerUnidadesSaude,
    layerCisternasDentroMunic,layerCisternasForaMunic,layerAbastAgua2000,layerMenores1Ano2000,layerCriancas1a5Anos2000,
    layerAnosEstudo2000,layerRendaFamilia2000];

//Criação de array com todos os ids dos inputs
var arrayElem = ['elemLayerOSM','elemlayerEstados','elemLayerCigex','elemlayerMunicipios','elemlayerSedesMunicipais','elemlayerDioceses',
  'elemlayerParoquias','elemlayerSedesParoquias','elemlayerSetores','elemlayerCapitais','elemlayerRamos',
  'elemlayerComunidadesPontoMedio','elemlayerHortaCasa','elemlayerPontosCriancas','elemlayerPontosGestantes',
  'elemlayerPontosBebes','elemlayerGraficoPontos','elemlayerSulfatoFerroso','elemlayerAcidoFolico','elemlayerMenores1Ano2010','elemlayerCriancas0a6Anos2010',
  'elemlayerCriancasPobres2010','elemlayerCriancasMiseraveis2010','elemlayerRendaCrianca2010','elemlayerAbastAgua2010',
  'elemlayerAbastAguaPie2010','elemlayerEsgotSanit2010','elemlayerEsgotSanitPie2010','elemlayerDestLixo2010',
  'elemlayerDestLixoPie2010','elemlayerPropCatolicos2010','elemlayerUnidadesSaude','elemlayerCisternasDentroMunic',
  'elemlayerCisternasForaMunic','elemlayerAbastAgua2000','elemlayerMenores1Ano2000','elemlayerCriancas1a5Anos2000',
  'elemlayerAnosEstudo2000','elemlayerRendaFamilia2000'];

//Criação de array com todos os tiles
var arrayTiles = [null,tileEstados,null,tileMunicipios,tileSedesMunicipais,tileDioceses,tileParoquias,tileSedesParoquias,tileSetores,
   tileCapitais,tileRamos,tileComunidadesPontoMedio,tileHortaCasa,tilePontosCriancas,tilePontosGestantes,tilePontosBebes,tileGraficoPontosEstado,
   tileSulfatoFerrosoMunicipio,tileAcidoFolicoMunicipio,tileMenores1Ano2010,tileCriancas0a6Anos2010,tileCriancasPobres2010,tileCriancasMiseraveis2010,
   tileRendaCrianca2010Munic,tileAbastAguaMunic,null,tileEsgotSanitAreaMunic,null,tileDestLixo2010Munic,null,
   tilePropCatolicos2010Munic,tileUnidadesSaude,tileCisternasDentroMunic,tileCisternasForaMunic,tileAbastAgua2000,
   tileMenores1Ano2000,tileCriancas1a5Anos2000,tileAnosEstudo2000,tileRendaFamilia2000];

//Array com todos os códigos UFs
var arrayCodUf = [{codUf:11, uf:"RO"}, {codUf:12, uf:"AC"}, {codUf:13, uf:"AM"}, {codUf:14, uf:"RR"}, {codUf:15, uf:"PA"}, 
   {codUf:16, uf:"AP"}, {codUf:17, uf:"TO"}, {codUf:21, uf:"MA"}, {codUf:22, uf:"PI"}, {codUf:23, uf:"CE"}, 
   {codUf:24, uf:"RN"}, {codUf:25, uf:"PB"}, {codUf:26, uf:"PE"}, {codUf:27, uf:"AL"}, {codUf:28, uf:"SE"}, 
   {codUf:29, uf:"BA"}, {codUf:31, uf:"MG"}, {codUf:32, uf:"ES"}, {codUf:33, uf:"RJ"}, {codUf:35, uf:"SP"}, 
   {codUf:41, uf:"PR"}, {codUf:42, uf:"SC"}, {codUf:43, uf:"RS"}, {codUf:50, uf:"MS"}, {codUf:51, uf:"MT"}, 
   {codUf:52, uf:"GO"}, {codUf:53, uf:"DF"}
];

//Array com os nomes dos layers e nome de apenas 1 coluna
var arrayLayers1Col = [{layer:"municipios_poligono", colName:"Nome"}, {layer:"sedes_municipais", colName:"Nome"}, {layer:"paroquias", colName:"Nome"}, {layer:"dioceses", colName:"Nome"}, 
        {layer:"sedes_paroquias", colName:"Nome"}, {layer:"setores", colName:"Nome"}, {layer:"capitais", colName:"Nome da capital"}, 
        {layer:"ramos", colName:"Nome"}, {layer:"pontos_criancas", colName:"Nome da criança"}, {layer:"pontos_gestantes", colName:"Nome da gestante"}, 
        {layer:"pontos_bebes", colName:"Nome do bebê"}, 
        {layer:"abastecimento_agua_2000", colName:"Valor em %"}, {layer:"criancasmenos1", colName:"Número de crianças"}, 
        {layer:"criancas1a5", colName:"Número de crianças"}, {layer:"anosestudo", colName:"Valor"}, {layer:"rendafamilia", colName:"Valor"}
];

//Array com os nomes dos layers e nome de N colunas
var arrayLayersNcol = [{layer:"comunidades_ponto_medio", colName: [{col1: "Nome da comunidade"}, {col2: "Número de crianças"}, {col3: "Meses com informação"}]},
        {layer:"grafico_pontos_estado", colName: [{col1: "Nome"}, {col2: "% de crianças visitadas"}, {col3: "% de bebês visitados"}, {col4: "% de gestantes visitadas"}, {col5: "% de crianças, bebês e gestantes abandonados"}] }, 
        {layer:"grafico_pontos_setor", colName: [{col1: "Nome"}, {col2: "% de crianças visitadas"}, {col3: "% de bebês visitados"}, {col4: "% de gestantes visitadas"}, {col5: "% de crianças, bebês e gestantes abandonados"}] }, 
        {layer:"grafico_pontos_comunidade", colName: [{col1: "Nome"}, {col2: "% de crianças visitadas"}, {col3: "% de bebês visitados"}, {col4: "% de gestantes visitadas"}, {col5: "% de crianças, bebês e gestantes abandonados"}] }, 
        {layer:"sulfato_ferroso_municipio", colName: [{col1: "Município"}, {col2: "533 - Está em falta no posto"}, {col3: "534 - Tem suplemento mas está vencido"}, {col4: "535 - O suplemento foi comprado pela gestante"}, {col5: "536 - O médico disse que não precisa"}, {col6: "537 - Outro motivo"}] }, 
        {layer:"sulfato_ferroso_paroquia", colName: [{col1: "Paróquia"}, {col2: "533 - Está em falta no posto"}, {col3: "534 - Tem suplemento mas está vencido"}, {col4: "535 - O suplemento foi comprado pela gestante"}, {col5: "536 - O médico disse que não precisa"}, {col6: "537 - Outro motivo"}] }, 
        {layer:"acido_folico_municipio", colName: [{col1: "Município"}, {col2: "539 - Está em falta no posto"}, {col3: "540 - Tem suplemento mas está vencido"}, {col4: "541 - O suplemento foi comprado pela gestante"}, {col5: "542 - O médico disse que não precisa"}, {col6: "543 - Outro motivo"}] }, 
        {layer:"acido_folico_paroquia", colName: [{col1: "Paróquia"}, {col2: "539 - Está em falta no posto"}, {col3: "540 - Tem suplemento mas está vencido"}, {col4: "541 - O suplemento foi comprado pela gestante"}, {col5: "542 - O médico disse que não precisa"}, {col6: "543 - Outro motivo"}] }, 
        {layer:"abastecimento_agua_2010_setor_censitario", colName: [{col1: "Nome ou código"}, {col2: "Nº total de domicílios"}, {col3: "Nº de domicílios abastecidos por rede geral"}, {col4: "% com abastecimento por rede geral"}, {col5: "% com água de poço ou nascente"}, {col6: "% com água de chuva em cisterna"}, {col7: "% com outra forma de abastecimento"}] }, 
        {layer:"abastecimento_agua_2010_municipio", colName: [{col1: "Nome ou código"}, {col2: "Nº total de domicílios"}, {col3: "Nº de domicílios abastecidos por rede geral"}, {col4: "% com abastecimento por rede geral"}, {col5: "% com água de poço ou nascente"}, {col6: "% com água de chuva em cisterna"}, {col7: "% com outra forma de abastecimento"}] }, 
        //{layer:"abastecimento_agua_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}] }, 
        {layer:"cisternasDentroMunic", colName: [{col1: "Nº da Cisterna"}, {col2: "Nome da Comunidade"}, {col3: "Nome do Município"}, {col4: "UF"}] }, 
        {layer:"cisternasForaMunic", colName: [{col1: "Nº da Cisterna"}, {col2: "Nome da Comunidade"}, {col3: "Nome do Município"}, {col4: "UF"}] }, 
        //{layer:"comunidades", colName: [{col1: "Nome da Comunidade"}, {col2: "Crianças de 0 a 6 anos cadastradas"}, {col3: "Crianças de 0 a 1 ano cadastradas"}] }, 
        {layer:"criancas_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de crianças"}] }, 
        {layer:"criancas_miseraveis_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças Miseráveis"}] }, 
        {layer:"criancas_pobres_0_6_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças Pobres"}] }, 
        {layer:"renda_crianca_setor_censitario", colName: [{col1: "de 0 a 1/4 s.m."}, {col2: "de 1/4 a 1/2 s.m."}, {col3: "de 1/4 a 1/2 s.m."}, {col4: "de 1 a 10 ou mais s.m."}] }, 
        {layer:"renda_crianca_municipio", colName: [{col1: "de 0 a 1/4 s.m."}, {col2: "de 1/4 a 1/2 s.m."}, {col3: "de 1/4 a 1/2 s.m."}, {col4: "de 1 a 10 ou mais s.m."}] }, 
        {layer:"destino_lixo_2010_area_setor_censitario", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% Coletado por serviço de limpeza"}, {col4: "% Coletado em caçamba"}, {col5: "% Queimado"}, {col6: "% Enterrado"}, {col7: "% Jogado em terreno baldio"}, {col8: "% Jogado em rio&#44; lago ou mar"}, {col9: "% Outro destino"}] }, 
        {layer:"destino_lixo_2010_area_municipio", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% Coletado por serviço de limpeza"}, {col4: "% Coletado em caçamba"}, {col5: "% Queimado"}, {col6: "% Enterrado"}, {col7: "% Jogado em terreno baldio"}, {col8: "% Jogado em rio&#44; lago ou mar"}, {col9: "% Outro destino"}] }, 
        //{layer:"destino_lixo_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}, {col6: ""}, {col7: ""}, {col8: ""}, {col9: ""}] }, 
        {layer:"esgotamento_sanitario_2010_area_setor_censitario", colName: [{col1: "Código ou Nome"}, {col2: "Qualidade"}, {col3: "% rede geral de esgoto ou pluvial"}, {col4: "% fossa séptica"}, {col5: "% fossa rudimentar"}, {col6: "vala"}, {col7: "% rio&#44; lago ou mar"}, {col8: "% outro escoadouro"}] }, 
        {layer:"esgotamento_sanitario_2010_area_municipio", colName: [{col1: "Nome do município"}, {col2: "Qualidade"}, {col3: "% rede geral de esgoto ou pluvial"}, {col4: "% fossa séptica"}, {col5: "% fossa rudimentar"}, {col6: "vala"}, {col7: "% rio&#44; lago ou mar"}, {col8: "% outro escoadouro"}] }, 
        //{layer:"esgotamento_sanitario_2010_pie", colName: [{col1: "Código do Setor Censitário"}, {col2: ""}, {col3: ""}, {col4: ""}, {col5: ""}, {col6: ""}, {col7: ""}] }, 
        {layer:"horta", colName: [{col1: "Nome da comunidade"}, {col2: "Mês inicial"}, {col3: "Mês final"}, {col4: "Crianças com horta"}] }, 
        {layer:"criancas_menores_1_2010", colName: [{col1: "Código do Setor Censitário"}, {col2: "Número de Crianças"}] }, 
        {layer:"prop_catholics_setor_censitario", colName: [{col1: "Código do Setor Censitário"}, {col2: "% de Católicos"}] }, 
        {layer:"prop_catholics_municipio", colName: [{col1: "Código do Município"}, {col2: "% de Católicos"}] }, 
        {layer:"UnidadesSaude", colName: [{col1: "Código"}, {col2: "Nome da US"}, {col3: "Nome do Município"}, {col4: "UF"}] }
];

var arrayPrior = [{layerName: layerOSM, zindex: 0}, {layerName: layerEstados, zindex: 2},
   {layerName: layerCigex, zindex: 4}, {layerName: layerMunicipios, zindex: 2}, 
   {layerName: layerSedesMunicipais, zindex: 4}, {layerName: layerDioceses, zindex: 2}, 
   {layerName: layerParoquias, zindex: 2}, {layerName: layerSedesParoquias, zindex: 4}, 
   {layerName: layerSetores, zindex: 2}, {layerName: layerCapitais, zindex: 4}, 
   {layerName: layerRamos, zindex: 2}, {layerName: layerComunidadesPontoMedio, zindex: 5}, 
   {layerName: layerHortaCasa, zindex: 4}, {layerName: layerPontosCriancas, zindex: 4}, 
   {layerName: layerPontosGestantes, zindex: 4}, {layerName: layerPontosBebes, zindex: 4}, 
   {layerName: layerGraficoPontos, zindex: 4}, {layerName: layerSulfatoFerroso, zindex: 2}, 
   {layerName: layerAcidoFolico, zindex:2}, {layerName: layerMenores1Ano2010, zindex: 2}, 
   {layerName: layerCriancas0a6Anos2010, zindex: 2}, {layerName: layerCriancasPobres2010, zindex: 2}, 
   {layerName: layerCriancasMiseraveis2010, zindex: 2}, {layerName: layerRendaCrianca2010, zindex: 1}, 
   {layerName: layerAbastAgua2010, zindex: 1}, {layerName: layerAbastAguaPie2010, zindex: 4}, 
   {layerName: layerEsgotSanit2010, zindex: 1}, {layerName: layerEsgotSanitPie2010, zindex: 4}, 
   {layerName: layerDestLixo2010, zindex: 1}, {layerName: layerDestLixoPie2010, zindex: 4}, 
   {layerName: layerPropCatolicos2010, zindex: 1}, {layerName: layerUnidadesSaude, zindex: 4}, 
   {layerName: layerCisternasDentroMunic, zindex: 4}, {layerName: layerCisternasForaMunic, zindex: 4}, 
   {layerName: layerAbastAgua2000, zindex: 1}, {layerName: layerMenores1Ano2000, zindex: 2}, 
   {layerName: layerCriancas1a5Anos2000, zindex: 2}, {layerName: layerAnosEstudo2000, zindex: 1}, 
   {layerName: layerRendaFamilia2000, zindex: 1}
];

function redirectZoom(location_id) {
    $.ajax({ 
        url:        '/map/ol52/get-table-geos-by-location-id',
        type:       'POST',
		dataType:   'text',
		data:		{ location_id: location_id },
        success: function(data) {
            redirectExtent(data);
        }
    });
}

//eventsOsm
$(document).ready(function(){
	
	//Seta zindex
	for (var cont = 0; cont < arrayPrior.length; cont++) {
		var zindex = arrayPrior[cont]['zindex'];
		arrayPrior[cont]['layerName'].setZIndex(zindex);
	}
	
	map.on('singleclick', function(evt) {
		$('body').css('cursor', 'wait');
		
		var viewResolution = (view.getResolution());
        console.log(viewResolution);
		
		var resTable = '';
		
		$('input[type="checkbox"]').each(function(){
			if ($(this).is(':checked')) {
				var id_item = $(this).attr('id');
			}
			var layer = '';
			
			//Verificação de qual input selecionou
			for (var i = 0; i < arrayElem.length; i++) {
				var count = 0;
				
				if ((arrayElem[i] != 'elemLayerOSM' && arrayElem[i] != 'elemLayerCigex') && arrayElem[i] == id_item) {
					tile = arrayTiles[i];
					
					if (tile != null) {
                        //viewResolution < 0.001040759971511351 = quando camadas (% de crianças de 0 a 6 anos em faixas de renda, Água - Percentual de domicílios abastecidos pela rede geral, Esgotamento sanitário - Indicador em áreas e Destino do lixo - Indicador em áreas), mudam de layer
                        if (viewResolution <= 0.012619977044645162 && viewResolution > 0.0015774971305806452) {
                            if (arrayElem[i] == 'elemlayerGraficoPontos') {
                                tile = tileGraficoPontosSetor;
                            }
                        } else if (viewResolution <= 0.0015774971305806452) {
                            if (arrayElem[i] == 'elemlayerGraficoPontos') {
                                tile = tileGraficoPontosComunidade;
                            }
                        }
                        if (viewResolution <= 0.0008453597808917424) {
                            if (arrayElem[i] == 'elemlayerSulfatoFerroso') {
                                tile = tileSulfatoFerrosoParoquia;
                            }
                            if (arrayElem[i] == 'elemlayerAcidoFolico') {
                                tile = tileAcidoFolicoParoquia;
                            }
                        }
						if (viewResolution < 0.001040759971511351) {
							if (arrayElem[i] == 'elemlayerRendaCrianca2010') {
								tile = tileRendaCrianca2010SetorCens;
							} else if (arrayElem[i] == 'elemlayerPropCatolicos2010') {
								tile = tilePropCatolicos2010SetorCens;
							} else if (arrayElem[i] == 'elemlayerAbastAgua2010') {
								tile = tileAbastAguaSetorCens;
							} else if (arrayElem[i] == 'elemlayerEsgotSanit2010') {
								tile = tileEsgotSanitSetorCens;
							} else if (arrayElem[i] == 'elemlayerDestLixo2010') {
								tile = tileDestLixo2010SetorCens;
							}
							
							var url = tile.getGetFeatureInfoUrl(
								evt.coordinate, viewResolution, 'EPSG:4326'
							);
						} else {
							var url = tile.getGetFeatureInfoUrl(
								evt.coordinate, viewResolution, 'EPSG:4326'
							);
						}
					}
					
					var layerName = $(this).attr('layerName');
					if (url) {
						$.ajax({ 
							url:        url,
							dataType:   'text',
							success: function(data) {
								if (data.indexOf('Search returned no results') <= -1) {
                                    if (data.indexOf('location_id') != -1) {
                                        var dataLocationId = data.split("location_id");
                                        var dataSplit = dataLocationId[0].split(" = '");
                                        var locationId = dataLocationId[1].replace(/'/g, "").trim();
                                        locationId = locationId.replace("=", "").trim();
                                    } else {
                                        var dataSplit = data.split(" = '");
                                        var locationId = "";
                                    }
									
									if (dataSplit.length > 1) {
                                        var line1 = '<tr style="color: #003333; background-color: #EBEADB;"><td></td>';
                                        if (locationId) {
                                            var line2 = '<tr><td onclick="redirectZoom('+locationId+')">@</td>';
                                        } else {
                                            var line2 = '<tr><td>@</td>';
                                        }
										
										for (var i = 1; i < dataSplit.length; i++) {
											if (i == (dataSplit.length - 1)) {
												var valCol = dataSplit[i].replace("'","").trim();
											} else {
												var valCol = dataSplit[i].split("'")[0].trim();
											}
											
											for (var cont = 0; cont < arrayCodUf.length; cont++) {
												if (arrayCodUf[cont]['uf'] == valCol && data.indexOf('br_ufs') > -1) {
													var codUf = arrayCodUf[cont]['codUf'];
													line1 += '<td style="width: 50%;">Código</td>';
													line2 += '<td>'+codUf+'</td>';
												}
											}
											
											if (codUf) {
												line1 += '<td style="width: 50%;">UF</td>';
												line2 += '<td>'+valCol+'</td>';
											} else {
												if (data.indexOf('br_ufs') <= -1) {
													if (dataSplit.length == 2) {
														for (var j = 0; j < arrayLayers1Col.length; j++) {
															if (data.indexOf("'"+arrayLayers1Col[j]['layer']+"'") > -1) {
																line1 += '<td style="width: 100%;">'+arrayLayers1Col[j]['colName']+'</td>';
																line2 += '<td>'+valCol+'</td>';
															}
														}
													} else {
														for (var j = 0; j < arrayLayersNcol.length; j++) {
															if (data.indexOf(arrayLayersNcol[j]['layer']) > -1) {
																for (var k = 0; k < arrayLayersNcol[j]['colName'].length; k++) {
																	
																	if (count == k) {
																		var col = 'col'+(k+1);
																		
																		var nomeColuna = arrayLayersNcol[j]['colName'][k][col];
																		var tamColuna = parseInt(nomeColuna.length) * 5;
																		var tamColunaVal = parseInt(valCol.length) * 7.5;
																		
																		if (tamColuna > tamColunaVal) {
																			line1 += '<td style="min-width: '+tamColuna+'px; width: '+tamColuna+'px;">'+nomeColuna+'</td>';
																		} else {
																			line1 += '<td style="min-width: '+tamColunaVal+'px;">'+nomeColuna+'</td>';
																		}
																	}
																	
																	count++;
																}
																
																line2 += '<td>'+valCol+'</td>';
															}
														}
													}
												}
                                            }
										}
										line1 += '</tr>';
										line2 += '</tr>';
										
										resTable += '<b style="font-size: 13px;">'+layerName+'</b>';
										resTable += '<table cellspacing="0" border="1" class="tblResultados">';
										resTable += line1;
										resTable += line2;
                                        resTable += '</table>';
									}
									
									setTimeout(function(){
										$('#resDialog').html(resTable);
										
										$('#resDialog').dialog({
											width: 500,
											height: 250,
											open: function() {
												$(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-dialog-titlebar-close');
											}
										}).prev('.ui-dialog-titlebar').css('background','#000000');
										
										$('.ui-dialog-title').css('font-size','15px');
										
										$('body').css('cursor', 'default');
									}, 3000);
								} else {
									setTimeout(function(){
										$('body').css('cursor', 'default');
									}, 3000);
								}
								
							}
						});
					}
				}
			}
		});
	});
	
	$('.td_search').hide();
	
	$('#sel_busca_por').change(function(){
		len_search = 3;
		
		$('#map_search').val('');
		
		if ($(this).val() != "0") {
			type_id = $(this).val();
			$('.td_search').show();
		} else {
			$('.td_search').hide();
		}
	});
	
	$('#btn_consulta').click(function(){
		if (search_val.length >= len_search) {
			if (location_id != null && name_table != null) {
				getExtent(location_id, name_table);
			}
		}
	});
	
	$('.sidepanel').append(selCamadas);
	
	$('.zoom1000').attr('disabled', 'disabled');
	$('.zoom750').attr('disabled', 'disabled');
	$('.zoom400').attr('disabled', 'disabled');
	$('.zoom50').attr('disabled', 'disabled');
	
	$('.zoom4500').css('color', '#CCC');
	$('.zoom1000').css('color', '#CCC');
	$('.zoom750').css('color', '#CCC');
	$('.zoom400').css('color', '#CCC');
	$('.zoom50').css('color', '#CCC');
	
	//Habilita a camada conforme o nível de zoom que o usuário está
	map.on('moveend', function(evt){
		var viewResolution = (view.getResolution());
		
		if (viewResolution < 0.0002113399452229356) {
			$('.zoom50').removeAttr('disabled');
			$('.zoom50').css('color', '');
			$('.zoom400').removeAttr('disabled');
			$('.zoom400').css('color', '');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.0002113399452229356 && viewResolution < 0.0016907195617834848) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').removeAttr('disabled');
			$('.zoom400').css('color', '');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution == 0.0016907195617834848) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').removeAttr('disabled');
			$('.zoom750').css('color', '');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution > 0.0016907195617834848 && viewResolution < 0.006762878247133939) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').removeAttr('disabled');
			$('.zoom1000').css('color', '');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.006762878247133939 && viewResolution < 0.013525756494267878) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').attr('disabled', 'disabled');
			$('.zoom1000').css('color', '#CCC');
			$('.zoom4500').removeAttr('disabled');
			$('.zoom4500').css('color', '');
		} else if (viewResolution >= 0.013525756494267878) {
			$('.zoom50').attr('disabled', 'disabled');
			$('.zoom50').css('color', '#CCC');
			$('.zoom400').attr('disabled', 'disabled');
			$('.zoom400').css('color', '#CCC');
			$('.zoom750').attr('disabled', 'disabled');
			$('.zoom750').css('color', '#CCC');
			$('.zoom1000').attr('disabled', 'disabled');
			$('.zoom1000').css('color', '#CCC');
			$('.zoom4500').attr('disabled', 'disabled');
			$('.zoom4500').css('color', '#CCC');
		}
	});
	
	//Seta como 500px para remover o atributo que o bootstrap impôs as imagens (max-width: 100%) e, 
	//nesse caso, estava limitando o tamanho da imagem não conseguindo ver nada na legenda
	$('img').css('max-width', '500px');
	
	//Verifica quais inputs estão selecionados
	$('input').each(function(){
		if ($(this).is(':checked')) {
			$(this).parent().children('ul.tree').toggle(300);
		}
	});
	
	//$('.icon').addClass('icon-minus');
	$('.tree-toggler').click(function (event) {
		//Pega a classe completa que o usuário selecionou
		var classTarget = $(this).attr('class');
		
		//Separa a classe do elemento que usuário clicou
		var classSpElemArray = classTarget.split(' ');
		//Passa somente a classe de início 'sp'
		classSpElem = classSpElemArray[0];
		
		//Loop da classe icon para pegar a classe corresponte ao que o usuário clicou
		$('.icon').each(function(){
			
			//Classe que contém icon
			var classSpanIcon = $(this).attr('class');
			
			//Verificação se a classe que contém icon possui a classe que começa com 'sp' 
			//(classe de início 'sp' se refere ao nome do layer que a pessoa selecionou)
			if (classSpanIcon.indexOf(classSpElem) != -1) {
				//Passa para variável a classe de início 'sp' + a clase icon
				classIcon = '.' + classSpElem + '.icon';
			}
		});
		
		//Abre ou esconde o 'corpo' da tree
		$(this).parent().children('ul.tree').toggle(300, function(){
			
			if (classTarget.indexOf('header') != -1) {
				//Ao clicar no header da tree, verifica se o 'corpo' está visível
				if ($(this).is(':hidden')) {
					$(classIcon).removeClass('icon-minus');
					$(classIcon).addClass('icon-plus');
				} else {
					$(classIcon).removeClass('icon-plus');
					$(classIcon).addClass('icon-minus');
				}
			} else {
				//Clicou em alguma das opções do 'corpo' da tree para mostrar a legenda
				if($(this).is(':hidden'))
				{
					$(classIcon).removeClass('icon-minus');
					$(classIcon).addClass('icon-plus');
				}
				else
				{
					$(classIcon).removeClass('icon-plus');
					$(classIcon).addClass('icon-minus');
				}
			}
		});
		
	});
	
	$('input[type="checkbox"]').click(function(){
		
		if ($(this).attr('name') != undefined) {
			var group = 'input[name="' + $(this).attr('name') + '"]';
			
			$('ul').find(group).not(this).attr('checked', false).prop('checked', false);
			
			$(group).each(function(){
				if (!$(this).is(':checked')) {
					var groupItem = $(this).attr('id');
					//Verificação de qual input selecionou
					for (var i = 0; i < arrayElem.length; i++) {
						if (arrayElem[i] == groupItem) {
							layer = arrayLayers[i];
						}
					}
					map.removeLayer(layer);
				}
			});
		}
		
		var layerChecked = $(this).prop('checked');
		var id_item = $(this).attr('id');
		var layer = '';
		
		//Verificação de qual input selecionou
		for (var i = 0; i < arrayElem.length; i++) {
			if (arrayElem[i] == id_item) {
				layer = arrayLayers[i];
			}
		}
		
		if (layerChecked) { //Input está selecionado, adicionar camada no mapa
			map.addLayer(layer);
		} else { //Input não está selecionado, remover camada do mapa
			map.removeLayer(layer);
		}
    });
    
    
	
});


if (param_location_id != '' && param_city_gid != '' && typeof(param_location_id) !== 'undefined') {
    getExtentCommunity(param_location_id, param_city_gid);
} else {
    if (box_sequence != '' && typeof(box_sequence) !== 'undefined') {
        redirectExtent(box_sequence, children, babies, pregnant, states);
    }
}

if (geo_point != '' && geo_point != null) {
    var tilePontoGPS = new ol.source.TileWMS({
        url: urlOrigin+localMapfile+'brasil_ponto_gps.map',
        params: {
            'LAYERS': 'ponto_gps',
            'geo_point': (geo_point),
            'FORMAT': 'image/png',
            TRANSPARENT: true,
            'TILED': true
        },
        serverType: 'mapserver'
    });
    var layerPontoGPS = new ol.layer.Tile({
        source: tilePontoGPS,
        preload: Infinity
    });

    map.addLayer(layerEstados);
    map.addLayer(layerPontoGPS);

    if (box_geo_point != null && box_geo_point != '' && typeof(box_geo_point) !== 'undefined') {
        //Retira 'BOX', '(', ')' e ',' do box_geo_point 
        box_geo_point = box_geo_point.replace('BOX','');
        box_geo_point = box_geo_point.replace('(','');
        box_geo_point = box_geo_point.replace(')','');
        box_geo_point = box_geo_point.replace(',',' ');
        
        extent = box_geo_point.replace(/ /g,', '); //Troca todos os espaços por ', '
        extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
        extent = JSON.parse(extent); //Transforma extent em array
        
        view.fit(extent); //Ajusta a extent na borda do mapa
    }
}
