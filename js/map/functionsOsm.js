function delay(callback, ms) {
	var timer = 0;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
			callback.apply(context, args);
		}, ms || 0);
	};
}

function getExtent(location_id, name_table) {
	
	$.ajax({ 
		url:        '/map/ol52/getlocal', 
		type:       'POST',
		dataType:   'text',
		data:		{ location_id: location_id, name_table: name_table },
		success: function(data) { 
			var retorno = data.split('@');
			data = retorno[0];
			var sequence_map = retorno[1];
			
			//Retira 'BOX', '(', ')' e ',' do resultado 
			data = data.replace('BOX','');
			data = data.replace('(','');
			data = data.replace(')','');
			data = data.replace(',',' ');
			
			extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
			extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
			extent = JSON.parse(extent); //Transforma extent em array
			
			if (layerHighlight) {
				map.removeLayer(layerHighlight);
				layerHighlight = null;
			}
			
			layerHighlight = new ol.layer.Tile({
				source: new ol.source.TileWMS({
					url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
					params: {
						'LAYERS': 'highlight_element',
						'FORMAT': 'image/png',
						TRANSPARENT: true,
						'TILED': true
					},
					serverType: 'mapserver'
				}),
				preload: Infinity
			});
			
			//Seta index
			layerHighlight.setZIndex(3);
			
			map.addLayer(layerHighlight);
			
			view.fit(extent); //Ajusta a extent na borda do mapa
		}, 
		error : function(xhr, textStatus, errorThrown) { 
			console.log('Ajax request failed.'); 
		}
	});
	
}

function redirectExtent(box_sequence, layer_children, layer_babies, layer_pregnant, states) {
	var retorno = box_sequence.split('@');
	box_sequence = retorno[0];
	var sequence_map = retorno[1];
	
	//Retira 'BOX', '(', ')' e ',' do resultado 
	box_sequence = box_sequence.replace('BOX','');
	box_sequence = box_sequence.replace('(','');
	box_sequence = box_sequence.replace(')','');
	box_sequence = box_sequence.replace(',',' ');
	
	extent = box_sequence.replace(/ /g,', '); //Troca todos os espaços por ', '
	extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
	extent = JSON.parse(extent); //Transforma extent em array
	
	if (layerHighlight) {
		map.removeLayer(layerHighlight);
		layerHighlight = null;
	}
	
	layerHighlight = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
			params: {
				'LAYERS': 'highlight_element',
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	
	if (layer_children != null) {
		var layerCriancas = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_criancas.map',
				params: {
					'LAYERS': layer_children,
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerCriancas.setZIndex(4);
	}
	
	if (layer_babies != null) {
		var layerBebes = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_bebes.map',
				params: {
					'LAYERS': layer_babies,
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerBebes.setZIndex(4);
	}
	
	if (layer_pregnant != null) {
		var layerGestantes = new ol.layer.Tile({
			source:  new ol.source.TileWMS({
				url: urlOrigin+localMapfile+'brasil_pontos_gestantes.map',
				params: {
					'LAYERS': layer_pregnant,
					'FORMAT': 'image/png',
					TRANSPARENT: true,
					'TILED': true
				},
				serverType: 'mapserver'
			}),
			preload: Infinity
		});
		layerGestantes.setZIndex(4);
	}
		
	//Seta index
	layerHighlight.setZIndex(3);
	
	map.addLayer(layerHighlight);
	if (layer_pregnant != null) 
		map.addLayer(layerGestantes);
	
	if (layer_children != null) 
		map.addLayer(layerCriancas);
	
	if (layer_babies != null) 
		map.addLayer(layerBebes);
	
	if (states == 1) 
		map.addLayer(layerEstados);

	view.fit(extent); //Ajusta a extent na borda do mapa
}

function getExtentCommunity(location_id, city_gid) {
	
	$.ajax({ 
		url:        '/map/ol52/getlocal-community', 
		type:       'POST',
		dataType:   'text',
		data:		{ location_id: location_id, city_gid: city_gid },
		success: function(data) { 
			if (data.charAt(0) != '@') {
				var retorno = data.split('@');
				data = retorno[0];
				var sequence_map = retorno[1];
				
				//Retira 'BOX', '(', ')' e ',' do resultado 
				data = data.replace('BOX','');
				data = data.replace('(','');
				data = data.replace(')','');
				data = data.replace(',',' ');
				
				extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
				extent = '['+extent+']'; //Adiciona '[' e ']' para depois tranformar em array
				extent = JSON.parse(extent); //Transforma extent em array
				
				var datas = data.split(' ');
				
				var new_extent = '';
				
				//var geo1 = (parseFloat(datas[0]) - 0.008).toPrecision(8);
				var geo1 = datas[0];
				var geo2 = datas[1];
				var geo3 = datas[2];
				var geo4 = datas[3];
				
				new_extent += geo1 + ', ';
				new_extent += geo2 + ', ';
				new_extent += geo3 + ', ';
				new_extent += geo4;
				
				extent = data.replace(/ /g,', '); //Troca todos os espaços por ', '
				extent = '['+new_extent+']'; //Adiciona '[' e ']' para depois tranformar em array
				extent = JSON.parse(extent); //Transforma extent em array
				
				if (layerHighlight) {
					map.removeLayer(layerHighlight);
					layerHighlight = null;
				}
				
				layerHighlight = new ol.layer.Tile({
					source: new ol.source.TileWMS({
						url: urlOrigin+localMapfile+'highlight_element_'+sequence_map+'.map',
						params: {
							'LAYERS': 'highlight_element',
							'FORMAT': 'image/png',
							TRANSPARENT: true,
							'TILED': true
						},
						serverType: 'mapserver'
					}),
					preload: Infinity
				});
				
				//Seta index
				layerHighlight.setZIndex(3);
				
				map.addLayer(layerHighlight);
				map.addLayer(layerPontosBebes);
				map.addLayer(layerPontosGestantes);
				map.addLayer(layerPontosCriancas);
				map.addLayer(layerComunidadesPontoMedio);
				
				$('#elemlayerPontosCriancas').prop('checked', true);
				$('#elemlayerPontosGestantes').prop('checked', true);
				$('#elemlayerPontosBebes').prop('checked', true);
				$('#elemlayerComunidadesPontoMedio').prop('checked', true);
				
				view.fit(extent); //Ajusta a extent na borda do mapa
			} else {
				alert('Extent desse location_id não existe');
			}
		}, 
		error : function(xhr, textStatus, errorThrown) { 
			console.log('Ajax request failed.'); 
		}
	});
	
}

function verif_digitado(){
	setTimeout(function(){
		search_val = $('#map_search').val();
		
		if(search_val.length >= len_search){
			$.ajax({
				url:        '/map/ol52/search', 
				type:       'POST',
				dataType:   'text',
				data:		{ type_id: type_id, search_val: search_val},
				success: function(data) { 
					if (data != "Não tem dados") {
						data = data.split(',');
						
						$('.dropdown .dropdown-menu  li a').parent().remove();
						
						for (var i = 0; i < data.length; i++) {
							var dataList = data[i].split('|');
							
							//dataList[0] = location_id, dataList[1] = nome do estado/municipio/disocese/..., dataList[2] = nome da tabela
							$('.dropdown .dropdown-menu').append('<li role="presentation" style="cursor: pointer;"><a class="search_list" role="menuitem" tabindex="-1" id="'+dataList[0]+'" name="'+dataList[0]+'" name_table="'+dataList[2]+'">' + dataList[1] +'</a></li>');
						}
						
						$('.dropdown-menu').show();
						
					} else {
						location_id = null;
						name_table = null;
						
						$('.dropdown .dropdown-menu  li a').parent().remove();
						$('.dropdown .dropdown-menu').append('<li><a><em>Nenhuma resultado encontrado.</em></a></li>');
					}
					
					$('.search_list').click(function(){
						$('.dropdown .dropdown-menu  li a').parent().remove();
						$('.dropdown-menu').hide();
						
						location_id = $(this).attr('id');
						name_table = $(this).attr('name_table');
						result_search = $(this).text();
						
						$('#map_search').val(result_search);
						
						getExtent(location_id, name_table);
					});
					$(document).ready(function(){
						$('.dropdown-toggle').dropdown('toggle');
					});
				},

			});
		} else {
			$('.dropdown .dropdown-menu  li a').parent().remove();
			$('.dropdown-menu').hide();
		}
	}, 800 );
	$('body').click(function(e) {
		if ($(e.target).closest('.dropdown .dropdown-menu  li a').length === 0) {
			$('.dropdown .dropdown-menu  li a').parent().remove();
			$('.dropdown-menu').hide();
		}
	});
}

function KeyCheck(event) {
   var KeyID = event.keyCode;
   switch(KeyID)
   {
	  case 8:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
		  verif_digitado();
	  break; 
	  case 27:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
	  break;
	  case 46:
		  $('.dropdown .dropdown-menu  li a').parent().remove();
		  $('.dropdown-menu').hide();
		  verif_digitado();
	  break;
	  case 90:
		  $('.dropdown .dropdown-menu  li a').show();
		  verif_digitado();
	  break;
	  default:
	  break;
   }
}

function click_input() {
	if (verif_digitado() == true) {
		$('.dropdown .dropdown-menu').show();
		verif_digitado();
	}
}

function getMinZoom() {
	var height = $('.map').height();
	var result = (Math.LOG2E * Math.log(height / 30)).toFixed(1);
	return parseFloat(result);
}


async function getLayerPessoas()
{
	var users_id = await refreshUsersAppVisita();

	var layerComunAppvisita = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'brasil_comunidades_appvisita.map',
			params: {
				'LAYERS': 'comun_appvisita',
				'users_id': (users_id == '' ? '' : users_id),
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	layerComunAppvisita.setZIndex(5);

	return Promise.resolve(layerComunAppvisita);
}

function refreshUsersAppVisita() {
	return new Promise((resolve, reject) => {
		$.ajax({
			url:        '/map/ol52/refresh-users', 
			type:       'POST',
			dataType:   'text',
			success: function(data) {
				var users_id = data.split('||')[0];
				// console.log(users_id);
				num_usuarios_ativos = data.split('||')[1];
				$("canvas").attr("id", "canvasMap");
				return resolve(users_id);
			}
		});
	});	
}

var dely = time_reload;
var id_sensor = null;
var sensor = function(callback){
	clearTimeout(id_sensor);
	id_sensor = setTimeout(callback, dely);
	var c = document.getElementById("canvasMap");
	var ctx = c.getContext("2d");
	ctx.font = "20px Arial";
	ctx.fillStyle = "#ba1313";
	ctx.fillText(num_usuarios_ativos+" pessoas usando o AppVisita",c.width*0.77,25);
}

var last_layer = null;
var new_layer  = null;
var recursive = async function(){
	if(!!last_layer) {		
		map.removeLayer(last_layer);
		last_layer = null;
		dely = time_reload;
	}else{
		if (!!layerAppvisita) {
			map.removeLayer(layerAppvisita);
			layerAppvisita = null;
		}
		last_layer = new_layer;
		new_layer  = await getLayerPessoas();
		map.addLayer(new_layer);
		dely = 1000;
	}	
}

if (time_reload) {
	setTimeout(() => {
		map.on('postrender', function(event){
			sensor(recursive);
		});
	}, 1000);
}