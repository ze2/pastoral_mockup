/** PASTORAL DA CRIANÇA **/ 

var tileSetores = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_setores.map',
	params: {
		'LAYERS': 'setores',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileRamos = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_ramos.map',
	params: {
		'LAYERS': 'ramos',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileComunidadesPontoMedio = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_comunidades_ponto_medio.map',
	params: {
		'LAYERS': 'comunidades_ponto_medio',
		'dias_comunidade_show': (dias_comunidade_show == '' ? '' : dias_comunidade_show),
		'dias_comunidade_ativa': (dias_comunidade_ativa == '' ? '' : dias_comunidade_ativa),
		'user_id': (user_id == '' ? '' : user_id),
		'temporary_table': (localStorage.getItem('temporary_table') == '' ? '' : localStorage.getItem('temporary_table')),
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileHortaCasa = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_horta_em_casa.map',
	params: {
		'LAYERS': 'horta',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tilePontosCriancas = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_pontos_criancas.map',
	params: {
		'LAYERS': 'pontos_criancas',
		'dias_crianca_show': (dias_crianca_show == '' ? '' : dias_crianca_show),
		'dias_crianca_ativa': (dias_crianca_ativa == '' ? '' : dias_crianca_ativa),
		'user_id': (user_id == '' ? '' : user_id),
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tilePontosGestantes = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_pontos_gestantes.map',
	params: {
		'LAYERS': 'pontos_gestantes',
		'dias_gestante_show': (dias_gestante_show == '' ? '' : dias_gestante_show),
		'dias_gestante_ativa': (dias_gestante_ativa == '' ? '' : dias_gestante_ativa),
		'user_id': (user_id == '' ? '' : user_id),
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tilePontosBebes = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_pontos_bebes.map',
	params: {
		'LAYERS': 'pontos_bebes',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileGraficoPontos = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_grafico_pontos.map',
	params: {
		'LAYERS': 'grafico_pontos',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});


/** CNBB **/

var tileDioceses = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_dioceses.map',
	params: {
		'LAYERS': 'dioceses',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileParoquias = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_paroquias.map',
	params: {
		'LAYERS': 'paroquias',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileSedesParoquias = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_sedes_paroquias.map',
	params: {
		'LAYERS': 'sedes_paroquias',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

/** CENSO IBGE 2010 **/

var tileMenores1Ano2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_menores_1_ano_2010.map',
	params: {
		'LAYERS': 'criancas_menores_1_2010',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCriancas0a6Anos2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_criancas_0_a_6_anos.map',
	params: {
		'LAYERS': 'criancas_0_6_2010',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCriancasPobres2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_criancas_pobres.map',
	params: {
		'LAYERS': 'criancas_pobres_0_6_2010',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCriancasMiseraveis2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_criancas_miseraveis.map',
	params: {
		'LAYERS': 'criancas_miseraveis_0_6_2010',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileRendaCrianca2010Munic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_renda_crianca.map',
	params: {
		'LAYERS': 'renda_crianca_municipio',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileRendaCrianca2010SetorCens = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_renda_crianca.map',
	params: {
		'LAYERS': 'renda_crianca_setor_censitario', //MAXSCALEDENOM
		'FORMAT': 'image/png'
	},
	serverType: 'mapserver'
});

var tileAbastAguaMunic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010.map',
	params: {
		'LAYERS': 'abastecimento_agua_2010_municipio',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileAbastAguaSetorCens = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010.map',
	params: {
		'LAYERS': 'abastecimento_agua_2010_setor_censitario', //MAXSCALEDENOM
		'FORMAT': 'image/png'
	},
	serverType: 'mapserver'
});

var tileAbastAguaPie2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2010_pie.map',
	params: {
		'LAYERS': 'abastecimento_agua_2010_pie',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileEsgotSanitAreaMunic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
	params: {
		'LAYERS': 'esgotamento_sanitario_2010_area_municipio',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileEsgotSanitSetorCens = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
	params: {
		'LAYERS': 'esgotamento_sanitario_2010_area_setor_censitario', //MAXSCALEDENOM
		'FORMAT': 'image/png'
	},
	serverType: 'mapserver'
});

var tileEsgotSanitPie2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_esgotamento_sanitario_2010.map',
	params: {
		'LAYERS': 'esgotamento_sanitario_2010_pie',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileDestLixo2010Munic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_destino_lixo_2010.map',
	params: {
		'LAYERS': 'destino_lixo_2010_area_municipio',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileDestLixo2010SetorCens = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_destino_lixo_2010.map',
	params: {
		'LAYERS': 'destino_lixo_2010_area_setor_censitario', //MAXSCALEDENOM
		'FORMAT': 'image/png'
	},
	serverType: 'mapserver'
});

var tileDestLixoPie2010 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_destino_lixo_2010_pie.map',
	params: {
		'LAYERS': 'destino_lixo_2010_pie',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tilePropCatolicos2010Munic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_prop_catolicos.map',
	params: {
		'LAYERS': 'prop_catholics_municipio',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tilePropCatolicos2010SetorCens = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_prop_catolicos.map',
	params: {
		'LAYERS': 'prop_catholics_setor_censitario', //MAXSCALEDENOM
		'FORMAT': 'image/png'
	},
	serverType: 'mapserver'
});

/** POLÍTICO-ADMINISTRATIVO **/

var tileEstados = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_estados.map',
	params: {
		'LAYERS': 'br_ufs',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileMunicipios = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_municipios.map',
	params: {
		'LAYERS': 'municipios_poligono',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileSedesMunicipais = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_sedes_municipais.map',
	params: {
		'LAYERS': 'sedes_municipais',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCapitais = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_capitais.map',
	params: {
		'LAYERS': 'capitais',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

/** SAÚDE **/

var tileUnidadesSaude = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_unidades_saude.map',
	params: {
		'LAYERS': 'UnidadesSaude',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

/** PROJETO 1.000.000 DE CISTERNAS **/

var tileCisternasDentroMunic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_cisternas_dentro_munic.map',
	params: {
		'LAYERS': 'cisternasDentroMunic',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCisternasForaMunic = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_cisternas_fora_munic.map',
	params: {
		'LAYERS': 'cisternasForaMunic',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

/** CENSO IBGE 2000 **/

var tileAbastAgua2000 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_abastecimento_agua_2000.map',
	params: {
		'LAYERS': 'abastecimento_agua_2000',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileMenores1Ano2000 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_menores_1_ano_2000.map',
	params: {
		'LAYERS': 'criancasmenos1',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileCriancas1a5Anos2000 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_criancas_1_a_5_anos.map',
	params: {
		'LAYERS': 'criancas1a5',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileAnosEstudo2000 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_anos_estudo.map',
	params: {
		'LAYERS': 'anosestudo',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});

var tileRendaFamilia2000 = new ol.source.TileWMS({
	url: urlOrigin+localMapfile+'brasil_renda_familia.map',
	params: {
		'LAYERS': 'rendafamilia',
		'FORMAT': 'image/png',
		TRANSPARENT: true,
		'TILED': true
	},
	serverType: 'mapserver'
});


//Serviço de Mapas (OpenStreetMap)
var layerOSM = new ol.layer.Tile({
					source: new ol.source.OSM(),
					preload: Infinity
				});

//Pastoral da Criança (Setores)
var layerSetores = new ol.layer.Tile({
						source: tileSetores,
						preload: Infinity
					  });

//Pastoral da Criança (Ramos)
var layerRamos = new ol.layer.Tile({
						source: tileRamos,
						preload: Infinity
					  });

//Pastoral da Criança (Comunidades ativas)
var layerComunidadesPontoMedio = new ol.layer.Tile({
						source: tileComunidadesPontoMedio,
						preload: Infinity
					  });

//Pastoral da Criança (Crianças com horta em casa)
var layerHortaCasa = new ol.layer.Tile({
						source: tileHortaCasa,
						preload: Infinity
					  });

//Pastoral da Criança (Pontos crianças)
var layerPontosCriancas = new ol.layer.Tile({
						source: tilePontosCriancas,
						preload: Infinity
					  });

//Pastoral da Criança (Pontos gestantes)
var layerPontosGestantes = new ol.layer.Tile({
						source: tilePontosGestantes,
						preload: Infinity
					  });

//Pastoral da Criança (Pontos bebês)
var layerPontosBebes = new ol.layer.Tile({
						source: tilePontosBebes,
						preload: Infinity
					  });

//Pastoral da Criança (Pontos bebês)
var layerGraficoPontos = new ol.layer.Tile({
						source: tileGraficoPontos,
						preload: Infinity
					  });


//CNBB (Dioceses)
var layerDioceses = new ol.layer.Tile({
						source: tileDioceses,
						preload: Infinity
					  });

//CNBB (Paroquias)
var layerParoquias = new ol.layer.Tile({
						source: tileParoquias,
						preload: Infinity
					  });

//CNBB (Sedes das Paroquias)
var layerSedesParoquias = new ol.layer.Tile({
						source: tileSedesParoquias,
						preload: Infinity
					  });

//Censo IBGE 2010 (Crianças menores de 1 ano)
var layerMenores1Ano2010 = new ol.layer.Tile({
						source: tileMenores1Ano2010,
						preload: Infinity
					  });

//Censo IBGE 2010 (Crianças de 0 a 6 anos incompletos)
var layerCriancas0a6Anos2010 = new ol.layer.Tile({
						source: tileCriancas0a6Anos2010,
						preload: Infinity
					  });

//Censo IBGE 2010 (Crianças Pobres de 0 a 6 anos)
var layerCriancasPobres2010 = new ol.layer.Tile({
						source: tileCriancasPobres2010,
						preload: Infinity
					  });

//Censo IBGE 2010 (Crianças miseráveis de 0 a 6 anos)
var layerCriancasMiseraveis2010 = new ol.layer.Tile({
						source: tileCriancasMiseraveis2010,
						preload: Infinity
					  });
					  
//Censo IBGE 2010 (% de crianças de 0 a 6 anos em faixas de renda)
var layerRendaCrianca2010 = new ol.layer.Group({
								title: 'RendaCrianca2010',
								layers: [new ol.layer.Tile({
											source: tileRendaCrianca2010Munic,
											preload: Infinity
										 }),
										 new ol.layer.Tile({
											source: tileRendaCrianca2010SetorCens,
											preload: Infinity
										 })
										]
							});

//Censo IBGE 2010 (Água - Percentual de domicílios abastecidos pela rede geral)
var layerAbastAgua2010 = new ol.layer.Group({
							title: 'AbastAgua2010',
							layers: [new ol.layer.Tile({
										source: tileAbastAguaMunic,
										preload: Infinity
									 }),
									 new ol.layer.Tile({
										source: tileAbastAguaSetorCens,
										preload: Infinity
									 })
									]
						 });

//Censo IBGE 2010 (Água - Detalhes do abastecimento em gráficos)
var layerAbastAguaPie2010 = new ol.layer.Tile({
						source: tileAbastAguaPie2010,
						preload: Infinity
					  });

//Censo IBGE 2010 (Esgotamento sanitário - Indicador em áreas)
var layerEsgotSanit2010 = new ol.layer.Group({
							title: 'EsgotSanit2010',
							layers: [new ol.layer.Tile({
										source: tileEsgotSanitAreaMunic,
										preload: Infinity
									 }),
									 new ol.layer.Tile({
										source: tileEsgotSanitSetorCens,
										preload: Infinity
									 })
									]
						 });

//Censo IBGE 2010 (Esgotamento sanitário - Detalhes em gráficos)
var layerEsgotSanitPie2010 = new ol.layer.Tile({
						source: tileEsgotSanitPie2010
					  });

//Censo IBGE 2010 (Destino do lixo - Indicador em áreas)
var layerDestLixo2010 = new ol.layer.Group({
							title: 'DestLixo2010',
							layers: [new ol.layer.Tile({
										source: tileDestLixo2010Munic,
										preload: Infinity
									 }),
									 new ol.layer.Tile({
										source: tileDestLixo2010SetorCens,
										preload: Infinity
									 })
									]
						 });

//Censo IBGE 2010 (Destino do lixo - Detalhes em gráficos)
var layerDestLixoPie2010 = new ol.layer.Tile({
						source: tileDestLixoPie2010
					  });

//Censo IBGE 2010 (% de católicos em relação a outras religiões e sem religião)
var layerPropCatolicos2010 = new ol.layer.Group({
	title: 'PropCatolicos2010',
	layers: [new ol.layer.Tile({
				source: tilePropCatolicos2010Munic,
				preload: Infinity
			 }),
			 new ol.layer.Tile({
				source: tilePropCatolicos2010SetorCens,
				preload: Infinity
			 })
			]
});

//Politico-administrativo (Estados brasileiros)
var layerEstados = new ol.layer.Tile({
					source: tileEstados,
					preload: Infinity
				});

//Politico-administrativo (Municipios)
var layerMunicipios = new ol.layer.Tile({
						source: tileMunicipios,
						preload: Infinity
					  });

	//Politico-administrativo (Sedes municipais)
	var layerSedesMunicipais = new ol.layer.Tile({
							source: tileSedesMunicipais,
							preload: Infinity
						  });

//Politico-administrativo (Capitais)
var layerCapitais = new ol.layer.Tile({
						source: tileCapitais,
						preload: Infinity
					  });

//Saúde (Unidades de Saúde)
var layerUnidadesSaude = new ol.layer.Tile({
						source: tileUnidadesSaude,
						preload: Infinity
					  });

//Serviço de Mapas (Cigex)
var layerCigex = new ol.layer.Tile({
					source: new ol.source.TileWMS({
						url: 'http://mapas.mma.gov.br/cgi-bin/mapserv?map=/opt/www/html/webservices/baseraster.map',
						params: {
							'LAYERS': 'baseraster',
							'FORMAT': 'image/png'
						},
						serverType: 'mapserver'
					}),
					preload: Infinity
				});

//Projeto 1.000.000 de cisternas (Cisternas Dentro do Município Declarado)
var layerCisternasDentroMunic = new ol.layer.Tile({
						source: tileCisternasDentroMunic,
						preload: Infinity
					  });

//Projeto 1.000.000 de cisternas (Cisternas Fora do Município Declarado)
var layerCisternasForaMunic = new ol.layer.Tile({
						source: tileCisternasForaMunic,
						preload: Infinity
					  });

//Censo IBGE 2000 (Abastecimento de água)
var layerAbastAgua2000 = new ol.layer.Tile({
						source: tileAbastAgua2000,
						preload: Infinity
					  });

//Censo IBGE 2000 (Criancas menores de 1 ano)
var layerMenores1Ano2000 = new ol.layer.Tile({
						source: tileMenores1Ano2000,
						preload: Infinity
					  });

//Censo IBGE 2000 (Criancas de 1 a 5 anos)
var layerCriancas1a5Anos2000 = new ol.layer.Tile({
						source: tileCriancas1a5Anos2000,
						preload: Infinity
					  });

//Censo IBGE 2000 (Média de anos de estudo)
var layerAnosEstudo2000 = new ol.layer.Tile({
						source: tileAnosEstudo2000,
						preload: Infinity
					  });

//Censo IBGE 2000 (Media de renda do chefe da familia (salário mínimo R$151,00))
var layerRendaFamilia2000 = new ol.layer.Tile({
						source: tileRendaFamilia2000,
						preload: Infinity
					  });
