initialZoom = getMinZoom();

if (box_sequence != '') {
	var map = new ol.Map({
		target: 'map',
		controls: [],
		view: new ol.View({
			projection: 'EPSG:4326',
			center: [-51.9253, -14.235],
			minZoom: initialZoom, //Zoom minimo no mapa
			maxZoom: 20, //Zoom minimo no mapa
			zoom: initialZoom
		})
	});
} else {
	var map = new ol.Map({
		target: 'map',
		controls: ol.control.defaults().extend([
			new ol.control.FullScreen({
				source: 'fullscreen' //Side panel continua com fullscreen
			})
		]),
		view: new ol.View({
			projection: 'EPSG:4326',
			center: [-51.9253, -14.235],
			minZoom: initialZoom, //Zoom minimo no mapa
			maxZoom: 20, //Zoom minimo no mapa
			zoom: initialZoom
		})
	});

	map.addLayer(layerEstados);
	map.addLayer(layerCapitais);
}
map.addLayer(layerOSM);

var view = map.getView();
$("canvas").attr("id", "canvasMap");

if (!!time_reload_tmp) {
	layerAppvisita = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: urlOrigin+localMapfile+'brasil_comunidades_appvisita.map',
			params: {
				'LAYERS': 'comun_appvisita',
				'users_id': (users_id == '' ? '' : users_id),
				'FORMAT': 'image/png',
				TRANSPARENT: true,
				'TILED': true
			},
			serverType: 'mapserver'
		}),
		preload: Infinity
	});
	layerAppvisita.setZIndex(5);
	map.addLayer(layerAppvisita);
}
