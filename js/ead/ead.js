$(function () {

    function bindFilters(){

        $("[data-filter]").change(function(){

            const target = $(this).data('filter-target');
            const filter = $(this).data('filter');
            const material = $('[data-material]').val();
            let status = $(this).is(':checked');

            if(status){ status = 1; }else{ status = 0;}
            if(!status){ $(target).hide(); }else{ $(target).show();}

            setCookie(material, 'filter.'+filter,status);
        });

    }

    function bindRemoteForm(scope){

        $(scope).find("[data-remote-form]").click(function(e){
            e.preventDefault();

            let href = $(this).attr('href');
            let ref = $(this).attr('ref');
            let params = {};

            try{
                let stringParams = $(this).data('params');

                if(typeof stringParams != 'undefined'){
                    params = JSON.parse('{'+$(this).data('params')+'}');
                    params = $.param(params);
                }else{
                    params = null;
                }

                let openedWindow = window.open( href + '?' + params , 'popup' );

                openedWindow.addEventListener('beforeunload', function(){

                    $.post('/ead/stages/updatestage', {}, function(){

                    });

                });

            }catch(error)
            {
                console.error(error);
            }
        });
    }

    function bindSortable(scope){

        $(scope).find(".sortable").sortable({
            handle: '.sortable-handler',
            update: function(event, ui){

                //Envia a informação da ordenação
                let $form = $('#orderForm');
                let dataArray = $form.serializeArray();

                dataArray.push({
                    'name' : 'trigger',
                    'value' : $(ui.item[0]).data('refresh')
                });

                dataArray.push({
                    'name' : 'trigger-id',
                    'value' : $(ui.item[0]).data('refresh-id')
                });

                $.post('/ead/index/update_order',dataArray);

            }
        });
    }

    function bindCollapsable(scope){

        $(scope).find('.collapsable-handler')
            .click(function() {
                $(this).parent('.sortable-header').toggleClass('active');

                $(this).toggleClass('fa-arrow-down');
                $(this).toggleClass('fa-arrow-up');

                const parentName = $(this).data('cookie');
                const material = $('[data-material]').val();

                let value = $(this).parent('.sortable-header').hasClass('active');

                if(value){ value = 1 }else{ value = 0 }
                setCookie(material, parentName, value );

                return false;
            });
    }

    function bindTemplateAppend(scope){

        $(scope).find("[data-new-template]").click(function(event){

            event.preventDefault();

            let type = $(this).data('new-template');
            let idRef = $(this).data('new-ref');
            let uuid = generateUUID();
            let stringParams = $(this).data('params');

            try{
                let newElement = $($(type).html());
                newElement.attr('data-uuid', uuid);
                newElement.find('a').attr('data-params', stringParams);
                newElement.find('[data-order]').attr('value', uuid);

                let params = JSON.parse('{'+stringParams+'}');
                params['sequence'] = newElement.find('[data-order]').data('order');

                $(idRef).after(newElement);
                initBindings(newElement);

            }catch(error)
            {
                console.error(error);
            }

        });
    }

    function bindOpenAssociation(scope){
        $(scope).find("[data-associate]");
    }

    function initBindings(scope){

        bindRemoteForm(scope);
        bindSortable(scope);
        bindCollapsable(scope);
        bindTemplateAppend(scope);
        bindOpenAssociation(scope);
        bindFilters();
        $('#orderForm').show();

    }

    function generateUUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function setCookie(material, cname, cvalue) {
        let d = new Date();
        d.setTime(d.getTime() + (1*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();

        let currentCookie = getCookie(material);
        try{
            if(currentCookie === ''){ currentCookie = '{}'; }

            let parsedCookie = JSON.parse(currentCookie);
            if(cvalue){
                parsedCookie[cname] = cvalue;
            }else{
                if(parsedCookie.hasOwnProperty(cname) && parsedCookie[cname]){
                    delete parsedCookie[cname];
                }
            }

            let finalValue = JSON.stringify(parsedCookie);
            document.cookie = material + "=" + finalValue + ";" + expires + ";path=/";
        }catch (e) {
            alert('Ocorreu um erro ao recuperar o estado da página, o estado foi resetado');
        }
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    initBindings($("#material-tree"));

});


$(function() {
    if (localStorage.tempScrollTop) {
        $(window).scrollTop(localStorage.tempScrollTop);
    }
});

$(window).on("scroll", function() {
    localStorage.setItem("tempScrollTop", $(window).scrollTop());
});