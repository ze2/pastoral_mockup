//Adiciona um caracter a esquerda da string
function padLeft(numero, digitos, strpad){
    var pad = '';
    for(var x= 0; x<digitos; x++){
        pad += strpad;
    }
    return (pad + numero).slice(digitos *  -1);
}

var CE = function(tag, cls){
    var element = $(document.createElement(tag));
    if(cls) element.addClass(cls);
    return element;
};


/**
 *  Ajax requestion, to a specific url.
 *  @param string   url
 *    url to ajax request
 *  @param string   divResponse
 *    element to receive the html response
 *  @param boolean  effect
 *    flag para utilizar efeitos jquery.
 */
function ajaxRequest(url, divResponse, effect){
    if(effect == null)
        effect  = true;

    var div = '#'+ divResponse;

    $(div).hide();
    $('#carregando').show();
    $.post(
        url,
        function(data){
            $('#carregando').fadeOut('fast', function(){

                $(div).html(data);
                if(effect){
                    $(div).slideDown('slow', function(){
                        $(div).effect('highlight', {}, 500);
                    });
                }
                else{
                    $(div).show();
                }
            });
        }
    );
}

var display = function(url, options){

    $('#carregando').show();
    $.get(url, function(html){

        $(html).dialog(options);
        $('#carregando').hide();
    });
}

/**
 *  Utilizando a biblioteca jquery.tooltip, habilita tooltips
 *  das imagens com classe tooltipImage de um elemento com classe tooltip
 *  ou das imagens com classe imgLinkSmall.
 *
 *  Inicializa também os datepickers utilizando jquery-ui
 *  em todos os elementos que possuirem a classe .formDatePicker
 */
var loadingStart = function(href){
    var ok = checkIfLoadingIsPossible();
    
    if(href == undefined || href == 'undefined' || href=='' || href ==null){
        var href = $(this).attr("href");
    }
    
    if(href === undefined){
        loadingStartAction();
    }else{
        if(href  !== '#' 
                && ok 
                && href.substr(9)  !== 'javascript' 
                && href.substr(5)  !== 'void('
        ){
            loadingStartAction();
        }
    }
    
};

var loadingStartAction = function(){
    $("#loading").addClass('show');
    $("body").css('overflow', 'hidden');
};

var loadingStop  = function(){
    $("#loading").removeClass('show');
    $("body").css('overflow', 'auto');
};
var checkIfLoadingIsPossible = function(){
    if(keys['16']  === 'down' || 
       keys['17']  === 'down' || 
       keys['18']  === 'down' ||
       keys['255'] === 'down' ||
       keys['91']  === 'down'){
        return false;
    }else{
        return true;
    }
};

var keys = {};
$(document).ready(function(){
    $("body").on("keydown", function(e){
        keys[e.keyCode] = 'down';
    });
    $("body").on("keyup", function(e){
        keys[e.keyCode] = 'up';
    });
    $("#menu-accordion-modal a").on("click", function(){
        loadingStart($(this).attr("href"));
    });
    $(".tooltip, .tooltip img.tooltipImage, img.imgLinkSmall, ul#selecaoIdioma li a img, #topBarItemi18n a img").tooltip({
        track       : true,
        delay       : 0,
        showURL     : false,
        opacity     : 1,
        fixPNG      : true,
        showBody    : " - ",
        extraClass  : "pretty fancy",
        top         : -15,
        left        : 5
    });

    /// Picker para campos de data
    $(".formDatePicker").datepicker($.datepicker.regional['pt-BR']);

    /// Mask para telefones
    $(".phoneField").mask("(99)9999-9999");

    /// Mask para cep
    $(".zipCodeField").mask("99999-999");

    $(".time").focusout(function(){
        var $this = $(this);
        var value = $this.val();
        if(value.search('_') != -1) $this.val('');

    }).mask("99:99");

    /// Mask para code11
    $('input[type=text].code11').keyup(function(){
        code11Mask($(this));
    });
    $('input[type=text].code11').change(function(){
        validate11Code($(this));
    });
    
    $('.helper_tooltip_info img').click(function(){
        
        var parent = $(this).parent();
        
        var title = parent.attr('text');
        var description = parent.attr('description');
        
        var div = CE('div');
        div.attr('title', title);
        
        var cont_description = CE('div');
        div.append(cont_description);
        cont_description.html(description);
        
        div.dialog({modal: true,
           width: '500px'
    });
    });
    
    //permite entrada somente de numeros decimais
    $(".numeric").on("keyup", function(event){
        var scale = $(this).data('scale');
        if(scale==null){
            scale = 0;
        }
        var value = fixDecimal($(this).val(), scale);

        $(this).val(value);
    });

    $('.fixInt').keyup(function(e){
        // passando o valor para a função retornar apenas números
        var valueInt = fixInt($(this).val());
        //colocando o valor de volta no input
        $(this).val(valueInt);        
    });
    
});

//Função utilizada em campos que podem entrar apenas numeros
function fixDecimal(value, n){
    if(value.length < n){
        var q = n - value.length;
        for(var i=-1; q > i; i++){
            value = '0' + value;
        }
    }

    value = value.replace(',', '').replace('.', '');
    value = parseFloat(value);
    var pot = Math.pow(10, (n * -1));
    value = value * pot;
    value = value.toFixed(n);
    value = value.replace(",", ".");
    if(value === 'NaN'){
        value = '';
    }
    return value;
}

//permite entrada somente de numeros inteiros
function fixInt(value){
      
    if(value.trim() == '-'){
      return '-';
    }

    //aplica expressão regular em value para deixar entrar apenas numeros
    var regex = value.match(/^-?\d+/);
    //utliza a regex apenas se tiver retornado algo (se voltar null não tinha números)
    var value2 = (regex !== null)? regex : ''; 
    return value2;
}

/**
 *  Gerencia elementos, ocultando ou exibindo.
 *  Altera a imagem de exibição para a ação adequada.
 *  @param id
 *    id do elemento que deve ser gerenciado
 */
function manageDiv(id){
  div = $("#"+ id);

  div.slideToggle('slow', function(){
    if(div.css('display') == 'block'){
      div.children("fieldset").effect("highlight", {}, 500);
    }
  });
}

/**
 *  Quando um valor for verdadeiro,
 *    oculta um elemento informado e limpa seu valor.
 *  Qualdo for falso,
 *    Exibe o elemento informado.
 *  @param selecionado
 *    booleano para fazer a verificação
 *  @param id
 *    id do elemento que será gerenciado
 */
function ocultarField(selecionado, id){
  field = $("#"+ id);
  if(selecionado){
    field.hide();
    field.val(null);
  }
  else{
    field.show();
  }
}

/**
 *  Exibe / oculta os campos de filtro e legenda de ações
 *  na listagem de radios, módulo radios, controller index
 *  action index.
 */
function manageListagemIndex(id){
  elemento  = $("#"+ id);

  display = elemento.css('display');

  if(display == 'none'){
    elemento.slideDown();
    $("#"+ id +"Show").hide();
    $("#"+ id +"Hide").show();
  }
  else{
    elemento.slideUp();
    $("#"+ id +"Hide").hide();
    $("#"+ id +"Show").show();
  }
}

/**
 *  Altera a visualização dos dados das telas de listagem,
 *  mudando o campo tipo_visualizacao para o valor informado.
 *  @param string tipo
 */
function mudaVisualizacao(tipo){
  $('#tipo_exibicao').val(tipo);
}


var get = function(path, value){

    if(typeof(value) == 'object') value = JSON.stringify(value);
    if(value != undefined) value += '?' + value;

    window.location.href = path;
};

var help = function(site){
    $('#carregando').show();
    $.get('/permission/help', {'id': site}, function(data){
        $(data).dialog({'width': 800, 'maxHeight': 800, resizable: false});
        $('#carregando').hide();
    })
};

/* Set callback(module)
*/
var require = function(url, callback){

   var result = function(data, status, jqxhr){
     callback(window.module || data, status, jqxhr);
     window.module = undefined;
   };

   return $.ajax({
     url: url,
     cache: true,
     dataType: "script",
     success: result,
   });
};

var require_once = function(url, callback){
    if(!window.urls){
        window.urls = [];
    }

    var i = 0;
    for(i in window.urls){
        if(url==urls[i]){
            callback(window.module);
            return;
        }
    }

    window.urls[i]=url;
    require(url, callback);
};


var rpc = function(){
    var method = arguments[0];
    var uri = '/rest/index/model/' + method + '?params=';
    var callback = undefined;
    var params = [];
    for(var x = 1; x < arguments.length; x++){
        var arg = arguments[x];
        if(typeof arg == 'function'){
            callback = arg;
        }else{
            params.push(arg);
        }
    };
    uri += JSON.stringify(params);
    var loading = $('#carregando').show();
    return $.getJSON(uri, function(data){ loading.hide(); callback(data)}).fail(function(){
        loading.hide();
    });
};

// Faz o enter funcionar como Tab
$.fn.enterToTab = function(input_submit){

    var edit = $('#edit');
    if(edit.size() > 0){
        $(window).keypress(function(event) {
            if (!(event.which == 115 && event.ctrlKey) && !(event.which == 19)) return true;
            edit.trigger('click');
            event.preventDefault();
            return false;
        });
    }
    
     var self = $(this);
    $(this).data('send', false);

    console.log($(this), $(this).data('send'));

    if(input_submit == undefined) input_submit = '#send';

    $(this).bind("keyup", function(e) {

        $('input[tabindex]').removeAttr('tabindex');
        var code = e.keyCode || e.which;
        if (code == 13) {
            var target = e.target;
            var found  = false;

            var inputs = self.find('input').add('button');
            for(var i in inputs){

                var v = inputs[i];

                if(found){
                    var input = $(v);
                    if(input.is(':visible') && !input.attr('disabled')){
                        input.focus();
                        break;
                    }else{
                        continue;
                    }
                }

                if(target == v){
                    found = true;
                    continue;
                }

            };
        }
    }).submit(function(event){
        return self.data('send');
        
    }).find(input_submit).click(function(event){
        self.data('send', true);
        $('#target').val(event.target.name);
        self.submit();
    });
};
