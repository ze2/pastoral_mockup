/**
 *  Preenche o campo de telefone com a máscara correta
 *  para o formato (41)3030-3030
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function formPhoneMasc(fieldId){
  var field = document.getElementById(fieldId);
  if (field.value.length == 1){
    field.value = '(' + field.value;
  }

  if (field.value.length == 3){
    field.value = field.value + ')';
  }

  if (field.value.length == 8){
    field.value = field.value + '-';
  }
  return true;
}

/**
 *  Faz uma pré-validação do campo de telefone.
 *  Se for apenas '(', apaga.
 *  @param  string fieldId
 */
function validatePhone(fieldId){
  var field = document.getElementById(fieldId);
  if(field.value == '(' || field.value == '(('){
    field.value = '';
  }
  else{
    if(field.value.length > 1){
      var er      = new RegExp(/^\(\d{2}\)\d{4}-\d{4}$/);
      if(!er.test(field.value)){
        alert('Telefone não está no formato correto. Ex (41)3030-3030');
        field.style.border = 'solid 1px #f00';
      }
      else{
        field.style.border = 'solid 1px #ccc';
      }
    }
  }
}

/**
 *  Formata um campo numérico com ponto
 *  flutuante e precisão de 2 casas decimais.
 *  @param  string fieldId
 */
function formMoneyMasc(fieldId){
  var field   = document.getElementById(fieldId);
  var valor   = field.value;
  var size    = valor.length;

  /// Nada a ser feito. Retorna o valor
  if (size == 0){
    return true;
  }
  if (size == 1){
    valor = "0.0"+ valor;
    field.value = valor;
    return true;
  }

  /// Remover o ponto
  tmp = "";
  for(i = 0; i < size; i++){
    if(valor[i] != ".")
      tmp = tmp + valor[i];
  }
  valor = tmp;

  /// Colocar no mínimo em 3 dígitos
  size  = valor.length;

  /// Remover o 0 caso não necessite
  if(size == 4 && valor[0] == 0){
    tmp = "";
    for(i = 1; i < size; i++){
      tmp = tmp + valor[i];
    }
    valor = tmp;
  }

  /// Quebrar o valor em inteiro e ponto flutuante
  size    = valor.length;
  limit   = size - 2;
  first   = valor.substr(0, limit);
  second  = valor.substr(limit);

  field.value = first+ "." +second;
  return true;
}

function validateMoney(fieldId){
  var field = document.getElementById(fieldId);
  var er      = new RegExp(/^\d*\.\d{2}$/);

  if(!er.test(field.value)){
    alert('Número inválido. Utilize separador "." e 2 casas de precisão (Ex. 1000.00)');
    field.style.border = 'solid 1px #f00';
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 *  Preenche o campo de CEP com a máscara correta
 *  para o formato 80011-970
 *  @author Rodrigo Maia
 *  @date   2008-11-24
 *  @param  string fieldId
 */
function formCepMasc(fieldId){
  var field = document.getElementById(fieldId);

  if (field.value.length == 5){
    field.value = field.value + '-';
  }
  return true;
}

/**
 *  Faz uma pré-validação do campo de telefone.
 *  Se for apenas '(', apaga.
 *  @author Rodrigo Maia
 *  @date   2008-11-18
 *  @param  string fieldId
 */
function validateCep(fieldId){
  var field = document.getElementById(fieldId);
  if(field.value.length > 1){
    var er      = new RegExp(/^[0-9]{5}-[0-9]{3}$/);
    if(!er.test(field.value)){
      alert('CEP não está no formato correto. Ex 99999-999');
      field.style.border = 'solid 1px #f00';
    }
    else{
      field.style.border = 'solid 1px #ccc';
    }
  }
}

/**
 *  Preenche o campo de CGC com a máscara correta
 *  para o formato 99.999.999/9999-99
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function formCgcMasc(fieldId){
  var field = document.getElementById(fieldId);
  if (field.value.length == 2){
    field.value = field.value + '.';
  }

  if (field.value.length == 6){
    field.value = field.value + '.';
  }

  if (field.value.length == 10){
    field.value = field.value + '/';
  }

  if (field.value.length == 15){
    field.value = field.value + '-';
  }

  return true;
}

/**
 *  Faz uma pré-validação do campo de CGC.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateCgc(fieldId){
  var field = document.getElementById(fieldId);
  var er      = new RegExp(/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/);

  if(field.value.length == 0){
    return true;
  }

  if(!er.test(field.value)){
    alert('CGC não está no formato correto. Ex 99.999.999/9999-99');
    field.style.border = 'solid 1px #f00';
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 *  Faz uma pré-validação do campo de e-mail.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateEmail(fieldId){
  return true;
}

/**
 *  Preenche o campo de CPF com a máscara correta
 *  para o formato 999.999.999-99
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function formCpfMasc(fieldId){
  var field = document.getElementById(fieldId);
  if (field.value.length == 3 || field.value.length == 7){
    field.value = field.value + '.';
  }

  if (field.value.length == 11){
    field.value = field.value + '-';
  }

  return true;
}

/**
 *  Faz uma pré-validação do campo de CGC.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateCpf(fieldId){
  var field   = document.getElementById(fieldId);
  var er      = new RegExp(/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/);

  if(field.value.length == 0){
    return true;
  }

  if(!er.test(field.value)){
    alert('CPF não está no formato correto. Ex 999.999.999-99');
    field.style.border = 'solid 1px #f00';
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 *  Preenche o campo de hora
 *  para o formato 01:23
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function formTimeMasc(fieldId){
  var field = document.getElementById(fieldId);
  if (field.value.length == 2){
    field.value = field.value +':';
  }
  return true;
}

/**
 *  Faz uma pré-validação do campo de hora.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateTime(fieldId){
  var field   = document.getElementById(fieldId);
  var er      = new RegExp(/^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/);

  if(field.value.length == 0){
    return true;
  }

  if(!er.test(field.value)){
    alert('Hora inválida');
    field.style.border = 'solid 1px #f00';
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 *  Preenche o campo de data
 *  para o formato 31/12/2010
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function formDateMasc(fieldId){
    //Desativada por causa de problemas com backspace e del...
    //var field = document.getElementById(fieldId);
  //if (field.value.length == 2 || field.value.length == 5){
    //field.value = field.value +'/';
  //}
  return true;
}

/**
 *  Faz uma pré-validação do campo de data.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateDate(fieldId){
  var field   = document.getElementById(fieldId);
  var er      = new RegExp(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/);

  if(field.value.length == 0){
    return true;
  }
  
  var numbers =  field.value;
  numbers = numbers.replace(/\D/g,'');
  var newDate = '';
  var day;
  var month;
  var year;
  
  if(numbers.length == 8){ //ddmmyyyy
    day = numbers.substr(0,2);
    month = numbers.substr(2,2);
    year = numbers.substr(4,4) * 1;
    
    field.value = day + '/' + month + '/' + year;  
  }if(numbers.length == 6){ //ddmmyy
    day = numbers.substr(0,2);
    month = numbers.substr(2,2);
    year = numbers.substr(4,2) * 1;
    if(year < 30){
        year = numbers.substr(4,2) * 1 + 2000;
    }else{
        year = numbers.substr(4,2) * 1 + 1900;
    }
    
    field.value = day + '/' + month + '/' + year;  
  }


  if(!er.test(field.value)){
    alert('Data inválida');
    field.style.border = 'solid 1px #f00';
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 * Create a dd/mm masc.
 * @param string fieldId
 */
function formDayMonthMasc(fieldId){
  var field   = $("#"+fieldId);

  fieldValue  = field.val();

  if(fieldValue.length == 2){
    field.val(field.val()+"/");
  }

  return true;
}

/**
 *  dd/mm pre-validation
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateDayMonth(fieldId){
  var field   = $("#"+fieldId);
  var er      = new RegExp(/^(0[1-9]|[1-2]\d|3[0-1])\/(0[1-9]|1[0-2])$/);

  if(field.val().length == 0){
    field.css("border", "solid 1px #ccc");
    return true;
  }

  if(!er.test(field.val())){
    alert('Data inválida');
    field.css("border", "solid 1px #f00");
  }
  else{
    field.css("border", "solid 1px #ccc");
  }
  return true;
}

/**
 *  Permite somente números.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateNumber(fieldId){
  return true;
  var field = document.getElementById(fieldId);
  var er    = new RegExp(/^-?\d*$/);

  if(!er.test(field.value)){
    alert('Numero inválido');
    field.style.border = 'solid 1px #f00';

    size        = field.value.length - 1;
    novaString  = field.value.substr(0, size);

    field.value = novaString;
    return false;
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

/**
 *  Permite somente números.
 *  @author Rodrigo Maia
 *  @param  string fieldId
 */
function validateSignedNumber(fieldId){
  var field = document.getElementById(fieldId);
  var er    = new RegExp(/^-?\d*$/);

  if(!er.test(field.value)){
    alert('Numero inválido');
    field.style.border = 'solid 1px #f00';

    size        = field.value.length - 1;
    novaString  = field.value.substr(0, size);

    //field.value = novaString;
    return false;
  }
  else{
    field.style.border = 'solid 1px #ccc';
  }
  return true;
}

function code11Mask(element){
    fieldValue  = element.val();
    strSize     = fieldValue.length;

    /// Validate last char
    if(strSize == 0)
        return fieldValue;
    lastChar    = fieldValue[strSize - 1];
    if(lastChar == '-')
        return;

    var er      = new RegExp(/\d/);
    if(!er.test(lastChar)){
        element.val(fieldValue.substring(0, strSize - 1));
    }

    /// Create mask
    if(strSize <= 2)
        return;

    if(strSize == 3){
        element.val(fieldValue[0] + "-" + fieldValue.substring(1));
        return;
    }

    /// Remove '-'
    parts       = fieldValue.split('-');
    fieldValue  = parts.join("");

    /// Add '-' on the right place
    fieldValue  = fieldValue.substring(0, strSize - 3) + '-' + fieldValue.substring(strSize - 3);

    return element.val(fieldValue);
}

function validate11Code(element){
    fieldValue  = element.val();

    var er      = new RegExp(/^\d+\-\d{2}$/);
    if(!er.test(fieldValue)){
        alert("Digite o número correto");
        element.focus();
    }
}

/**
 * Verifica se um e-mail é valido (apenas sintaxe).
 * @param email
 * @returns {Boolean}
 */
function isEmail(email){
    var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
    var check=/@[\w\-]+\./;
    var checkend=/\.[a-zA-Z]{2,3}$/;
    if(((email.search(exclude) != -1)||(email.search(check)) == -1)||(email.search(checkend) == -1)){return false;}
    else {return true;}
}

ajax = {}
ajax.Post = function(uri, form, container){

    $('#carregando').show();
    var datas = $(form).serialize();
    console.log(datas);

    $.post(uri, datas, function(html){

        $(container).html('');
        $(container).html(html);
        $('#carregando').hide();
    });
}

/**
 * PErmite que apenas números sejam digitados em um input box. use com $('#obj').bind('keydown',filterOnlyNumbers);
 * @param {type} e
 * @returns {Boolean}
 */
function filterOnlyNumbers(e){

    //console.log(e);

    //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
    keyCodesPermitidos = new Array(8,9,37,39,46);

    //numeros e 0 a 9 do teclado alfanumerico
    for(x=48;x<=57;x++){
        keyCodesPermitidos.push(x);
    }

    //numeros e 0 a 9 do teclado numerico
    for(x=96;x<=105;x++){
        keyCodesPermitidos.push(x);
    }

    //Pega a tecla digitada
    keyCode = e.which;

    //Verifica se a tecla digitada é permitida
    if ($.inArray(keyCode,keyCodesPermitidos) != -1 && !e.shiftKey){
        return true;
    }
    
    if(e.ctrlKey){
        return true;
    }
    
    return false;
};

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(function(){
    if($('.money').maskMoney){
        $('.money').maskMoney({allowZero:false, allowNegative:true, defaultZero:true, decimal:'.', thousands:''});
    }
});

/**
 * Bloqueia a entrada de \ (barra) nos campos
 */
$(document).ready(function(){
    //funcionou só com keyup (?)
   $("body").delegate("input", "keyup keydown" , function(event){ 
       var value = $(this).val();
       var value2 = bloqChars(value, ["\\"]);
       //só redefino o valor do input caso necessário
       if(value !== value2){
           //capturo a posição do caret antes de redefinir o valor do input
           var start = event.target.selectionStart;
           var end = event.target.selectionEnd;
           //mudo o valor do input
           $(this).val(value2);
           //re-coloco o caret na posição certa, é necessário decrentar os valores
           //pq nós apagamos um caracter
           event.target.selectionStart = --start;
           event.target.selectionEnd = --end;
           
       }
   });

    $("body").ready(function() {
        
        var forbiden_characters = ['?', '&'];
        $("#form_generic_view").submit(function(e) {

            var form = $("#form_generic_view");
            
            $("#form_generic_view input, #form_generic_view textarea").each(function(key, element){
                
                var value = $(element).val();
                $.each(forbiden_characters, function(fKey, char) {
                    
                    value = value.split(char).join('');
                });
                
                $(element).val(value); 
            });
        }); 
    });

   
   activeF5Blocker();
});

function autoCorrectStepMinutes(field){
    var value = field.val(); console.log(value, value.length);
    switch(value.length){
        case 0 :
            var value = '00';
            field.val(value);
            break;
        case 1 :
            var value = value + '0';
            field.val(value);
            break;
    } console.log(value);
}

function bloqChars(value, chars){
    value2 = value;
    $.each(chars, function(key,char){
        value2 = value2.replace(char, "");
    });
   
   return value2;
}

function utf8_to_b64( str ) {
    return window.btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8( str ) {
    return decodeURIComponent(escape(window.atob( str )));
}

JSON.realStringify = JSON.stringify;
JSON.realParse     = JSON.parse;

function encode(val){
    var result = '*64*' + utf8_to_b64(val);
    return result;
}
function decode(val){
    var result = b64_to_utf8(val.substr(4));
    return result;
}

function encodeJson(json){
    var text = JSON.realStringify(json);   
    return encode(text);
}
function decodeJson(text){
    var json64 = decode(text);
    return JSON.realParse(json64);
}

JSON.parse = function(str){

  if(str.substr(0, 4) == '*64*'){
    return decodeJson(str);
  }
  return JSON.realParse(str);
};

JSON.stringify = function(str){
    return encodeJson(str);
};


//A ideia é que apenas o primeiro F5 pressionado pelo usuário surta efeito, os outros serão ignorados
//atpe que o browser recarregue.
var hasPressedF5 = false;
function activeF5Blocker()
{
    $cancelF5 = function(e){
         if(e.key == 'F5'){
             
             if(hasPressedF5){
                e.preventDefault();
                return false;
            }else{
                hasPressedF5 = true;
            }
         }
     }
    
    $('body').on('keyup', $cancelF5);
    $('body').on('keydown', $cancelF5);
    $('body').on('keypress', $cancelF5);
}

function fixDecimalOnly(value, n) {
    
    value = value.replace(',', '').replace('.', '');
    
    if(value.length < n) {
        
        var q = n - value.length;
        for(var i=-1; q > i; i++) {
            value = '0' + value;
        }
    } 
    
    value = parseInt(value);
    var pot = Math.pow(10, (n * -1));
    value = value * pot;
    value = value.toFixed(n);
    value = (isNaN(value))? '' : value;
    value = value.replace(".", ",");
    return value;
}

$.fn.fixDecimalsOnly = function(n) {
    
    var el    = $(this);
    $(this).on("keyup", function(event) {
        
        var value = fixDecimalOnly(el.val(), n);
        el.val(value);
    });
    $(this).ready(function(){
        
        var value = el.val().replace('.', ',');
        var valueSplit = value.split(',');
        
        var decimalLength = (valueSplit[1] !== undefined)? valueSplit[1].length : 0;
        
        if(decimalLength < n) {
            
            if(decimalLength === 0) {
                value += ',';
            }
            
            for(var n2 = n; decimalLength < n2; --n2) {
                
                value += '0';
            }
        }
        
        el.val(value);
    });
    
    return this;
};

function validateCnpj(s) {
    let cnpj = s.replace(/[^\d]+/g, '')

    // Valida a quantidade de caracteres
    if (cnpj.length !== 14)
        return false

    // Elimina inválidos com todos os caracteres iguais
    if (/^(\d)\1+$/.test(cnpj))
        return false

    // Cáculo de validação
    let t = cnpj.length - 2,
        d = cnpj.substring(t),
        d1 = parseInt(d.charAt(0)),
        d2 = parseInt(d.charAt(1)),
        calc = x => {
            let n = cnpj.substring(0, x),
                y = x - 7,
                s = 0,
                r = 0

            for (let i = x; i >= 1; i--) {
                s += n.charAt(x - i) * y--;
                if (y < 2)
                    y = 9
            }

            r = 11 - s % 11
            return r > 9 ? 0 : r
        }

    return calc(t) === d1 && calc(t + 1) === d2
}

function isValidCPF(number) {
    var sum;
    var rest;
    sum = 0;
    number = number.replace(/[^\d]+/g, '');

    if (number == "00000000000") return false;

    for (i=1; i<=9; i++) sum = sum + parseInt(number.substring(i-1, i)) * (11 - i);
    rest = (sum * 10) % 11;

    if ((rest == 10) || (rest == 11))  rest = 0;
    if (rest != parseInt(number.substring(9, 10)) ) return false;

    sum = 0;
    for (i = 1; i <= 10; i++) sum = sum + parseInt(number.substring(i-1, i)) * (12 - i);
    rest = (sum * 10) % 11;

    if ((rest == 10) || (rest == 11))  rest = 0;
    if (rest != parseInt(number.substring(10, 11) ) ) return false;
    return true;
}
