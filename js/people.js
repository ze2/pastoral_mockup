/**
 *  Convert the birthday on format dd/mm/yyyy to yyyy-mm-dd to 
 *  pass by parameter
 */
function getPeopleByNameAndBirthday(personName, birthday){
  if(birthday == ''){
    url = '/people/edit/get-person-by-name-and-birthday/filter_person_name/'+ personName;
  }
  else{
    parts     = birthday.split('/');
    birthday  = parts[2] +"-"+ parts[1] +"-"+ parts[0];
    url       = '/people/edit/get-person-by-name-and-birthday/filter_person_name/'+ personName +'/filter_person_birthday/'+birthday;
  }
  ajaxRequest(
    url,
    'responsePersonData'
  );
}