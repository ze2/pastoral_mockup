        
function getId() {
    
    var id = null;
    let split  = window.location.href.split('/id/');
    
    if(split.length > 1){
        
        let split2 = split[1].split('/');
        var id = (split2.length > 0)? split2[0] : split[1];
    }
        
    return(id);
}

    
function hideShowThumbnail() {
    
    if(typesWithThumbnail !== undefined) {
        
        let val = parseInt($("#type_artifacts_id").val());

        if(typesWithThumbnail.includes(val)) {

            $("#div_thumbnail_id").slideDown();
        } else {

            $("#div_thumbnail_id").slideUp();
        }
    }
}

$("body").ready(function() {
    
    var id  = getId();
    
    $.each($('input[type=file]'), function() {
        
        let input   = $(this);
        let idSplit = input.attr('id').split("file_");
        let lang    = idSplit[1];

        artifacts.getArtifact(id, lang, function(artifact) {
            if(artifact.file.code === lang) {
                
                artifacts.showSmall(artifact, input, 'file');
            }
        });
    });
    
    hideShowThumbnail();
    
    $("#type_artifacts_id").on("change", function() {

        hideShowThumbnail();
    });

});

