    var hitBottom  = false;
    var searching  = false;
       
    var form = {};
        
    var search = function(clear){
        
        searching = true;
        if(clear === true){
            hitBottom  = false;
            offset     = 0;
            $(".criteria").each(function(){
                var input = $(this);
                if(input.attr('id') !== undefined){
                    form[input.attr('id')] = input.val();
                }
            });
        }
        
        loadingStart();
        
        var url  = baseUrl + "offset/" + offset;
        console.log(url, form);
        
        $.post(url,form,function(data){
            console.log(data.toString().length);
            if(data.toString().length < 5){
                hitBottom = true;
            }else{
                offset = offset + limit;
                if(clear === true){
                    $("#result").html( data );
                }else{
                    $("#result").append( data );
                }
                
                searching = false;
            }
            loadingStop();
        });

    };
    
    $("body").ready(function(){
        search(true);
        $(window).scroll(function() {
            if(hitBottom === false && searching === false && $(window).scrollTop() + $(window).height() >= $(document).height() - 400) {
                search();
            }
        });
        $("#send").on("click", function(e){
            e.stopPropagation();
            search(true);
        });
    });
