var user = user == undefined ? {} : user;
;(function(module){

    var element = function(id){
            
        var self = this;

        this.container  = $('#' + id + '_container');
        this.display = new widgets.elements.Up(id);
        this.display.onSearch = function(search){

            if(search.trim() == '') return;
            rpc('user.search_user', search, function(rowset){
                self.display._search(rowset, search);
            });
        };

        this.container.append(this.display.up);
    };
    module.Element = element;

    element.prototype.setValue = function(value){

        var self = this;

        if(value){
            rpc('user.search_user', value, function(rowset){
                self.display.rowset = rowset;
                self.display.setValue.call(self.display, value);
            });
        }
    };

}(user));
