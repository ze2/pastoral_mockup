
var filePrev = '';
var fileNext = '';
var epochTime = new Date().getTime();

function showItem(el, type){
        var type = el.data('type');
        var path = el.data('url') + '?t=' + epochTime;
        filePrev = getPrev(el);
        if(filePrev === null){
                $("#prev").css('display', 'none');
        }else{
                $("#prev").css('display', 'block');
        }
        fileNext = getNext(el);
        if(fileNext === null){
                $("#next").css('display', 'none');
        }else{
                $("#next").css('display', 'block');
        }
        $("#fileName").html(el.data("name"));
        switch(type){
                case 'video' :
                                var html = "<video controls><source src='"+path+"'/></video>";
                break;
                case 'image' :
                                var html = "<img src='"+path+"'/>";
                break;
        }
        $("#popContent").html(html);
        $("#popShow").fadeIn();
}

var getPrev = function(el){ 
    var prev = el.prev();
    console.log(prev);
    if(prev.length === 0){
        return null;
    }
    else{
        var prevType = prev.data('type');
        if(prevType === 'image' || prevType === 'video'){
            return prev;
        }else{
            return getPrev(prev);
        }
    }
};
var getNext = function(el){ 
    var next = el.next();
    if(next.length === 0){
        return null;
    }
    else{
        var nextType = next.data('type');
        if(nextType === 'image' || nextType === 'video'){
            return next;
        }else{
            return getNext(next);
        }
    }
};

var reloadList = function(){
    $("#loading").addClass('show');
    $( "#reload" ).load( window.location.href +" #filesList", function(){
        $("#loading").removeClass('show');
    });
};

var showAddFolderPopUp = function(){

    var html = '<label>Nome</label><input id="folderName" type="text"/>';
        popUp.abre( "addFolderPop",
                "Adicionar Pasta",
                html,
                "CANCELAR",
                "SALVAR",
                "confirm","p", "verde", "bola", 
                "",
                function(){
                    
                    addFolder();

                },
                function(){
                    $("#folderName").focus();
                }
        );
};
var addFolder = function(){
    var folderName = $("#folderName").val();
    if(folderName !== ''){
        var post = {postAction: 'addFolder', folderName: folderName};
        $.post('', post, function(ret){
            var data = $.parseJSON(ret);
            reloadList();
            popUp.fecha();
            console.log(data);
        });
    }
};

var deleteFolderPopUp = function(name, code){
        var html = 'Tem certeza que deseja apagar a pasta <br><b>'+ name +'</b>, e todo seu conteúdo?';
        popUp.abre( "delFolderPop",
                "Apagar Pasta",
                html,
                "CANCELAR",
                "APAGAR",
                "confirm","p", "vermelho", "bola", 
                "",
                function(){
                    
                    var post = {postAction: 'deleteFolder', code: code};
                    $.post('', post, function(ret){
                        var data = $.parseJSON(ret);
                        reloadList();
                        popUp.fecha();
                        console.log(data);
                    });
                },
                ''
        );
};
var deleteFilePopUp = function(name, code){
        var html = 'Tem certeza que deseja deletar o arquivo <br><b>"'+name+'"</b>?<br>Essa ação não poderá ser revertida!';
        popUp.abre( "delFilePop",
                "Apagar Arquivo",
                html,
                "CANCELAR",
                "APAGAR",
                "confirm","p", "vermelho", "bola", 
                "",
                function(){
                    var post = {postAction: 'deleteFile', code: code};
                    $.post('', post, function(ret){
                        var data = $.parseJSON(ret);
                        reloadList();
                        popUp.fecha();
                        console.log(data);
                    });
                },
                ''
        );
};

var loadResponseFromUpload = function(content){
    if(isJson(content)){
        var data = $.parseJSON(content);
        console.log(data);
        if(data !== null){
            if(data.status === 'ok'){
               reloadList();
            }
            if(data.status === 'error'){
               var html = data.message;
               popUp.abre( "error",
                       "Erro",
                       html,
                       "CANCELAR",
                       "APAGAR",
                       "alert","p", "vermelho", "bola", 
                       ''
               );
            }
        }

    }else{
        var html = 'Não foi possível realizar o upload deste arquivo.';
           popUp.abre( "error",
                   "Erro",
                   html,
                   "CANCELAR",
                   "APAGAR",
                   "alert","p", "vermelho", "bola", 
                   ''
           );
    }
};

var isJson = function(text){
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
        replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
        replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

          return true;

        }else{

          return false;

        }
};
function closeContent(){
    $("#popShow").fadeOut();
    $("#popContent").html('');
}

$("body").ready(function(){
    $("body").delegate(".download","click", function(e){
        e.stopPropagation();
    });
    $("body").delegate(".upload","click", function(e){
        e.stopPropagation();
    });
    $("body").delegate(".addFolder","click", function(e){
        e.stopPropagation();
        showAddFolderPopUp();
    });
    $("body").delegate(".image","click", function(e){
        e.stopPropagation();
        showItem($(this));
    });
    $("body").delegate(".video","click", function(e){
        e.stopPropagation();
        showItem($(this));
    });
    $("body").delegate(".url","click", function(){
        var url = $(this).data('url');
            window.open(url,'_blank');
    });
    $("body").delegate("#fecharPopShow","click", function(){
        closeContent();
    });
    $("body").delegate("#upload", "change", function(){
        $("#formUpload").submit();
    });
    $("#uploadFrame").on("load", function () {
        var content = $("#uploadFrame").contents().find("body").html();
        loadResponseFromUpload(content);       
    });
    $("body").delegate(".folder .delete", "click", function(e){
        e.preventDefault();
        e.stopPropagation();
        let i = $(this).data('i');
        var name = $("#dir_name_" + i).html();
        var code = $(this).data('code');
        deleteFolderPopUp(name, code);
    });
    $("body").delegate(".notFolder .delete", "click", function(e){
        e.preventDefault();
        e.stopPropagation();
        let i = $(this).data('i');
        var name = $("#f_name_" + i).html();
        var code = $(this).data('code');
        deleteFilePopUp(name, code);
    });
    $("body").delegate("#folderName", "keyup", function(e){
        var keyCode = e.keyCode;
        if(keyCode === 13){
            addFolder();
        }
       
    });
    $("#popShow").click(function(){
        closeContent();
    });
    $("body").delegate("#popContent img, #popContent video","click", function(e){
         e.stopImmediatePropagation();
    });
    $("#prev").on("click", function(e){
        e.stopPropagation();
        if(filePrev.length > 0){
            $("#popContent").animate({left: "100%"},300, function(){
                showItem(filePrev);
                $("#popContent").css('left', '-100%');
                $("#popContent").animate({left: 0},300);
            });
        }
    });
    $("#next").on("click", function(e){
        e.stopPropagation();
        if(fileNext.length > 0){
            $("#popContent").animate({left: "-100%"},300, function(){
                showItem(fileNext);
                $("#popContent").css('left', '100%');
                $("#popContent").animate({left: 0},300);
            });
        }
    });
});
