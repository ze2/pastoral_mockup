var locations = {};
;(function(locations){

    var CE = function(tag, clas){
        var tag = $(document.createElement(tag));
        if(clas) tag.addClass(clas);
        return tag;
    };

    var main = function(id, layout_options){
        if(!!!layout_options){
            layout_options={};
        }
        this.id = id;
        this.layout_options = layout_options;
        this.isSelectionVisible = true;
        this.isLabelVisible = true;
        this.containerSize = layout_options.hasOwnProperty('containerSize') ? layout_options.containerSize : '450px';
        this.inputSize = layout_options.hasOwnProperty('inputSize') ? layout_options.inputSize : '340px';
        this.labelSize = 'initial';
        this.firstLabelSize = 'initial';
        this.elements = [];
        this.isDisable = true;
        this.isReadOnly = false;
        this.optionals = []; //Tipos de localização destravados por naturesa (area, core, etc...)
        
        this._isDescribeBranchAsParish  = true;
        this._isDescribeSectorAsDiocese = true;

        this.dontPropagateEnter = $("input[type=submit]").size()>0;
    }
    locations.simple = main;

    main.prototype.setContainerSize = function(value){
        this.containerSize = value;
    }

    main.prototype.setLabelSize = function(value){
        this.labelSize = value;
    }

    main.prototype.setFirstLabelSize = function(value){
        this.firstLabelSize = value;
    }

    main.prototype.setInputSize = function(value){
        this.inputSize = value;
    }

    main.prototype.setValue = function(value){
        this.value = value;
        this.veritySecondEvent();

        if(value){
            var obj = {};
            obj.isDescribeBranchAsParish = this._isDescribeBranchAsParish;
            obj.isDescribeSectorAsDiocese = this._isDescribeSectorAsDiocese;
            obj.location_id = value;
            obj = [obj];

            var uri = '/rest/index/model/location.get_location_info?params=';
            uri += JSON.stringify(obj);

            var self = this;

            $.getJSON(uri, function(data){
                self.setLocation(data.location);
                self.setGenealogy(data.genealogy);
               
                parent = null
                $.each(self.location_types, function(k, v){
                    var lastLabel = '';
                    
                    if(!!self.genealogy && self.genealogy[v.name + '_name'] != undefined){
                        if(self.genealogy[v.name + '_external_id'] != undefined){
                            lastLabel = self.genealogy[v.name + '_external_id'] + ' - ' + self.genealogy[v.name + '_name'];
                        }else{
                            lastLabel = self.genealogy[v.name + '_name'];
                        }
                        
                        
                        v.selected = parseInt(self.genealogy[v.name + '_id']);
                        v.input.removeAttr('disabled');
                    }
                    
                    v.input.val(lastLabel);
                                        
                    if(parent && ! v.parent){
                        v.parent = parent
                    }
                    
                    parent = v;
                });
               
            }).fail(function(err){
                console.log( "error: ", err );
            });
        }
    }
    
    main.prototype.setDisable = function(flag){
        this.isDisable = flag;
    }

    main.prototype.setReadOnly = function(flag){
        this.isReadOnly = flag;
    }

    main.prototype.getValue = function(element){
        if(!element){
            return this.hidden.val();
        }
        if(this.elements[element]){
            return this.elements[element].selected;
        }
        return null;
    }

    main.prototype.setFirstTyping = function(value){
        this.first_typing = value;
    }

    main.prototype.setLocation = function(json_location){
        this.location = json_location;
    }

    main.prototype.setGenealogy = function(json_genealogy){
        this.genealogy = json_genealogy;
    }

    main.prototype.setOptionals = function(json_optionals){
        this.optionals = json_optionals;
    }

    main.prototype.setLocationTypes = function(location_types){
        this.location_types = location_types;
    }

    main.prototype.render = function(){
        var container = $('#container_' + this.id);
        container.append(this.input).css({'width': this.containerSize});
        this.makeInputs(container);
    }

    main.prototype.search = function(loc_type, value){

        var obj = {'type': loc_type.name, 'search': value};
        if(loc_type.parent) {
            obj.parent = loc_type.parent.selected;
        }else{
            //Se o elemento não possuir um value, significa que está totalmente em branco.
            if(this.getValue() > 0){
                obj.parent = this.getValue();
            }
        }
        if(this.getValue() > 0){
            obj.selected = this.getValue();
            if(!obj.parent){
                obj.parent = obj.selected;
            }
        }
        obj.isDescribeBranchAsParish = this._isDescribeBranchAsParish;
        obj.isDescribeSectorAsDiocese = this._isDescribeSectorAsDiocese;
        
        obj = [obj];
        var uri = '/rest/index/model/location.get_children?params=';
        uri += JSON.stringify(obj);

        var self = this;
        $.getJSON(uri, function(data){
           self.ignoreSearchResult=false;
           self.grid(loc_type, data);
        }).fail(function(err){
            console.log( "error: ", err );
        });
    }

    main.prototype.onSelect = function(event, element, type)
    {
        this.veritySecondEvent(event, element, type);
    }

    main.prototype.veritySecondEvent = function(e,element, type)
    {
        var firstType = $(element).attr('first_typing');
        var secondType = $(element).val();
        var firstTypeId = $(element).attr('first_typing_id');

        if(firstType==undefined ){
            return;
        }
        if(!type.selected){
            return;
        }

        e.stopPropagation();
        if(firstType != secondType){
            var response = question(firstType, secondType, type.label, function(){});
            if(response == firstType){
                $(element).val(response);
                this.hidden.val(firstTypeId);
                this.label.val(type.label + ': ' + response);
                type.selected = firstTypeId;
            }else{
                this.clearBelow(type, true);
                this.next(type, true);
            }
        }

    }


    main.prototype.makeInputs = function(container){

        var self = this;
        var last = null;
        var activeNext = false;
        $.each(this.location_types, function(k, v){
            v.input = CE('input', 'wdr').css({'width': self.inputSize});
            v.input.attr('type','text');
            v.input.attr('autocomplete', 'off');
                        
            if(self.isDisable){
                v.input.attr('disabled', 'disabled');
            }

            if(v.readOnly)
            {
                v.input.attr('readonly', 'true');
                v.input.addClass('readonly');
            }

            if(! self.firstInput ){
                self.firstInput = v.input;
            }

            v.input.attr({name: self.id + '_' + v.name, id: self.id + '_' + v.name, autocomplete: 'off', 'data-id': '' })
            self.elements[v.name] = v;
            v.input.attr('placeholder', v.label);

            self.installMove(v);
            if(self.genealogy){
                if(self.genealogy[v.name+'_id']){

                    var lastLabel = ""
                    if(self.genealogy[v.name + '_external_id'] != undefined){
                        lastLabel = self.genealogy[v.name + '_external_id'] + ' - ' + self.genealogy[v.name + '_name'];
                    }else{
                        lastLabel = self.genealogy[v.name + '_name'];
                    }

                    if(self.first_typing != '' && self.first_typing != undefined){
                        v.input.attr('first_typing', lastLabel);
                        v.input.attr('first_typing_id',self.genealogy[v.name + '_id']);
                        v.input.focusout(function(e){
                            element = this;
                            self.veritySecondEvent(e, element, v);
                        });
                    }else{
                        v.input.val(lastLabel);
                        v.selected = parseInt(self.genealogy[v.name + '_id']);
                        v.input.removeAttr('disabled');
                    }
                    
                    v.parent = last;
                    last = v;
                    activeNext=true;
                }else{
                    if(activeNext){
                        v.input.removeAttr('disabled');
                        activeNext = false;
                    }
                }

            }

            var containerInputSize = self.containerSize;
            if(self.layout_options.hasOwnProperty('containerInputSize')){
                containerInputSize = self.layout_options.containerInputSize;
            }

            var div = CE('div').css({'width': containerInputSize, 'float': 'left'});
            container.append(div);
            
            /*         
            var lbl = CE('span', 'wdl').text(v.label + ':');

            if(self.firstLabelSize!='initial'){
                lbl.css({'width': self.firstLabelSize});
                self.firstLabelSize='initial';
            }else if(self.labelSize!='initial'){
                lbl.css({'width': self.labelSize});
            }
            
            div.append(lbl);
            
            self.lbl = lbl;
            if(!self.isLabelVisible){
                lbl.hide();
            }
            
            */

            if(self.layout_options.hasOwnProperty('noSpan') && self.layout_options.noSpan){
                div.append(v.input);
            }else{
                div.append(CE('span').append(v.input));
            }

            //Evento keyup para busca
            v.input.keydown(function(event){
                var code = event.keyCode;
                if(code == 13){
                    event.preventDefault();
                }
            });
            v.input.keyup(function(event){

                if(v.readOnly)
                {
                    return;
                }

                var code = event.keyCode;
                if(code == 80 || code == 38 || code == 40 || code == 37 || code == 39 || code == 46 || code == 13 || code == 9 || (event.shiftKey && event.keyCode == 16)){
                    return;
                }

                self.clearBelow(v);
                var value = $(this).val();
                if(value.length > 0){
                    if(self.handleSearch>0){
                        clearTimeout(self.handleSearch);
                    }
                    var timeToSendSearch = 200;

                    self.handleSearch = setTimeout(function(){
                        self.search(v, value);
                    }, timeToSendSearch);
                }else{
                    self.clearGrid(v);
                }
            });

        });

        this.hidden = CE('input').attr({type: 'hidden', name: this.id, id: this.id });
        this.hidden.data('element', this);
        this.hidden.data('setValue', function(value){
            self.setValue.call(self,value);
        });

        var containerInputSize = self.containerSize;
        if(self.layout_options.hasOwnProperty('containerInputSize')){
            containerInputSize = self.layout_options.containerInputSize;
        }

        var div = CE('div').css({'width': containerInputSize, 'float': 'left'});
        this.label = CE('input','wdr').attr({type: 'text', id: this.id  + 'label', name: this.id + 'label', disabled: 'disabled'});
        this.label.css({'width': '100%', 'float': 'left'});
        if(!this.isSelectionVisible){
            this.label.hide();
        }
        container.append(div.append(this.label));
        container.append(this.hidden);

        if(self.genealogy && last){
            this.hidden.val(last.selected);
            var lastLabel = last.label + ': ' + last.input.val();
            this.label.val(lastLabel);
        }
        //Libera o primeiro elemento, sempre
        this.location_types[0].input.removeAttr('disabled');

        //Recalcula o tabindex
        $('input:visible').add('textarea:visible').each(function(k,v){
            $(v).attr('tabindex', k);
        });
    }

    main.prototype.clearGrid = function(type){
        $('#up_' + type.name).remove();
        clearTimeout(this.handleSearch);
    }

    main.prototype.clear = function(loc_type){
        var toClear = loc_type.selected;
        loc_type.selected = undefined;
        loc_type.focus = undefined;
        clearTimeout(this.handleSearch);
        this.ignoreSearchResult=true;
        loc_type.input.val('');
        this.clearGrid(loc_type);
        this.clearBelow(loc_type);

        if(loc_type.parent){
            this.hidden.val(loc_type.parent.selected);
            this.label.val(loc_type.parent.label + ': ' + loc_type.parent.input.val());
        }else{
            if(this.hidden.val()==toClear){
                this.label.val('');
                this.hidden.val('');
            }else{
                //Verifica se foi primeiro campo a receber o clear(), e reseta o hidden
                if(this.firstInput.val()==''){
                    this.label.val('');
                    this.hidden.val('');
                }
            }
        }
    }

    main.prototype.clearBelow = function(loc_type, clearFirstTyping){

        var below = false; //Flag para marcar se já está abaixo
        var self = this;
        clearTimeout(self.handleSearch);

        var leaveOpen = false;
        if(this.optionals.indexOf(loc_type.name)>=0){
            leaveOpen = true;
        }

        for(var x in this.location_types){
            var next_type = this.location_types[x];

            if(below){
                next_type.focus = undefined;
                next_type.selected = undefined;

                if(self.isDisable){
                    if(!leaveOpen){
                        next_type.input.attr('disabled','disabled');
                    }
                    leaveOpen = false;
                }

                if(clearFirstTyping){
                    next_type.input.removeAttr('first_typing');
                    next_type.input.removeAttr('first_typing_id');
                }
                next_type.input.val('');
                this.clearGrid(next_type);
            }

            if(loc_type.name == next_type.name){
                below = true;
            }
        }
    }

    main.prototype.installMove = function(loc_type){

        var self = this;
        var selfInput = loc_type.input;
        loc_type.input.keyup(function(event){

            if(loc_type.readOnly)
            {
                return;
            }

            //Del
            if(event.keyCode == 46){
                self.clear(loc_type);
                return;
            }

            //Backspace
            if(event.keyCode == 8){
                if(selfInput.context.value==''){
                    self.countBack=0;
                    self.clear(loc_type);
                    return;
                }

                if(!self.countBack){
                    self.countBack=0;
                }
                self.countBack++;
                if(self.countBack>2){
                    self.countBack=0;
                    self.clear(loc_type);
                    return;
                }
            }else{
                self.countBack=0;
            }

            var div = loc_type.up;
            if(div == undefined) return;
            var focus  = div.find('.highlight');
            if(focus.size() == 0){
                loc_type.focus = undefined;
                if(!self.genealogy || !self.genealogy['id']){
                    loc_type.selected = undefined;
                }
            }



            if(event.keyCode === 38){
                if(focus.length == 0){
                    focus = div.find('div').last();
                    focus.addClass('highlight');
                }else{
                    focus.removeClass('highlight');
                    focus = focus.prev();
                    focus.addClass('highlight');
                }

                loc_type.focus = focus;
                return;

            }

            if(event.keyCode === 40){
                if(focus.length == 0){
                    focus = div.find('div').first();
                    focus.addClass('highlight');
                }else{
                    focus.removeClass('highlight');
                    focus = focus.next();
                    focus.addClass('highlight');
                }

                loc_type.focus = focus;
                return;
            }

            //ENTER
            if(event.keyCode === 13){

                if(div.find('div').size() == 1){
                    loc_type.focus = div.find('div');
                }
                if(loc_type.focus != undefined) self.next(loc_type);

                if(self.dontPropagateEnter){
                    event.stopPropagation();
                    return false;
                }
                clearTimeout(self.handleSearch);
            }



        });
    }

    main.prototype.next = function(loc_type, onlyOpen){
        $(loc_type.input).attr('data-id', loc_type.focus.attr('location_id'));
        
        this.clearGrid(loc_type);
        loc_type.input.val(loc_type.focus.text());
        loc_type.selected = loc_type.focus.attr('location_id');
        this.hidden.val(loc_type.selected);
        console.log(loc_type.selected);
        this.label.val(loc_type.label + ': ' + loc_type.input.val());
        var last = null;

        for(var x in this.location_types){
            var next_type = this.location_types[x];
            if(last != null && loc_type.name == last.name){
                next_type.parent = last;

                if(this.optionals.indexOf(next_type.name) >= 0 ){
                    next_type.input.removeAttr('disabled');
                    continue;
                }else{
                    break;
                }
            }
            last = next_type;
        }


        next_type.input.removeAttr('disabled');
        if(!onlyOpen){
            next_type.input.focus();
        }


    }

    main.prototype.grid = function(loc_type, data){

        this.clearGrid(loc_type);
        if(this.ignoreSearchResult){
            this.ignoreSearchResult = false;
            return;
        }
        var self = this;

        var div = CE('div', 'up').css({'float': 'left', 'position': 'relative', 'width': '98%'});
        div.attr('id', 'up_' + loc_type.name);
        loc_type.input.after(div);

        $.each(data, function(k, v){
            var label = CE('div');
            label.text(v.external_id + ' - ' + v.name);
            label.attr('location_id', v.location_id)
            div.append(label);
            label.click(function(event){
                loc_type.focus = label;

                if(self.first_typing != undefined && self.first_typing != ''){
                    self.veritySecondEvent.call(self,event, loc_type.input, loc_type);
                }else{
                    self.next(loc_type);
                }
            });
        });
        div.show();
        loc_type.up = div;
    }

    main.prototype.turnSelectionLabelVisible = function(isSelectionVisible){
        this.isSelectionVisible = isSelectionVisible;
        if(isSelectionVisible){
            $('#' + this.id + 'label').hide();
        }else{
            $('#' + this.id + 'label').show();
        }
    }

    main.prototype.turnLabelVisible = function(isLabelVisible){
        this.isLabelVisible = isLabelVisible;
        if(this.lbl){
            if(isLabelVisible){
                this.lbl.hide();
            }else{
                this.lbl.show();
            }
        }
    }
    
    main.prototype.setDescribeBranchAsParish = function(value){
        this._isDescribeBranchAsParish = (value == 1);
    }
    
    main.prototype.setDescribeSectorAsDiocese = function(value){
        this._isDescribeSectorAsDiocese = (value == 1);
    }
    
    main.prototype.isDescribeBranchAsParish = function(){
        return this._isDescribeBranchAsParish;
    }
    
    main.prototype.isDescribeSectorAsDiocese = function(){
        return this._isDescribeSectorAsDiocese;
    }



}(locations));
