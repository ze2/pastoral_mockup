var entities = {};
;(function(entities){
        
    var CF = function(type, label, name, cls){
        var fs = CE('div', 'wdl');
        var lbl = CE('span', 'wdl lbl').text(label).css({'width':this.pxLbl});
        
        var element = $(document.createElement('input'));
        if(cls) element.addClass(cls);
        
        element.attr('id', name);
        element.attr('name', name);
        
        fs.append(lbl).append(element);
        fs.label = lbl;
        fs.element = element;
        
        if(type=='date'){
            element.mask("99/99/9999");
            element.getValue = function(){
                var date = this.val();
                if(date.trim()!=''){                    
                    return date.substr(6,4) + '-' + date.substr(3,2) + '-' + date.substr(0,2);
                }

                return date;
            }
        }else if(type=='birth'){
            element.on('blur', function(e) {
                var unformat = $(this).val();
                var pre = '0'; //112 == 01/12                
                var format = '';
                var day = '';
                var month = '';
                
                var isDay = true;
                
                var digit = '';
                for(var x = 0; x<unformat.length; x++){
                    digit = unformat.substr(x,1);
                    if(!isNaN(digit*1)){
                        pre = pre + digit;
                        if(isDay){
                            day = day + digit;
                        }else{
                            month = month + digit;
                        }
                    }else{
                        isDay = false;
                    }
                }
                
                if(month.length > 2){
                    month = month.substr(0,2);
                }
                
                if(day.length > 0 && month.length > 0){
                    format = padLeft(day, 2, '0') + "/" + padLeft(month, 2, '0');
                }else if(pre.length==3){
                    day = pre.substr(0,2) * 1;
                    month = pre.substr(2,1) * 1;
                }else if(pre.length==4){
                    day = pre.substr(0,2) * 1;
                    month = pre.substr(2,2) * 1;
                }else if(pre.length>4){
                    day = pre.substr(1,2) * 1;
                    month = pre.substr(3,2) * 1;
                }
               
                format = padLeft(day, 2, '0') + "/" + padLeft(month, 2, '0');
                var validate = new Date('2012-' + padLeft(month, 2, '0') + '-' + padLeft(day, 2, '0'));
                
                if(isNaN(validate.getDay())){
                    format = '';
                }
                
                $(this).val(format);
                
            });
            
            element.getValue = function(){
                var val = $(this).val();
                val = val.replace('__/__', '').replace('__','').replace('_', '');     
                return val.replace('/','-').replace('\\','-');     
            }
            
        }else{
            element.getValue = function(){
                return $(this).val();     
            }
        }
        
        return fs;
    };

    var main = function(id){
        this.id = id;
        this.isAllowInsertNew = true;
        this.isShowInputCode = true;
        this.isOpenInputName = false;

        this.isSelectOnly = false;
        this.isAppOnly = false;
        
        //Variavel especial para inpedir que o evento de fechamento normal da janela de busca chame a verificação da
        //segunda digitsação repetidas vezes.
        this.hasApplySearch = false;
        
        var depDiv = CE('div');
        depDiv.attr('id', 'search_' + this.id);
        
        this.pxLbl = '150px';
        this.pxBig = '550px';
        this.pxMin = '100px';
        this.isIgnoreSecondVerification = false;
        
        $('#container_' + this.id).after(depDiv);
    };
    
    entities.simple = main;
    
    main.prototype.setPrefilter = function(prefilter){
        this.prefilter = prefilter;
    }
    main.prototype.onChange = function(){
 
    };
    main.prototype.setValue = function(value, name){
        this.value = value;
        this.valueName = name;
        this.onChange();
        if(this.inputCode){
            this.inputCode.val(this.value);
            if(name){
                this.inputName.val(name);
            }
            
            if(value && (value+'').trim()==''){
                this.inputName.val('');
                value = '';
            }
            this.inputName.attr('lastValue', this.inputName.val());
        
            var postValue = this.inputCode.data('postValue');
            if(typeof postValue === 'function') postValue.call(this, this.value);
        }
        
        if(!this.isIgnoreSecondVerification){
            this.veritySecondEvent(value, name);
        }
        

    };
    
    main.prototype.setShowInputCode = function(isVisible){
        this.isShowInputCode = isVisible;
    };
    
    main.prototype.setOpenInputName = function(isOpen){
        this.isOpenInputName = isOpen;
    };
    
    main.prototype.setShowSearchWindowEver = function(isShow){
        this.isShowSearchWindowEver = isShow;
    };
    
    main.prototype.setSearchFields = function(value){
        this.searchFields = value;
    };
    
    main.prototype.setAllowInsertNew = function(value){
        this.isAllowInsertNew = value;
    };

    main.prototype.setIsSelectOnly = function(value){
        this.isSelectOnly = value;
    };

    main.prototype.setIsAppOnly = function(value){
        this.isAppOnly = value;
    };
    
    main.prototype.setRenderLocation = function(renderLocation){
        this.renderLocation = renderLocation;
        $(this).data('renderLocation',renderLocation);
    };
    main.prototype.setRenderEmail = function(renderEmail){
        this.renderEmail = renderEmail;
    };
    main.prototype.render = function(renderInsertDialog){ 
        this.container = $('#container_' + this.id);
        this.divSearch = $('#search_' + this.id);
        this.divSearch.hide();
        
        this.renderFrontComponents();
        this.renderSearchComponents();
        
        if(this.isAllowInsertNew){
            this.renderInsertDialog(renderInsertDialog);
        }
        
        var self = this;
        this.divSearch.bind('keydown', function(e){
            var keycode = e.keyCode || e.wich;
            if(keycode == 13){
                self.doSearch();
            }
        });
        
    };

    main.prototype.renderInputCode = function(){
        this.inputCode = CE('input', 'wdl').css({'width': '100px'});
        this.inputCode.attr('placeholder', i18n_idholder);
        this.inputCode.bind('keydown',filterOnlyNumbers);
        
        this.inputCode.attr('id', this.id);
        this.inputCode.attr('name', this.id);
        
        this.inputCode.val(this.value);
                
        var self = this;
        this.inputCode.data('setValue', function(value){
            self.setValue.call(self, value); 
            self.leaveInputCode.call(self);
        });
        
        this.inputCode.change(function(e){
            self.leaveInputCode.call(self, e); 
        });
        
        this.inputCode.on('blur', function(e){
            
            if($(this).val()==''){
                self.leaveInputCode.call(self, e); 
            }
        });
        
        this.inputCode.bind('keyup',function(e){
            //PRecionou o mais (+)
            if((e.which == 187 && e.shiftKey==true) || (e.which == 107 && e.shiftKey==false)){
                self.openSearch.call(self);
            }
            
        });
        
        this.container.append(this.inputCode); 
        if(!this.isShowInputCode){
            this.inputCode.hide();
        }
    };
    
    main.prototype.renderInputName = function(){
        this.inputName = CE('input', 'wdl').css({'width': '450px'});
        this.inputName.attr('id', this.id + '_name');
        this.inputName.attr('name', this.id + '_name');
        this.inputName.val(this.valueName);
        this.inputName.attr('lastValue', '');
        
        var self = this;
        if(!this.isOpenInputName){
            this.inputName.attr('disabled','disabled');
        }else{
            this.inputName.on('blur', function(e){self.leaveInputName.call(self, e)});
            this.inputName.attr('placeholder', i18n_dica);
        }
        this.container.append(this.inputName);        
    };
    
    main.prototype.leaveInputName = function(e){
        if(this.inputName.val().trim()==''){
            this.setValue('','');
        }else if(this.inputName.val().trim()=='+'){
            this.inputName.val('');
            this.openSearch();
        }else{            
            if(this.inputName.attr('lastValue').trim().toUpperCase() != this.inputName.val().trim().toUpperCase()){
                
                //Se o nome for identico ao informado na primeira digitação, assume a primeira digitação
                if(this.first_typing_name && this.inputName.val().trim().toUpperCase() == this.first_typing_name.trim().toUpperCase()){
                     this.setValue(this.first_typing_id, this.first_typing_name);
                     return;
                }                
                //Senão pesquisa pelo nome
                this.searchName.element.val(this.inputName.val());
                this.openSearch();
                this.doSearch();
                this.inputName.val('');
            }
        }
        
        this.inputName.attr('lastValue', this.inputName.val());
    };
    
    main.prototype.renderButtonOpenSearch = function(){
        this.buttonSearch = CE('img','wdl').css({'width': '20px'});
        this.buttonSearch.attr('src', '/images/icons/search.png');
        this.buttonSearch.val('...');
        var self = this;
        this.buttonSearch.on('click', function(e){self.openSearch.call(self)});
        
        this.container.append(this.buttonSearch);
    };
    
    main.prototype.renderFrontComponents = function(){
        this.renderInputCode();
        this.renderInputName();
        this.renderButtonOpenSearch();
    };
    
    main.prototype.renderSearchName = function(){
        this.searchName = CF('text', i18n_nome, this.id + '_name');
        this.searchName.element.css({'width': '100%'});
        
        var tdLbl = CE('td');
        tdLbl.append(this.searchName.label);
        var tdElement = CE('td');
        tdElement.attr('colspan',3);
        tdElement.append(this.searchName.element);
        
        this.layoutLine.append(tdLbl);
        this.layoutLine.append(tdElement);
        this.layoutFill=4;
        this.newLayoutLine();
        //this.divSearch.append(this.searchName);
    };
    
    main.prototype.renderSearchEmail = function() {

        this.searchEmail = CF('text', i18n_email, this.id + '_email');
        this.searchEmail.element.css({'width': '100%'});
        
        var tdLbl     = CE('td');
            tdLbl.append(this.searchEmail.label);
        
        var tdElement = CE('td');
            tdElement.attr('colspan',1);
            tdElement.append(this.searchEmail.element);
        
        this.layoutLine.append(tdLbl);
        this.layoutLine.append(tdElement);
        this.layoutFill += 4;
    };
    
    main.prototype.renderSearchPhone = function() {
        
        this.searchPhone = CF('text', i18n_telefone, this.id + '_phone');
        this.searchPhone.element.css({'width': '100%'});
        this.addPhoneMask();
        
        var tdLbl     = CE('td');
            tdLbl.append(this.searchPhone.label);
        
        var tdElement = CE('td');
            tdElement.attr('colspan',3);
            tdElement.append(this.searchPhone.element);
        
        this.layoutLine.append(tdLbl);
        this.layoutLine.append(tdElement);
        this.layoutFill += 4;
    };
    
    main.prototype.addPhoneMask = function() {
        
//        this.searchPhone.element.unmask();
//        this.searchPhone.element.mask("(99) 99999-999?9");
    };
    
    main.prototype.renderDivResult = function(){
        this.divResult = CE('div');
        this.divSearch.append(this.divResult);
    };
    
    main.prototype.renderButtonDoSearch = function(td){
                 
        this.buttonDoSearch = CE('input','wdl');
        this.buttonDoSearch.attr('type', 'button');        
        this.buttonDoSearch.val(i18n_pesquisar);
        var self = this;
        this.buttonDoSearch.on('click', function(e){self.doSearch.call(self)});
        
        
        td.attr('rowspan',100);
        td.append(CE('div').css({'clear': 'both'}));
        td.append(this.buttonDoSearch);
        if(this.isAllowInsertNew){
            this.renderButtonDoInsert();
            td.append(this.buttonDoInsert);
        }
        this.layoutLine.append(td);
        
        return td;
    };
    
    main.prototype.renderButtonDoInsert = function(){
        this.buttonDoInsert = CE('input','wdl');
        this.buttonDoInsert.attr('type', 'button');
        this.buttonDoInsert.attr('id', 'callNewEntityPopup');
        this.buttonDoInsert.val(i18n_cadastrar);
        var self = this;
        this.buttonDoInsert.on('click', function(e){self.openInsertDialog.call(self, e)});
    };
    
    main.prototype.renderSearchCity = function(renderState){
        //Se já foi renderizado, ignora.
        if(this.searchCity){
            return;
        }
        this.searchCity = true;
        
        var self = this;
        var id   = self.id + 'LocationCity';
        var name = self.id + 'LocationCity';

        var locationContainer = CE('div');
        locationContainer.attr('id', 'container_' + id);
        
        var tdLbl = CE('td');
        var tdLblUf = CE('td');
        var tdElement = CE('td');
        var tdElementUf = CE('td');
        if(renderState){
            
            tdLblUf.text(i18n_estado + ':');
            tdLbl.text(i18n_cidade + ':');
            this.layoutLine.append(tdLblUf);
            this.layoutLine.append(tdElementUf);
            this.layoutLine.append(tdLbl);
            this.layoutLine.append(tdElement);
            this.layoutFill+=4;
        }else{            
            tdLbl.text(i18n_cidade + ':');
            this.layoutLine.append(tdLbl);
            this.layoutLine.append(tdElement);
            this.layoutFill+=2;
        }
        this.newLayoutLine();
        
        require('/js/element/Location.js' + cacheLocationJs, function(){
            var id   = self.id + 'LocationCity';
            var name = self.id + 'LocationCity';
            
            var row_location = {};
            var genealogy = {};

            var location_types=[];
            
            if(!renderState){
                var location_types = [{"label":i18n_cidade,"name":"city"}];
            }else{
                var location_types = [{"label":i18n_estado,"name":"state"}, {"label":i18n_cidade,"name":"city"}];
            }

            var element_LocationCity = new locations.simple(id);
            element_LocationCity.setContainerSize('');
            element_LocationCity.setInputSize('250px');
            element_LocationCity.setLocation(row_location);
            element_LocationCity.setLocationTypes(location_types);
            element_LocationCity.setValue('');
            element_LocationCity.setGenealogy(location_types);
            element_LocationCity.turnSelectionLabelVisible(false);
            element_LocationCity.turnLabelVisible(false);
            element_LocationCity.render();
            
            self.searchCity = element_LocationCity;
            tdElement.append(element_LocationCity.elements.city.input);
            if(renderState){
                tdElementUf.append(element_LocationCity.elements.state.input);
            }
            
            //self.divSearch.append(self.searchCity);
        });
    };
    
    main.prototype.renderSearchTheme = function(){
        var self = this;
        var id   = self.id + 'TypeTheme';
        var name = self.id + 'TypeTheme';

        var container = CE('div');
        container.attr('id', id + '_container');
        
        var tdLblType = CE('td');
        tdLblType.text(i18n_tipo);
        var tdTypeElement = CE('td');        
        tdTypeElement.append(container);
        
        var tdLblTheme = CE('td');
        tdLblTheme.text(i18n_tema);
        var tdThemeElement = CE('td');        
        
        this.layoutLine.append(tdLblType);
        this.layoutLine.append(tdTypeElement);
        this.layoutLine.append(tdLblTheme);
        this.layoutLine.append(tdThemeElement);
        this.layoutFill+=4;
        this.newLayoutLine();
        
        require('/capacity/js/elements/theme.js', function(module, status){
            var element = new module(id);
            element.setContainer($('#' + id + '_container'));
            element.setLayoutManual();
            element.render();
            
            tdTypeElement.append(element.inputType);
            tdThemeElement.append(element.inputTheme);
            self.themeElement = element;
        });
        
    };
    
    main.prototype.renderSearchBirth = function(){
        this.searchBirth = CF('birth', i18n_nascimento, this.id + '_birth', 'wdl').css({'width': '70px'});
        this.searchBirth.element.attr('maxlength', '5'); 
        this.searchBirth.element.mask("99/99"); 
        var tdLbl = CE('td');
        tdLbl.append(this.searchBirth.label);
        var tdElement = CE('td');        
        tdElement.append(this.searchBirth.element);
        
        this.layoutLine.append(tdLbl);
        this.layoutLine.append(tdElement);
        this.layoutFill+=2;
        this.newLayoutLine();
        
        //this.divSearch.append(this.searchBirth);
        
    };
    
    main.prototype.renderLayoutTable = function(){
        this.layoutTable = CE('table');
        this.layoutTable.css({'width': '100%'});
        this.newLayoutLine();
    };
    
    main.prototype.newLayoutLine = function(){
        if(this.layoutFill >=4){
            this.layoutLine = CE('tr');
            this.layoutTable.append(this.layoutLine);
            this.layoutFill = 0;
        }
    }
    
    main.prototype.renderAdvancedOptions = function(td){
        td.append(CE('div').css({'clear': 'both'}));
        this.renderAppOnly(td);
        td.append(CE('div').css({'clear': 'both'}));
        this.renderSearchNoCity(td);
        td.append(CE('div').css({'clear': 'both'}));
        this.renderSearchNoBirth(td);
        td.append(CE('div').css({'clear': 'both'}));
        this.renderSearchSimilarName(td);
    }
    
    main.prototype.renderSearchSimilarName = function(td){
        this.chkSimilarName = CE('input', 'wdl');
        this.chkSimilarName.attr('type','checkbox');
        this.chkSimilarName.attr('name',this.id + 'SimilarName');
        this.chkSimilarName.attr('id',this.id + 'SimilarName');
        this.chkSimilarName.attr('type','checkbox');
        this.chkSimilarName.attr('checked','checked');
        this.lblSimilarName = CE('label', 'wdl');
        this.lblSimilarName.text(i18n_nome_semelhante);
        this.lblSimilarName.attr('for', this.id + 'SimilarName');
        td.append(this.chkSimilarName);
        td.append(this.lblSimilarName);
    }
    
    main.prototype.renderSearchNoBirth = function(td){
        this.chkNoBirth = CE('input', 'wdl');
        this.chkNoBirth.attr('type','checkbox');
        this.chkNoBirth.attr('name',this.id + 'NoBirth');
        this.chkNoBirth.attr('id',this.id + 'NoBirth');
        this.chkNoBirth.attr('type','checkbox');
        this.chkNoBirth.attr('checked','checked');
        this.lblNoBirth = CE('label', 'wdl');
        this.lblNoBirth.text(i18n_sem_aniversario);
        this.lblNoBirth.attr('for', this.id + 'NoBirth');
        td.append(this.chkNoBirth);
        td.append(this.lblNoBirth);
    }
    
    main.prototype.renderSearchNoCity = function(td){
        this.chkNoCity = CE('input', 'wdl');
        this.chkNoCity.attr('type','checkbox');
        this.chkNoCity.attr('name',this.id + 'noCity');
        this.chkNoCity.attr('id',this.id + 'noCity');
        this.chkNoCity.attr('type','checkbox');
        this.chkNoCity.attr('checked','checked');
        this.lblNoCity = CE('label', 'wdl');
        this.lblNoCity.text(i18n_sem_municipio);
        this.lblNoCity.attr('for', this.id + 'noCity');
        td.append(this.chkNoCity);
        td.append(this.lblNoCity);
    }

    main.prototype.renderAppOnly = function(td){
        this.chkAppOnly = CE('input', 'wdl');
        this.chkAppOnly.attr('type','checkbox');
        this.chkAppOnly.attr('name',this.id + 'appOnly');
        this.chkAppOnly.attr('id',this.id + 'appOnly');
        this.chkAppOnly.attr('type','checkbox');
        
        if(this.isAppOnly){
            this.chkAppOnly.attr('checked','checked');
        }
        
        this.lblAppOnly = CE('label', 'wdl');
        this.lblAppOnly.text(i18n_app_only);
        this.lblAppOnly.attr('for', this.id + 'appOnly');
        td.append(this.chkAppOnly);
        td.append(this.lblAppOnly);
    }


    
    main.prototype.renderSearchComponents = function() {
        
        this.layoutFill = 4;
        this.renderLayoutTable();
        this.renderSearchName();
        this.renderSearchEmail();
        this.renderSearchPhone();
        
        var td = CE('td','wdr');
        this.renderAdvancedOptions(td);
        this.renderButtonDoSearch(td);
        
        this.newLayoutLine();
        
        for(i in this.searchFields){
            switch(this.searchFields[i]){
                case 'theme': this.renderSearchTheme(); break;
                case 'state': this.renderSearchCity(true); break; 
                case 'city': this.renderSearchCity(); break; 
                case 'birth': this.renderSearchBirth(); break;
                
            }            
        }
        
        this.divSearch.append(this.layoutTable);
        
        var self = this;
        
        $(this.divSearch).dialog({
            autoOpen: false,
            beforeClose: function( event, ui ){
                this.hasApplySearch = false;
            }
        });
        
        this.renderDivResult();
    };
    
    main.prototype.openSearch = function(){
        this.isIgnoreSecondVerification = true;
        this.divSearch.dialog({autoOpen: true, title: i18n_localizarPessoa, width: '1000', height: '600', resizable: false}).dialog('open');
        
    }
    
    main.prototype.doSearch = function() {
        var obj = {};
        
        for(var i in this.searchFields) {
            
            switch(this.searchFields[i]) {
                
                case 'name'         : obj.search = this.searchName.element.val(); break;
                case 'email'        : obj.email  = this.searchEmail.element.val(); break;
                case 'phone'        : obj.phone = this.searchPhone.element.val(); break;
                case 'state'        :  
                case 'city'         : 
                        if(obj.city){
                            break;
                        }
                    obj.state = this.searchCity.getValue('state'); 
                    obj.city = this.searchCity.getValue('city'); 
                    break; 
                case 'birth'        : obj.birth  = this.searchBirth.element.getValue(); break;                    
                case 'theme'        : obj.theme  = this.themeElement.getValue(); break;    
            }            
            
            obj.noCity      = this.chkNoCity.is(':checked');
            obj.noBirth     = this.chkNoBirth.is(':checked');
            obj.similarName = this.chkSimilarName.is(':checked');
            obj.social      = this.social;
            obj.isAppOnly   = this.chkAppOnly.is(':checked');
            
        }
        obj.isShowInputCode = this.isShowInputCode;
        
        obj.prefilter = this.prefilter;
        obj = [obj];
        
        var uri = '/rest/index/model/entitynew.get_people?params='; console.log(obj)
            uri += JSON.stringify(obj);
        
        var self = this;
        
        $('#carregando').show();
        $.getJSON(uri, function(data){
            self.grid(data);
            $('#carregando').hide();
        }).fail(function(err){
            console.log( "error: ", err );
        });
    };
    
    main.prototype.getHead = function(){

        var thead = CE('thead');
        var tr    = CE('tr');
        thead.append(tr);

        for(x in this.cols){
            var td = CE('th');
            td.text(this.cols[x]);
            tr.append(td);
        }

        var actions = CE('th');
        actions.text(i18n_acoes);
        actions.css('width', '100px');
        tr.append(actions);
        
        return thead;
    };
    
    main.prototype.grid = function(data)
    {
        var table = CE('table');
        table.attr('id', 'result'+this.id);
        table.attr('class', 'tableList'); 
        table.append(this.getHead());

        var tbody = CE('tbody');
        var cols  = this.cols;
        var self  = this;

        if(data.length==1){
            $.each(data, function(r, row){
                this.hasApplySearch = true;
                self.isIgnoreSecondVerification = false;
                self.setValue(row['id'], row['name']);        
                self.divSearch.dialog('close');
                if(self.postApply){
                    self.postApply.call(self, row);
                }
                //Retorna o foco para o componente
                self.reassignFocus();
            });
            //return;
        }
        
        if(data.length==0){
            var tr      = CE('tr');
            var td   = CE('td');
            td.html(i18n_dicas_busca);
            td.attr('colspan',100);
            tr.append(td);
            tbody.append(tr);
        }

        $.each(data, function(r, row){
        
            var tr      = CE('tr');
            var key     = row['id'];
            var labelSelect = row['name'];

            $.each(cols, function(name, label){
                
                var td   = CE('td');
                if(row[name]!='' && row[name]!=null){
                    td.text(row[name]);                   
                }else{
                    td.html('&nbsp;');
                }
                tr.append(td);
            });

            var actions = CE('td');
            tr.append(actions);

            var show = CE('img');
            
            show.attr('id', 'slc'+key);
            show.attr('src', '/images/icons/info.png').addClass('imgLinkSmall');            
            show.click(function(e){
                window.open("/entitynew/entity/show/id/" + row['id']);
            });

            var edit = CE('img');
            
            edit.attr('id', 'slc'+key);
            edit.attr('src', '/images/icons/edit.png').addClass('imgLinkSmall');            
            edit.click(function(e){
                window.open("/entitynew/entity/edit/id/" + row['id']);
            });

            var deleted = CE('img');
            
            deleted.attr('id', 'slc'+key);
            deleted.attr('src', '/images/icons/cancel.png').addClass('imgLinkSmall');            
            deleted.click(function(e){
                window.open("/entitynew/entity/delete/id/" + row['id']);
            });

            var select = CE('img');
            
            select.attr('id', 'slc'+key);
            select.attr('src', '/images/icons/apply.png').addClass('imgLinkSmall');            
            select.click(function(e){
                self.clickOnApply.call(self, e, key, labelSelect); 
                if(self.postApply){
                    self.postApply.call(self, row);
                }
            });

//            rpc('user.get_user', function(user){ 
//                /*your code here*/ 
//                if(user === 2716){
//                    actions.append(show);
//                }
//            });

            actions.append(show);
            
            if(!self.isSelectOnly){
                actions.append(edit);
                actions.append(deleted);
            }

            actions.append(select);

            tbody.append(tr);
        });
        table.append(tbody);

        //this.divResult = CE('div').append(table);
        //this.divSearch.append(this.divResult);
        
        $('#result'+this.id).remove();
        this.divResult.append(table);
        
    }
    
    main.prototype.clickOnApply = function(e, key, labelSelect){
        this.hasApplySearch = true;
        this.isIgnoreSecondVerification = false;
        this.setValue(key, labelSelect);    
        
        this.divSearch.dialog('close');  
        //Retorna o foco para o componente
        this.reassignFocus();
    }
    
    main.prototype.getEntityById = function(id){
        var obj = {};
        
        obj.id = id;        
        obj = [obj];
        
        var uri = '/rest/index/model/entitynew.get_people_by_id?params=';
        uri += JSON.stringify(obj);
        
        var self = this;
        
        $('#carregando').show();
        $.getJSON(uri, function(data){
            $('#carregando').hide();
            
            if(self.isShowSearchWindowEver){
                self.clear();
                self.grid(data);
                self.openSearch();
                return;
            }
            
            if(data.length==0){
                self.clear();
            }
            $.each(data, function(r, row){
                var key     = row['id'];
                var labelSelect = row['name'];
                self.setValue(key, labelSelect);                
            });
        }).fail(function(err){
            console.log( "error: ", err );
        });
    }
    
    main.prototype.clear = function(){
        this.setValue('','');
    }
    
    main.prototype.leaveInputCode = function(e){
        if(this.inputCode.val()!=''){
            this.getEntityById(this.inputCode.val());                
        }else{
            this.clear();
        }
    }
    
    main.prototype.setFirstTyping = function(id, name){
        this.first_typing_id = id;
        this.first_typing_name = name;
    }
    
    main.prototype.veritySecondEvent = function(id, name){
        if(this.first_typing_id==undefined){
            return;
        }
        var firstType = this.first_typing_id + " - " + this.first_typing_name;
        var secondType;
        if(this.inputCode){
            if(this.inputCode.val().trim()==''){
                secondType = this.inputCode.val() + " - " + '';
            }else{
                secondType = this.inputCode.val() + " - " + this.inputName.val();
            }
        }else{
            return;
        }
        
        if(firstType==undefined ){
            return;
        }
        
        if(firstType != secondType){
            var response = question(firstType, secondType, '', function(){});
            
            if(response == firstType){
                this.inputCode.val(this.first_typing_id);
                this.inputName.val(this.first_typing_name);
            }
        }
    }
    
    main.prototype.renderInsertDialog = function(){
        
        this.divInsert = CE('div', 'box');
        this.divInsert.attr('id', 'insert_' + this.id);
        $('#container_' + this.id).after(this.divInsert);
        this.divInsert.hide();
        this.divInsert.enterToTab();
        
        var self = this;
        
        this.renderInsertFields();
    }
    
    main.prototype.renderInsertFields = function(){
        this.divInsert.append(this.renderInsertName());
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        this.divInsert.append(this.renderInsertNick());
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        this.renderInsertGender();
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        this.renderInsertTitle();
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        this.divInsert.append(this.renderInsertBirth());
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        if(this.renderEmail !== false){
            this.divInsert.append(this.renderInsertEmail());
        }
        
        if(this.renderLocation !== false){
            this.renderInsertCommunity();
            this.renderInsertCity();
        }
        this.divInsert.append(CE('div').css({'clear': 'both'}))

        this.divInsert.append(this.renderInsertDdd());
        
        this.divInsert.append(this.renderInsertPhone());
        this.divInsert.append(CE('div').css({'clear': 'both'}))
        this.divInsert.append(this.renderDoInsertButton());
        
        this.divInsert.clear = function(){
            $(this).find("input").not("input[type=button]").val('');
            
        }
        
    }
    
    main.prototype.renderInsertName = function(){
        this.insertName = CF('text', i18n_nome, this.id + '_insert_name');
        this.insertName.element.css({'width': this.pxBig});
        this.insertName.label.css({'width': this.pxLbl})
        
        if(system_id==1){
            this.insertName.element.attr('maxLength', 40); 
        }else{
            this.insertName.element.attr('maxLength', 100); 
        }
        
        return this.insertName;
    }

    main.prototype.isChangingTitle = false;

    main.prototype.renderInsertGender = function(){
        var span        = CE('span', 'element_textselect');
        var lbl         = CE('span', 'wdl').text(i18n_sexo).css({'width':this.pxLbl});
        var inputName   = CE('input');
        inputName.attr('name', this.id + '_insert_gender_name');
        inputName.attr('id', this.id + '_insert_gender_name');
        inputName.attr('autocomplete', 'off');
        
        var inputHidden = CE('input');
        inputHidden.attr('name', this.id + '_insert_gender');
        inputHidden.attr('id', this.id + '_insert_gender');
        inputHidden.attr('type', 'hidden');
        
        
        span.append(lbl);
        span.append(inputName);
        span.append(inputHidden);
        
        this.divInsert.append(span);
        
        var self = this;
               
        var element = new widgets.elements.TextSelect(this.id + '_insert_gender');
        element.setParentFocusOut(false);
        element.getOptionsByCategory('sexo', function(){
            element.init();
        });
        element.onChange = function(old){

            if(self.isChangingTitle === true){
                self.isChangingTitle = false;
            }else{
                $('#' + self.id + '_insert_title_name').val('');
                $('#' + self.id + '_insert_title').val('');
            }
        };
        
        return span;
    };
    
    main.prototype.renderInsertTitle = function(){
        var span = CE('span', 'element_textselect');
        var lbl = CE('span', 'wdl').text(i18n_titulo).css({'width':this.pxLbl});;
        var inputName = CE('input');
        inputName.attr('name', this.id + '_insert_title_name');
        inputName.attr('id', this.id + '_insert_title_name');
        inputName.attr('autocomplete', 'off');
        
        var inputHidden = CE('input');
        inputHidden.attr('name', this.id + '_insert_title');
        inputHidden.attr('id', this.id + '_insert_title');
        inputHidden.attr('type', 'hidden');
        span.append(lbl);
        span.append(inputName);
        span.append(inputHidden);
        
        this.divInsert.append(span);
        
        var element = new widgets.elements.TextSelect(this.id + '_insert_title');
        element.setFilterBy(this.id + '_insert_gender');
        element.getOptionsByCategory('titulo_pessoa', function(){
            element.init();
        });
        var self = this;
        element.onChange = function(old){
            self.isChangingTitle = true;
        };
        
        
        return span;
    };
    
    
    main.prototype.renderInsertBirth = function(){
        var element = CF('date', i18n_nascimento, this.id + 'insert_birth', 'wdl');
        return element;
    };
    main.prototype.renderInsertEmail = function(){
        var element = CF('email', i18n_email, this.id + 'insert_email', 'wdl');
        return element;
    };
    main.prototype.renderInsertNick = function(){
        var element = CF('text', i18n_apelido, this.id + 'insert_nick', 'wdl');
        if(system_id==1){
            element.element.attr('maxLength', 20); 
        }else{
            element.element.attr('maxLength', 250);
        }
        return element;
    };
    main.prototype.renderInsertCommunity = function(){
        var self = this;
        var id   = self.id + 'insert_LocationCommunity';
        var name = self.id + 'insert_LocationCommunity';

        var locationContainer = CE('div');
        locationContainer.attr('id', 'container_' + id);
             
        self.divInsert.append(locationContainer);
        
        require('/js/element/Location.js'+ cacheLocationJs, function(){
            var id   = self.id + 'insert_LocationCommunity';
            
            var row_location = {};
            var genealogy = {};

            var location_types = [{"label":i18n_setor,"name":"sector"}, {"label":i18n_ramo,"name":"branch"}, {"label":i18n_comunidade,"name":"community"}];

            var element_LocationCommunity = new locations.simple(id);
            element_LocationCommunity.setContainerSize('');            
            element_LocationCommunity.setInputSize(self.pxBig);
            element_LocationCommunity.setLabelSize(self.pxLbl);
            
            element_LocationCommunity.setLocation(row_location);
            element_LocationCommunity.setLocationTypes(location_types);
            element_LocationCommunity.setValue('');
            element_LocationCommunity.setGenealogy(genealogy);
            element_LocationCommunity.turnSelectionLabelVisible(false);
            element_LocationCommunity.turnLabelVisible(true);
            element_LocationCommunity.render();
            
            self.divInsert.append(CE('div').css({'clear': 'both'}))
            self.divInsert.append(element_LocationCommunity);   
        });
    }
    main.prototype.renderInsertCity = function(){
        var self = this;
        var id   = self.id + 'insert_LocationCity';
        var name = self.id + 'insert_LocationCity';

        var locationContainer = CE('div');
        locationContainer.attr('id', 'container_' + id);
             
        self.divInsert.append(locationContainer);
        
        require('/js/element/Location.js'+ cacheLocationJs, function(){
            var id   = self.id + 'insert_LocationCity';
            
            var row_location = {};
            var genealogy = {};

            var location_types = [{"label":i18n_estado,"name":"state"}, {"label":i18n_cidade,"name":"city"}];

            var element_LocationCity = new locations.simple(id);
            element_LocationCity.setContainerSize('');
            element_LocationCity.setInputSize(self.pxBig);
            element_LocationCity.setLabelSize(self.pxLbl);
            element_LocationCity.setLocation(row_location);
            element_LocationCity.setLocationTypes(location_types);
            element_LocationCity.setValue('');
            element_LocationCity.setGenealogy(genealogy);
            element_LocationCity.turnSelectionLabelVisible(false);
            element_LocationCity.turnLabelVisible(true);
            element_LocationCity.render();
            
            self.divInsert.append(CE('div').css({'clear': 'both'}))
            self.divInsert.append(element_LocationCity);
            
           
            self.ajustInsertLayout();
           
            //self.divSearch.append(self.searchCity);
        });
    }
    
    //TODO: DDD e DDI deveriam ser gerenciados por um componente capaz de tratar particularidades internacionais.
    main.prototype.renderInsertDdd = function(){
        var element = CF('number', i18n_ddd, this.id + 'insert_ddd', 'wdl');
        if(system_id==1){
            element.element.attr('maxLength', 2); 
        }else{
            element.element.attr('maxLength', 4); 
        }
        element.element.bind('keydown',filterOnlyNumbers);
        return element;
    }
    main.prototype.renderInsertPhone = function(){
        var element = CF('number', i18n_telefone, this.id + 'insert_phone', 'wdl');
        if(system_id==1){
            element.element.attr('maxLength', 9); 
        }else{
            element.element.attr('maxLength', 20); 
        }
        element.element.bind('keydown',filterOnlyNumbers);
        return element;
    }
    
    main.prototype.openInsertDialog = function(){
        this.divSearch.dialog('close');
        this.divInsert.clear();
        this.divInsert.dialog({title: i18n_cadastrar, width: '1024px'});
    }
    
    main.prototype.sendDate = function(date){
        if(date.trim()!=''){                    
            return date.substr(6,4) + '-' + date.substr(3,2) + '-' + date.substr(0,2);
        }
        return date;
    }
    
    main.prototype.renderDoInsertButton = function()
    {
        var btn = CE('input');
        btn.attr('type', 'button');
        btn.val(i18n_salvar);
        var self = this;
        
        btn.click(function()
        {
            var obj = {
                "name": $('#' + self.id + '_insert_name').val(),
                "gender_id": $('#' + self.id + '_insert_gender').val(),
                "title_id": $('#' + self.id + '_insert_title').val(),
                "birth": self.sendDate($('#' + self.id + 'insert_birth').val()),
                "nick_name":$('#' + self.id + 'insert_nick').val(),
                "community_id":$('#' + self.id + 'insert_LocationCommunity').val(),
                "city_id": $('#' + self.id + 'insert_LocationCity').val(),
                "ddd": $('#' + self.id + 'insert_ddd').val(),
                "phone":$('#' + self.id + 'insert_phone').val(),
                "email":$("#" + self.id + 'insert_email').val()
            };
        
            obj = [obj];
            var uri = '/rest/index/model/entitynew.insert_people?params=';
            uri += JSON.stringify(obj);

            $('#carregando').show();
            $.getJSON(uri, function(data){
                $.each(data, function(r, row){
                    
                    if(row['error'] != undefined && row['error'] != ''){
                        alert(row['error']);
                        return;
                    }
                    
                    if(r=='error'){
                        alert(row);
                        return;
                    }
                    
                    if(row['id']==undefined){
                        alert(i18n_impossivel_cadastrar);
                        return;
                    }
                    
                    var key     = row['id'];
                    var labelSelect = row['name'];
                    self.setValue(key, labelSelect);
                    if(self.postApply){
                        self.postApply.call(self, row);
                    }
                    self.divInsert.dialog('close');
                });
                $('#carregando').hide();
            }).fail(function(err){
                console.log( "error: ", err );
            });
        });
        
        return btn;
    }
    
    main.prototype.ajustInsertLayout = function()
    {
        
        var maxLen = 0;
        
        $.each(this.divInsert.find('.lbl'), function(r, row){
            if(maxLen<$(row).width()){
                maxLen = $(row).width();
            }
        });
        
        e = this.divInsert;
        
        $.each(this.divInsert.find('.lbl'), function(r, row){
            $(row).width(maxLen);            
        });
        
        
        
    }
    
    main.prototype.reassignFocus = function(){
        if(this.isShowInputCode){
            this.inputCode.focus();
        }else{
            this.inputName.focus();
        }
    }
    
}(entities));
