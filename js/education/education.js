var refreshSelectByMaterial = function(material, selectName, dataName, idName)
{
    var select = $('#' + selectName);
    select.find('option').remove();
    
    select.append($('<option>', {
            value: null,
            text: '---'
        }));
    
    for(id in jsonData[dataName]){
        var item = jsonData[dataName][id];
        if(item['material_id'] != material){
            continue;
        }
        
        select.append($('<option>', {
            value: item[idName],
            text: item['text']
        }));
    }
}

var refreshSelectByMaterialAndForm = function(material, selectName, dataName, idName)
{
    var form = $('#form_type_id');
    var form_type_id = form.val();
    var select = $('#' + selectName);
    select.find('option').remove();
    
    select.append($('<option>', {
            value: null,
            text: '---'
        }));
    
    for(id in jsonData[dataName]){
        var item = jsonData[dataName][id];
        if(material && item['material_id'] != material){
            continue;
        }
        if(form_type_id && item['form_type_id'] != form_type_id){
            continue;
        }
        
        select.append($('<option>', {
            value: item[idName],
            text: item['text']
        }));
    }
}

var refreshByMaterial = function(material)
{
    
    refreshSelectByMaterial(material, 'topic_id', 'topics', 'id');
    refreshSelectByMaterial(material, 'form_type_id', 'form_types', 'form_type_id');
    refreshSelectByMaterialAndForm(material, 'fields_form_types_id', 'fields', 'id');
}

var material_change = function()
{
    var material = $('#material_id').val();
    refreshByMaterial(material);    
}

var form_type_change = function()
{
    var material = $('#material_id').val();
    refreshSelectByMaterialAndForm(material, 'fields_form_types_id', 'fields', 'id');
}

var start_education = function()
{
    $('#material_id').on('change', material_change);
    $('#form_type_id').on('change', form_type_change);
}

$(function(){
    start_education();
    var formType = $('#form_type_id').val();
    if(!formType){
        material_change();
    }
    
});
