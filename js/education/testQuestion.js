    
    var auxiliar_range = {
        
        min_value: -280,
        max_value: 2192,
        
        render: function(target) {

            self = this;
            let sub_label = $("#sub_label_age");

            var div = $(document.createElement('div'));
                div.addClass('coolForm');

                label_text = this.get_tag_by_range(target.val());
                sub_label.text(label_text);

            var range = $(document.createElement('input'));
                range.attr("type", 'range');
                range.addClass("coolRange");
                range.attr('min', this.min_value);
                range.attr('max', this.max_value);

                range.on("change", function(){

                    let text = self.get_tag_by_range($(this).val());
                    sub_label.text(text);
                    target.val($(this).val());
                });

                target.on("change", function(){

                    let text = self.get_tag_by_range($(this).val());
                    sub_label.text(text);
                    range.val($(this).val());
                });

                div.append(range);

                target.after(div);
        },

        get_tag_by_range : function(val) {

            let num = val * 1;
            let months = parseInt(num / 30);
            let weeks  = parseInt(num / 7);
            let years  = parseInt(months / 12);
            let entity = (years < 3) ? 'Bebê ' : 'Criança ';

            if(years > 0) {

                months = parseInt(months % 12);
            }

            let months_word = (months > 1)? 'meses' : 'mês';
            let years_word  = (years > 1) ? 'anos'  : 'ano';

            if(num < 0) {

                let gest = 40 + weeks;
                return gest + ' semanas de gestação';

            } else if(num === 0) {

                return 'Recém Nascido';

            } else if(num > 0) {

                let year_text = (years > 0)? years + ' ' + years_word : '';
                let artigo = (years > 0  && months > 0) ? ' e ' : '';
                let month_text = (months > 0)? months + ' ' + months_word: '';
                return entity +' com ' + year_text + artigo + month_text;

            }
        }
    };


    var collapse_open_all = function() {
        
        loadingStart();
        
        $("#topics_list .collapse").removeClass('max').addClass('min');
        
        $(".question_content").slideDown(function() {
            
            loadingStop();
        });
    };
    
    var collapse_close_all = function() {
        
        loadingStart();
        
        $("#topics_list .collapse").addClass('max').removeClass('min');
        
        $(".question_content").slideUp(function(){
            
            loadingStop();
        });
    };
    
    var update_warning = function() {
    
        var val_days     = $("#val_days");
        var days_tag     = $("#days_tag");
        var val_material = $("#val_material");
        
        val_days.html($('#age').val());
        days_tag.html(auxiliar_range.get_tag_by_range($('#age').val()));
        val_material.html($("#material option:selected").html());
    };
    
    $("body").ready(function() {
        
        auxiliar_range.render($("#age"));
        
        update_warning();
        
        $("#collapse_open_all").on("click", function(){
            
            collapse_open_all();
        });
        
        $("#collapse_close_all").on("click", function(){
            
            collapse_close_all();
        });
        
        $("#topics_list .collapse").on("click", function() {
            
            if($(this).hasClass('min') || $(this).hasClass('max')) {
                
                let target = $(this).data('target');
                let box    = $("#" + target);

                box.slideToggle();

                if($(this).hasClass('min')) {

                    $(this).removeClass('min').addClass('max');
                } else {

                    $(this).removeClass('max').addClass('min');
                }
            }
        });
        
        $("#age").on('keyup', function() {
            
            if ($(this).val() !== '' && $(this).val() !== '-') {
    
                var value = parseInt($(this).val());
                $(this).val(value);
            }
        });
    });