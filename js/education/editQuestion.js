
var validations = {
    
    name: 'validations',
    form_type: 'object',
    list: {
        validator: {
                    'type'   : 'select',
                    'options': [{'text': 'Data igual ou menor','value':'DateAtOrBelow'},
                                {'text': 'Data igual ou maior','value':'DateAtOrOver'},
                                {'text': 'Não vazio',          'value':'NotEmpty'}],
                    'default': '',
                    'return' : 'string'
                    },
        params   : {
                    'type'   : 'text',
                    'default': '',
                    'return' : 'jsonArray'
                    }
    }
};
var subquestions = {

    name: 'subquestions',
    form_type: 'single_value',
    'type': 'text',
    'placeholder': '1,2,3',
    'return': 'jsonArray'
};

var artifactsJson = {
    
    name: 'artifacts',
    list: {
        id: {
                'type'   : 'number',
                'default': 5,
                'return' : 'number'
                },
        config   : {
                'type'   : 'text',
                'default': 5,
                'return' : 'json'
                }
        }
};


$("body").ready(function(){
    
    inputJSON.render($("#field_params"), [validations, subquestions]);
    //inputJSON.render($("#message_parameters"), [artifactsJson]);
    $("#kid_age_ini, #kid_age_fim").on('keyup', function() {
        
        let value = $(this).val();
        
        if (value !== '' && value !== '-') {

            var new_value = parseInt(value);
            
            if(isNaN(new_value)) {
                
                $(this).val('');
            } else {
                
                $(this).val(new_value);
            }
        }
    });
    
    $("#message_id, #if_yes, #if_no, #if_not_known, #synthesize_form_type_id, #post_processor_message_id").on('keyup', function() {
            
        let value = $(this).val();
        
        if (value !== '') {

            var new_value = parseInt(value);
            
            if(isNaN(new_value)) {
                
                $(this).val('');
            } else {
                
                $(this).val(new_value);
            }
        }
    });
});