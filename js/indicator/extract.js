
var baseUrl = window.location.protocol + '//' + window.location.hostname;

var removeNotStandartGet = function(url) {
  
    var url = url.replace("?", "/");

    var url_split = url.split('&');
    
    if(url_split.length > 0) {
        
        var new_url = '';
        
        $.each(url_split, function(key, value) {
                       
            var url_split2 = value.split('=');
            
            var bar = (key > 0)? '/': '';
            
            new_url += bar + url_split2[0];
            
            if(url_split2.length > 0 && url_split2[1] != undefined) {
                
                
                new_url += '/' + url_split2[1];
            }
        });
        
        return new_url;
    } else {
        
        return url;
    }
};


var removeUriGet = function(var_name, url) {
  
    var url = (url === undefined)? window.location.href : url;

    url = removeNotStandartGet(url);

    var url_split = url.split('/' + var_name + '/');
    
    if(url_split.length > 0) {
        
        var newUrl = url_split[0];
        
        var url_second_part = ((url_split[1]) !== undefined)? url_split[1] : '';
        
        var second_part_split = url_second_part.split('/');

        if(second_part_split.length > 0){
            
            $.each(second_part_split, function(key, value){
                
                if(key > 0) {
                    newUrl += "/" + value;
                }
            });
        }
        return newUrl;
        
    }else{
        
        return url;
    }
};

var editUriGet = function(var_name, value, url) {
    
    var url = (url === undefined)? window.location.href : url;
    
    var slice_anchor = sliceAnchor(url);
    
        url = slice_anchor['url'];

    var url_split = url.split('/' + var_name + '/');

    if(url_split.length > 0) {

        var url_second_part = ((url_split[1]) !== undefined)? url_split[1] : '';
        
        var second_part_split = url_second_part.split('/');

        var bar = (url_split[0].slice(-1) === '/')? '' : '/';
        
        if(second_part_split.length > 0){
            
            var url_end = '';
            
            $.each(second_part_split, function(key, value){
                
                if(key > 0) {
                    
                    url_end += '/' + value;
                }
            });
                        
            var final_url = url_split[0] + bar + var_name + '/' + value + url_end;
        }else{
            
            var final_url = url_split[0] + bar + var_name + '/' + value;
        }
        
        if(slice_anchor['anchor'] !== null) {
            
            final_url += '#' + slice_anchor['anchor'];
        }
        console.log('monta1');
        console.log(final_url);
        
        return final_url;
    } else {
        console.log('monta2');
        console.log(url + '/' + var_name + '/' + value);
        return url + '/' + var_name + '/' + value;
    }
};

var getFiltersUri = function(column_id, url) {
    
    var url     = (url !== undefined)? url : window.location.href;
    
    if(parseInt(column_id) === 2) {
        
        var finalUrl = getFilterUri_AuxP(url);
    } else {
        
        var newUrl  = getFilterUri_Aux('L', column_id, url);
        var newUrl2 = getFilterUri_Aux('G', column_id, newUrl);
        var finalUrl = getFilterUri_AuxNull(column_id, newUrl2);
    }

    //console.log(url, finalUrl);
    return finalUrl;
};

var sliceAnchor = function(url){
    
    var final_url = null, anchor = null;
    
    var url_split = url.split('#');
    
    if(url_split.length > 0) {
        
        final_url = url_split[0];
        
        if(url_split[1] !== undefined) {
            
            anchor = url_split[1];
        }
    } else {
        
        final_url = url;
    }
    
    return {'url': final_url, 'anchor': anchor};
};

var getFilterUri_AuxP = function(url) {
    
    var input = $('#inputP');
    var value = input.val();
    var param = "pval";
    
    if(value !== '') {
        
        finalUrl = editUriGet(param, value, url);
    } else {
        
        finalUrl = removeUriGet(param, url);
    }
        
    return finalUrl;
};

var getFilterUri_AuxNull = function(column_id, url) {
    
    var input = $('#filters_c' + column_id + ' #inputNull');
    var value = input.val();
    var param = "c" + column_id + 'Null';
    
    if(value !== '0' && value !== '') {
        
        newUrl = editUriGet(param, value, url);
    } else {
        
        newUrl = removeUriGet(param, url);
    }
        
    return newUrl;
};

var getFilterUri_Aux = function(letter, column_id, url){
    
    var input  = $('#filters_c' + column_id + ' #filterValue' + letter);
    var select = $('#filters_c' + column_id + ' #filterOperator' + letter);
    
    var thanValue  = input.val();
    var hasValue   = (thanValue !== undefined && thanValue != '');
    var thanStatus = select.attr('data-value');
    var param      = "c" + column_id + thanStatus;
    
    if(hasValue) {
        
        newUrl = editUriGet(param, thanValue, url);
    } else {
        
        newUrl = removeUriGet(param, url);
    }
    
    return newUrl;
};

//Por conta do novo servidro, urls com barras tem causando problemas...
// Então susbstiui, /index/valor/1 por /index?valor=1&
var remove_slash = function(url)
{
    //console.log(url);
    var newurl = '';
    
    url = url.replace(/\?/g, "/").replace(/&/g,'/').replace(/=/g,'/');
    var parts = url.split('/');
    var isValue = false;
    
    for(i in parts){
        var part = parts[i];
        
        //Modulo/Controller
        if(i<3){
            newurl = newurl + part + '/';
        }else if(i==3){ //Action
            newurl = newurl + part + '?';
        }else{
            if(!isValue){
                newurl = newurl + part + '=';
                isValue = true;
            }else{
                newurl = newurl + part + '&';
                isValue = false;
            }
        }
    }
    
    return newurl;
}

$("body").ready(function() {
    
    var treated_url = urlScript;

    if(!treated_url.includes('extract/sort')) {

        treated_url =  '/indicator/extract/sort/' + urlScript;
    }

    var filtersHtml = [];

    $(".filterDivHtml").each(
        function(k,o){
            filtersHtml[$(o).attr('data-column_id')] = $(o).html();
        }
    );
    console.log(filtersHtml);
    //$(".filterDivHtml").each()

    // var filtersHtml = [ $("#filtersHtml_c0").html(), 
    //                     $("#filtersHtml_c1").html(),
    //                     $("#filtersHtml_c2").html()];
    
    $(".filterIco").on("click", function(){
        
        var column_id = $(this).attr('data-column_id');
        
        popUp.abre( "filter",
                    "Filtros",
                    filtersHtml[column_id],
                    "Cancelar",
                    "Filtrar",
                    "confirm","350px-300px", "branco", "bola", 
                    "",
                    function(){
                        
                        var filters_uri = getFiltersUri(column_id, treated_url);
                        //console.log(filters_uri);
                        window.location.href = remove_slash(filters_uri);
                    },
                    function(){
                        $("#filters_c" + column_id + ' .getFocus')[0].focus();
                    }
                );
    });
    
    $(".ico.order").on("click", function(){
        
        var orientation = ($(this).hasClass('active') && !$(this).hasClass('desc'))? 'desc' : 'asc';

        var column      = $(this).attr('data-column');
        
        var current_url = window.location.href;
        
        var new_url     = editUriGet('column', column, treated_url);
        
        if(orientation === 'desc') {
            
            var new_url = editUriGet('orientation', 'desc', new_url);
        } else {
            
            var new_url = removeUriGet('orientation', new_url);
        }
        
        if(new_url !== current_url) {

            window.location.href = remove_slash(new_url);
        }
    });
    
    $("body").delegate(".clearFilters","click", function(){
        
        var parent = $(this).parent();
        
        parent.children().val('');
    });
});