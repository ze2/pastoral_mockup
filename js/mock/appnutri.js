var mock = {};
(function(package, code){

  var get_sector = async function(){
  
    return new Promise(function(resolve, reject){
      
      model.Sector.select().one(function(sector){

        if(!!sector) return resolve(sector);

        var sector  = new model.Sector();
        sector.name = 'Sector mock';
        sector.save(resolve, reject);
      
      }, reject);
    });
  };
  package.get_sector = get_sector;

  var get_branch = async function(){
  
    var sector = await get_sector();

    return new Promise(function(resolve, reject){
      
      model.Branch.select().one(function(branch){

        if(!!branch) return resolve(branch);

        var branch  = new model.Branch();
        branch.name = 'Branch mock';
        branch.sector(sector);
        branch.save(resolve, reject);
      });
    });
  };
  package.get_branch = get_branch;

  var get_community = async function(){

    var branch = await get_branch();
  
    return new Promise(function(resolve, reject){
      
      model.Community.select().one(function(community){
      
        if(!!community) return resolve(community);

        var community  = new model.Community();
        community.name = 'Community mock';
        community.branch(branch);
        community.save(resolve, reject);
      })
    });
  };

  var get_child = async function(){

    var sector    = await get_sector();
    var community = await get_community();
  
    return new Promise(function(resolve, reject){
    
      model.Child.select().one(function(child){
      
        if(!!child) return resolve(child);

        var date  = new Date();
        date.setFullYear(date.getFullYear() - 2);

        var child  = new model.Child();
        child.name   = 'Mock 1';
        child.mother = 'Mock mãe';
        child.gender = 1;
        child.birth  = date;
        child.community(community);
        child.sector(sector);
        child.save(resolve, reject);
      });
    });
  };

  package.make_historical = async function(){

    var measures = [
      [0.7, 30], // Birth date | Erro de medida
      [3.07, 48],
      [3.35, 50.5],
      [4.1, 57.5],
      [5.13, 58.5],
      [null, 58.5],
      [5.4, 60],
      [5.81, 62],
      [6.3, 63.5],
      [6.3, null],
      [6.47, 65],
      [6.97, 66],
      [7, 69.5],
      [7.59, 69.5],
      [7.63, 71.5],
      [7.965, 72],
      [8.23, 73.5],
      [8.515, 76],
      [8.56, 78.5],
      [8.75, 80],
      [9.125, 81],
      [13, 105], //Last | Desnutrido grave | Estável
    ];

    var historical = [];

    var child     = await get_child();
    var sector    = await get_sector();
    var community = await get_community();
  
    measures.forEach(function(value, index){

      historical.push(new Promise(function(resolve, reject){

        var date = new Date(child.birth);
        date.setMonth(date.getMonth() + index); 
      
        var historic      = new model.Historic();
        historic.date     = date;
        historic.height   = value[1];
        historic.weight   = value[0];
        historic.child(child);
        historic.sector(sector);
        historic.community(community);
        historic.save(resolve, reject);
      }));
    });

    return Promise.all(historical);
  };

})(mock);
