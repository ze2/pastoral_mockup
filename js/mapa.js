/**
 *  Este arquivo é utilizado para manipulação do mapa.
 *  Extende o JQuery.
 */

/**
 *  Seleciona, de acordo com o fator de zoom informado,
 *  qual a opção do menu está selecionada. Caso seja 'z',
 *  não marca nenhuma.
 *  Utilizado apenas para funcionalidades que postam o form.
 */
function selecionaItemMenuAtivo(fator_zoom){
  switch(fator_zoom){
    /// Zoom In
    case '2':
      ativaLinkMenu('menu_zoom_in');
    break;

    /// Zoom Out
    case '-2':
      ativaLinkMenu('menu_zoom_out');
    break;

    /// Pan
    case '1':
      ativaLinkMenu('menu_pan');
    break;

    /// Pan
    case 'i':
      ativaLinkMenu('menu_info');
    break;
  }
}

/**
 *  Seta todas as opções do menu com style default,
 *  seleciona a opção informada com style diferenciado.
 */
function ativaLinkMenu(id){
  $('.menu_action').css("background-color","#557f01");
  $('.menu_action').css("border-top","solid 2px #ccc");
  $('.menu_action').css("border-left","solid 2px #ccc");
  $('.menu_action').css("border-right","solid 2px #555");
  $('.menu_action').css("border-bottom","solid 2px #555");

  $('#'+ id +' img').css("background-color","#050");
  $('#'+ id +' img').css("border-top","solid 2px #555");
  $('#'+ id +' img').css("border-left","solid 2px #555");
  $('#'+ id +' img').css("border-right","solid 2px #ccc");
  $('#'+ id +' img').css("border-bottom","solid 2px #ccc");
}

function zoom(nivel, id){
  ativaLinkMenu(id);
  $('#zoom_nivel').val(nivel);
}

function fullExtent(id){
  ativaLinkMenu(id);
  $('#zoom_nivel').val('z');
  $('#button_filtrar').click();
}

function worldExtent(id){
  ativaLinkMenu(id);
  $('#zoom_nivel').val('w');
  $('#button_filtrar').click();
}

function limpaFatorZoom(){
  $('#zoom_nivel').val('1');
  $('#form_mapa').submit();
}

function info(id){
  ativaLinkMenu(id);
  $('#zoom_nivel').val('i');
}

/**
 *  Recebe um evento click e retorna o ponto clicado.
 */
function getPoint(e){
  var el = e.target || e.srcElement;

  // Get mouse click co-ords
  var ePos = {x:0, y:0};
  if ('number' == typeof e.pageX){
    ePos.x = e.pageX;
    ePos.y = e.pageY;
  }
  else if ('number' == typeof e.clientX){
    ePos.x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    ePos.y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }

  // Get image top left co-ords
  var elPos = {x:0, y:0};
  if (el.offsetParent) {
    elPos.x = el.offsetLeft;
    elPos.y = el.offsetTop;
    while (el = el.offsetParent) {
      elPos.x += el.offsetLeft;
      elPos.y += el.offsetTop;
    }
  }

  point_x = ePos.x - elPos.x;
  point_y = ePos.y - elPos.y;

  res = new Array();
  res['point_x']  = point_x;
  res['point_y']  = point_y;

  return res;
}

/**
 *  Seta o ponto clicado na imagem
 */
function setClick(e){
  coordenadas = getPoint(e);

  // Write relative position of click to form controls
  $('#mapa_x').val(coordenadas['point_x']);
  $('#mapa_y').val(coordenadas['point_y']);

  $('#button_filtrar').click();
}

/**
 *  Verifica a dimensão do navegador e seta no campo apropriado.
 *  Como o IE não entende innerWidth, é necessário fazer a conta
 *  a partir da resolução da tela.
 */
function setWidthAndSubmitForm(){
  myWidth = window.innerWidth;

  if(myWidth == undefined){
    myWidth = screen.width - 50;
  }

  $("#width").val(myWidth);
  $("#button_filtrar").click();
}