/**
 *  No form de endereços do módulo rádios,
 *  ao alterar o campo de CEP, esta função é chamada para buscar,
 *  via ajax, o endereço do cep informado.
 */
function getEnderecoPessoa(cep){
  $('#carregando').show();

  $.post(
    '/radios/editar/get-endereco',
    'cep='+ cep,
    function(data){
      $('#municipio_id-element').html(data);
      $('#carregando').fadeOut('fast');
      $('#municipio_id-element').effect('highlight', {}, 500);
    }
  );
}

/**
 *  No form de endereços do módulo rádios,
 *  ao alterar o campo de Estado, esta função é chamada para buscar,
 *  via ajax, os municípios do estado selecionado.
 */
function getMunicipiosPessoa(uf){
  $('#carregando').show();

  $.post(
    '/radios/editar-pessoa/get-municipios',
    'uf='+ uf,
    function(data){
      $('#municipio_id-element').html(data);
      $('#carregando').fadeOut('fast');
      $('#municipio_id-element').effect('highlight', {}, 500);
    }
  );
}


var provider = function(id){

    var element = function(id){
    
        this.element = $('#' + id);
        this.display = $('#' + id + '_display');
        this.name    = $('#' + id + '_name');
        this.hname    = $('#' + id + '_hidden_name');
        this.provider_id    = $('#' + id + '_id');
        this.img     = $('#' + id + '_img');
        this.working = false;
        this.found   = false;
    };

    element.prototype.setValue = function(value, value2){
    
        this.name.val(value);
        this.provider_id.val(value2);
        this.getObject();
        this.name.focus();
    }

    element.prototype.getObject = function(){
    
        this.working = true;
        this.display.html('');
        var tax  = $.trim(this.name.val());
        var id  = $.trim(this.provider_id.val());
        var self = this;      

        if(tax == '') return;

        this.img.attr('src', '/images/ajax-loader.gif');
        $.getJSON('/entitynew/rest/entityByTax', {'tax': tax, 'id' : id}, function(json){ 
            self.parse.call(self, json) 
        }).fail(function(err){
            console.log( "error: ", err );
        });
    };

    element.prototype.parse = function(json){
        if(json.id == null){
            
        	this.display.text('');
            this.element.val('');
            this.hname.val('');
            this.provider_id.val('');
        	
            this.img.attr('src', '/images/icons/signal-attention.png');
            this.img.attr('title', 'Não encontrado');
            this.found = false;
        }else{
        	console.log(this);
            this.display.text(json.name);
            this.hname.val(json.name);
            this.element.val(json.id);
            this.provider_id.val(json.id);
            this.img.attr('src', '/images/icons/apply.png');
            this.found = true;

        }

        if(this.unparser != undefined){
            
            this.unparser();
        }

        this.working = false;
    };

    element.prototype.clear = function(){

        this.img.attr('src', '/images/icons/unknown.png');
        this.display.text('');
        this.element.val('');
        this.hname.val('');
        this.working = false;
        this.found   = false;
    };

    element.prototype.search = function(){
    
        var url = '/entitynew/entity/searchtax';
        url = url + '/target/' + this.name;

        var win = open(url, 'Fornecedor', 'width=1000, scrollbars=1');

        if(this.onunload != undefined){
            win.onunload = this.onunload;
        }
    };

    element.prototype.add = function(attr, callback){
    
        var path = '';

        for(x in attr){

            var value = attr[x];
            path += '/' + x + '/' + attr[x];
        }

        var url = '/entitynew/entity/index' + path + '/provider/true';
        var win = open(url, 'Fornecedor', 'width=800, scrollbars=1');

        console.log(url);

        var worked = function(){

            if(!win || win.closed){
            
                callback();
            }else{
            
                setTimeout(worked, 100);
            }
        };

        if(callback != undefined) worked();
    };

    return new element(id);
};
