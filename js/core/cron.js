$(function(){
	$('#class_name').change(function(){
		$('#carregando').show();
		
		$.post('/core/cron/change-class', 
				{'class_name': $('#class_name').val()}
		,function(data, errors){
			$('#carregando').hide();
			console.log(data);
			$.each(data, function(id, obj){
				if(id == 'error'){
					alert(obj);
				}
					
				if(id == 'exec_parameters'){
					if($('#exec_parameters').val()==''){
						$('#exec_parameters').val(obj);
					}
				}
			})
			
			
		}
		,'json')
		
		.fail(function(){
			
			alert('Ocorreu um erro ao consultar os parametros');
			$('#carregando').hide();
		})
	
	})

});