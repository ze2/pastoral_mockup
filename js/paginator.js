/**
 *  Para paginação em ajax, define o valor para o campo informado
 *  @param valor
 *    Valor que deverá ser setado no campo informado
 *  @param campo
 *    Campo que receberá o valor informado
 */
function setPagina(valor, campo,submit){
  field       = $("#"+ campo);
  field.val(valor);
  $("#"+ submit).click();
}

/**
 *  Alias for 'setPagina'
 */
function setPage(value, field, submit){
  setPagina(value, field, submit);
}

/**
 *  Esta função apenas é utilizada para o paginador inferior da página
 *  executar os eventos associados ao do topo da página clicando no item
 *  cujo id é dado.
 *  @param  id
 *    Id da imagem que será clicada.
 */
function clicaImagem(id){
  $("#"+ id).click();
}

/**
 *  Alias for 'clicaImagem'
 */
function imageClick(id){
  clicaImagem(id);
}
