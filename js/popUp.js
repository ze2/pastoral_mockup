/* Material G - popUp - version 0.4 */
var popUp = new Object();
popUp.numero = 0;
popUp.carregando = '';
popUp.carregandoX = 0;
popUp.carregandoY = 0;

popUp.mouseX = 0;
popUp.mouseY = 0;
popUp.mouseXR = 0;

popUp.lastMouseX = 0;
popUp.lastMouseY = 0;

popUp.abre = function(id, titulo, conteudo, textoBotao1, textoBotao2, tipo, tamanho, cor, animacao, funcaoBotao1, funcaoBotao2, callback){
    popUp.numero++;
    var id = (id === undefined || id === '')? 'popUp_' + popUp.numero : id ;
    var cor = (cor === undefined || cor === '')? 'normal': cor;
    var classesPop = 'popUp '+ tipo + ' ' + cor + ' ' + tamanho + ' ' + animacao;
    var style = '';
    var tamanhoArray = tamanho.split('-');
    if(tamanhoArray[1] !== undefined){
        style = 'width: ' + tamanhoArray[0] + '; height:' + tamanhoArray[1] + ';';
    }
    var classesFundo = 'popUpFundo ' + cor + ' ' + animacao;
    var xis = ''; var xisComp = ''; var bots = ''; var titleText = 'popUpHtext';
    $("html").css('overflow', 'hidden');
    switch(tipo){
        case 'alert' : 
                var bots    = $('<div>');
                var button1 = $('<button>').attr('id', id + '_botao1')
                                            .text(textoBotao1);
                bots.append(button1);
                
            break;
        case 'confirm' : 
                var bots    = $('<div>');
                var button1 = $('<button>').attr('id', id + '_botao1')
                                            .text(textoBotao1);
                var button2 = $('<button>').attr('id', id + '_botao2')
                                            .text(textoBotao2);
                bots.append(button1).append(button2);
                
            break;

    }
    var xis = $('<span>').attr('id', 'xis_'+ id).addClass('popUpXis').text('x');
            var xisComp = $('<span>').addClass('popUpXisComp');
            titleText = 'popUpHtextX';
        xis.on("click", function(){
            popUp.fecha(id);
        });
    
    var titleText = $('<div>').addClass(titleText).html(titulo);
    var h1 = $("<h1>").attr("data-popid", id);
        h1.append(xisComp);
        h1.append(titleText);
        h1.append(xis);
    
    var background = $("<div>").attr("id", 'fundo_'+id);
        background.addClass(classesFundo);
    var html = $("<div>").attr('id', id)
                         .attr('data-ani', animacao)
                         .attr('style', style)
                         .attr('data-xinicial','')
                         .attr('data-yinicial', '')
                         .append(h1);
    var wrapper = $("<div>").append(conteudo);          
    var content = $("<div>").append(wrapper);
        html.append(content).append(bots);
    
    
    $("body").prepend(background);
    $("body").prepend(html);
    
    html.css('top', popUp.mouseY - $(window).scrollTop())
        .css('left',popUp.mouseX - $(window).scrollLeft())
        .addClass(classesPop);
    
    if(!!button1){
        button1.on("click", function(){
            if((funcaoBotao1 !== undefined) && (funcaoBotao1 !== '')){
                funcaoBotao1();
            }else{
                popUp.fecha(id);
            }
        });
    }
    if(!!button2){
        button2.on("click", function(){
            if((funcaoBotao2 !== undefined) && (funcaoBotao2 !== '')){
                funcaoBotao2();
            }else{
                popUp.fecha(id);
            }
        });
    }

    if(tipo == 'message'){
        $("#xis_" + id + ", #fundo_" + id).click(function(){
            if((funcaoBotao1 !== undefined) && (funcaoBotao1 !== '')){
                funcaoBotao1();
            }else{
                popUp.fecha(id);
            }
        });
    }
    if(!!callback){
        callback();
    }
};
popUp.fecha = function(id, callback){
    
    if(id !== undefined && id !== ''){
        if($("#"+id).data('xinicial') !== ''){
            $("#"+id).animate({top:$("#"+id).data('yinicial'), left:$("#"+id).data('xinicial')}, 100, function(){
                popUp.fechaAcao(id); console.log(1);
            });
            
        }else{
            popUp.fechaAcao(id); console.log(2);
        }
        
    }else{
        $(".popUp").each(function(){
            var id= $(this).attr('id');
            if($("#"+id).data('xinicial') !== ''){
                $("#"+id).animate({top:$("#"+id).data('yinicial'), left:$("#"+id).data('xinicial')}, 50, function(){
                    popUp.fechaAcao(id);  console.log(3);
                });

            }else{
                popUp.fechaAcao(id);
            }
            
        });
    }
    if(callback !== undefined){
        callback();
    };
};

popUp.fechaAcao = function(id){
    if($(".popUp").length === 1){
        $("html").css('overflow', 'auto');
    }
    var animacao = $('#' + id).data('ani'); 
    $("#" + id + ', #fundo_'+id).removeClass(animacao).addClass(animacao + "Out"); 
    $("#fundo_" + id).fadeOut(505, function(){
        $("#fundo_" + id).remove();
        $("#" + $(this).attr("id") + ", #" + id).remove();
    });
};
popUp.atualizaPosicao = function(){
    popUp.lastMouseX =(popUp.lastMouseX === 0)? popUp.mouseX : popUp.lastMouseX;
    popUp.lastMouseY = (popUp.lastMouseY === 0)? popUp.mouseY : popUp.lastMouseY;
    var xMaior = (popUp.mouseX >= popUp.lastMouseX)? true : false;
    var yMaior = (popUp.mouseY >= popUp.lastMouseY)? true : false;
    var mX = (xMaior)? popUp.mouseX - popUp.lastMouseX : popUp.lastMouseX - popUp.mouseX;
    var mY = (yMaior)? popUp.mouseY - popUp.lastMouseY : popUp.lastMouseY - popUp.mouseY;
    popUp.lastMouseX = popUp.mouseX;
    popUp.lastMouseY = popUp.mouseY;
    var carregando = $("#" + popUp.carregando);
    var popUpXarray = carregando.css("left").split("px");
    var popUpYarray = carregando.css("top").split("px");
    popUp.carregandoX = Number(popUpXarray[0]).toFixed(0);
    popUp.carregandoY = Number(popUpYarray[0]).toFixed(0);
    
    $("#mCursor").html(popUp.mouseX + '-' + popUp.mouseY);
    
    
    $("#" + popUp.carregando + ' h1').css('cursor', 'move');
    var newTop = (yMaior)? Number(popUp.carregandoY) + Number(mY) : popUp.carregandoY - mY ;
    var newLeft = (xMaior)? Number(popUp.carregandoX) + Number(mX) : popUp.carregandoX - mX ;
    $("#mPop").html(newLeft + '-' + newTop);
    
    carregando.removeClass(carregando.data('ani'));
    
    carregando.css('display', 'block');
    carregando.css('top', newTop + 'px');
    carregando.css('left', newLeft + 'px');
    
};
popUp.iniciar = function(){
    $('body').delegate('.popUpHtext, .popUpHtextX, .popUpXisComp', 'touchstart', function(){
        if($(this).parent().attr('data-xinicial') === ''){
            $(this).parent().attr('data-xinicial', $(this).parent().css('left'));
            $(this).parent().attr('data-yinicial', $(this).parent().css('top'));
        }
        popUp.carregando = $(this).data('popid');
    });
    $('body').delegate('.popUpHtext, .popUpHtextX, .popUpXisComp', 'touchend', function(){
    
        popUp.carregando = '';
        $(this).css('cursor', '');
        popUp.lastMouseX = 0;
        popUp.lastMouseY = 0;
    });
    $('body').delegate('.popUpHtext, .popUpHtextX, .popUpXisComp', 'mousedown', function(){
        var popH = $(this).parent();
        var pop = $(this).parent().parent();
        if(pop.attr('data-xinicial') === ''){
            pop.attr('data-xinicial', pop.css('left'));
            pop.attr('data-yinicial', pop.css('top'));
        }
        popUp.carregando = popH.data('popid');
    });
    $('body').delegate('.popUpHtext, .popUpHtextX, .popUpXisComp', 'mouseup', function(){
        popUp.carregando = '';
        $(this).css('cursor', '');
        popUp.lastMouseX = 0;
        popUp.lastMouseY = 0;
    });
};



$("#altura, #largura").on("change", function(){
    if($("#altura").val() === '' || $("#largura").val() === ''){
        $("#tamanho").attr("disabled", false);
    }else{
         $("#tamanho").attr("disabled", true);
    }
});

//rastreando mouse
$(document).mousemove(function(e) {
    popUp.mouseX = e.pageX;
    popUp.mouseY = e.pageY;
    popUp.mouseXR = $("body").width() - popUp.mouseX;
    if(popUp.carregando !== ''){popUp.atualizaPosicao();}
});

$('body').ready(function(){
    popUp.mouseX = $(window).width() / 2;
    popUp.mouseY = $(window).height() / 2;
    popUp.mouseXR = $(window).width() / 2;
    popUp.iniciar();
    $("#generate").on("click", function(){
        var funcao1 = ($("#funcao1").val() === '')? '""' : $("#funcao1").val();
        var funcao2 = ($("#funcao2").val() === '')? '""' : $("#funcao2").val();
        var callback = ($("#callback").val() === '')? '""' : $("#callback").val();
        var tamanho = ($("#altura").val() !== '' && $("#largura").val() !== '')?$("#largura").val() + '-' + $("#altura").val() : $("#tamanho").val();
        var string = 'popUp.abre("'+$("#id").val()+'","'+$("#titulo").val()+'","'+$("#conteudo").val()+'","'+$("#botao1").val()+'","'+$("#botao2").val()+'","'+ $("#tipo").val()+'","'+ tamanho +'", "'+$("#cor").val()+'", "'+$("#animacao").val()+'", '+funcao1+','+funcao2+','+callback+');';

        $("#code").html(string);
        eval(string);

    });
});
