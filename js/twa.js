jquery = $;

Path = null;
self = null;
var twa = {

	clone: function(obj) {
	
		var clone = {};
		for(var i in obj) {
			if(typeof(obj[i])=="object")
				clone[i] = twa.clone(obj[i]);
			else
				clone[i] = obj[i];
		}
		return clone;
	},

	index: function(){},
	
	Class: function(){

		this.index = new function(){};
		this.FORM	= '#form form';
		this.PATH	= 'twa';
		this.TARGET = '#formatContent';

		// Construtor da página
		this.init = function(){ return this; };

		// Renderiza o html no target
		this.show = function(target){
			
			var html = jquery('#' + twa.cache[this.PATH]).html();
			var target = target === undefined ? this.TARGET : target;
			$(target).html(html);		
			return this;
		};

		this.hide = function(){
		
			$(this.TARGET).html('');
		},

		// Método para validação
		this.validate = function(){ return true };

		// Envia dados via get
		this.get = function(data){

			O(this.PATH,{'data':data,'cache':false});	
			return this;
		};

		// Envia dados via post
		this.post = function(obj){
			
			var data = null;
			var path = this.PATH;
			if(typeof(obj) == 'object'){
				if(obj['data'] !== undefined) data = obj['data'];
				if(obj['path'] !== undefined) path = obj['path'];
			};
			O(path,{'data':data,'cache':false,'method':'post'});	

			return this;
		};

		// Valida e submete o formulário via ajax
		this.submit = function(obj){
			
			try{
				alert(this.FORM);
				var data = $(this.FORM).serialize();
				var path = $(this.FORM).attr('action');
				if(self.validate()){
					self.post({'data':data,'path':path});
				}else{
				}
				return false;
			}
			catch(e){
				return false;
			}
		};

		// Trás o formulário
		this.form = function(){
		
			self.get($(self.FORM).serialize());
			return self;
		};

		// Contrutor que não pode ser sobrescrito
		this._build = function(){

			self = this;
			$(self.FORM).submit(self.submit);
		}
	},

	cache: [],

	include: function(path,setup){
		
		var settings = {
			async: false,
			url: twa.pathToUrl(path),
			dataType: 'html',
			timeout: 200,
			type: 'GET',
			beforeSend: function(){
				jquery('#status').text('Carregando...');
			},
			success: function(data){
				if(twa.cache[path] !== undefined) jquery('#' + twa.cache[path]).remove();
				twa.cache[path] = path.replace(/\./g,'').toUpperCase() + 'CACHE';
				jquery('#CACHE').append('<div id="' + twa.cache[path] + '">' + data + '</div>');
			},
			complete: function(){
				jquery('#status').text('');
			},
			error: function(erro){
				alert(erro);
			}
		};
		
		if(setup !== undefined){
			if(setup['data'] !== undefined) settings['data'] = setup['data'];
		}

		jquery.ajax(settings);
	},

	
	pathToUrl: function(path){

		return '/' + path.replace(/\./g,'/');
	},
}

var O = function(path,setup){

	var cache = true;
	if(setup !== undefined){
		if(setup['cache'] !== undefined) cache = setup['cache'];
	}
	// verifica se o site já foi baixado
	if((path in twa.cache) && (cache !== false)){
		var obj = eval(path);
		obj._build();
		obj.init();
		return obj;
	}else{
		// inclui o site
		twa.include(path,setup);
		// transforma o path em objeto
		if(Class !== null){
			// class herda twa.i
			Class.prototype = twa.i;
			// grava a inst
			path = Path === null ? path : Path;
			eval(path + ' = new Class()');
			// Limpa Class e Path
			Class	= null;
			Path	= null;
			var obj = eval(path);	
			// add path
			obj.PATH = path;
			// executa os construtores 
			obj._build();
			obj.init();
			// Deleta a Class
			//Class = null;
			return obj;
		}
	}
};
jquery(function(){
	jquery('body').append('<div id="CACHE" style="display:none;" ></div>');
	twa.i = new twa.Class();
});

var exec = function(instance,callback){
	self = instance;
	callback();
	return self;
}
