<?
//Simulacao de tela inicial de diocese
include_once("header_mockup.php");
include_once("zend_view_mockup.php");

//Apenas para o fluxo ficar parecido com o Zend.
class ZendActionMockup
{
	public function __construct(){
		$this->view = new ZendViewMockup();
		$this->translate = new TranslateMockup();
	}
	
	public function render($fileName)
	{
		$this->view->render($fileName);
	}
	
	public function dioceseAction()
    {
        //Dados mockup para criação do layout
        $session = new Pastoral_Session_Namespace();

        $this->view->translate = $this->translate;
        $this->view->user_name = $session->user->name;

        $this->view->diocese_name = "Diocese Rui Barbosa";

        //Dados mockup para envios ao setor
        $this->view->sended_to_sector_link = "#";
        $this->view->sended_to_sector = Array(
            Array("name" => "06 - Apoio financeiro mensal", "value" => 123.12),
            Array("name" => "12 - Apoio eventual com saldo", "value" => 54.00),
            Array("name" => "14 - Campanha de luz", "value" => 554.00),
            Array("name" => "15 - AFL (apoio de capacitação de lideres)", "value" => 333.03),
            Array("name" => "60 - Ramos - AFM pago pela CNPC", "value" => 1230.00),
            Array("name" => "61 - Ramos - AFM pago pela CNPC", "value" => 200.50),
            Array("name" => "62 - Ramos -Campanha de luz pago pela CNPC", "value" => 204.00),
        );
        $total = 0;
        foreach($this->view->sended_to_sector as $send){
            $total+= $send["value"];
        }
        $this->view->sended_to_sector_total = $total;

        //Dados mockup para o monitor de prestação de gastos
        $this->view->monitor_indicators_link = "#";
        $this->view->monitor_indicators = Array(
            Array("name" => "Última Pg recebida", "value" => "01/2021", "link" => "#"),
            Array("name" => "Última Pg auditada", "value" => "12/2020", "link" => "#"),
            Array("name" => "Saldo auditado da última PG", "value" => 5627.77, "link" => "#"),
            Array("name" => "Diferença de saldo", "value" => 0, "link" => "#"),
            Array("name" => "AES - Saldo auditado", "value" => 1, "link" => "#"),
            Array("name" => "% Ramos com mandato atrasado", "value" => 10, "link" => "#"),
            Array("name" => "% Paróquias com prestação de cntas em dia", "value" => 99, "link" => "#"),
            Array("name" => "% Paróquias que recebem direto da CNPC", "value" => 78, "link" => "#"),
        );
        $this->view->monitor_indicator_nota_1_a_5 = 4;

        //Dados mockup para o prestação de contas por tipo
        $this->view->pg_types_link = "#";
        $this->view->pg_types = Array(
            Array("name" => "07 - PRestação de gastos de ramos sem notas", "value" => 446.32),
            Array("name" => "07 - PRestação de gastos de ramos com notas", "value" => 1446.32),
            Array("name" => "02 - Apoio Eventual com saldo", "value" => 1554.00),
        );
        $this->view->pg_types_total = Array(5000,10000,15000);


         //Dados mockup para o prestação de contas por tipo
         $this->view->pg_types_link = "#";
         $this->view->pg_types = Array(
             Array("name" => "07 - Prestação de gastos de ramos sem notas", "value" => 446.32),
             Array("name" => "07 - Prestação de gastos de ramos com notas", "value" => 1446.32),
             Array("name" => "02 - Apoio Eventual com saldo", "value" => 1554.00),
         );
         $this->view->pg_types_total = Array(5000,10000,15000);


         //Dados mockup para o parecer do coordenador
         $this->view->coordinator_details_link = "#";
         $this->view->coordinator_details = Array(
             Array("id" => "1", "name" => "Despesa compatível com Recurso Governamental (AFM)", "value" => 4534.23, "pc"=>16.44, "notas" => 51, "link"=>"#"),
             Array("id" => "3", "name" => "Outros", "value" => 3395.40, "pc"=>12.31, "notas" => 13, "link"=>"#"),
             Array("id" => "4", "name" => "Recibo de Comunidade", "value" => 4457, "pc"=>16.16, "notas" => 136, "link"=>"#"),
             Array("id" => "7", "name" => "Compra de Máscaras", "value" => 2270, "pc"=>8.23, "notas" => 5, "link"=>"#"),
             Array("id" => "8", "name" => "Não informado", "value" => 200, "pc"=>0.73, "notas" => 2, "link"=>"#"),

         );

         //Dados mockup para o monitor de paróquias
         $this->view->parishes_link = "#";
         $this->view->parishes = Array(
             Array("id" => "2862", "name" => "Nossá Senora de Nazaré", "last_pg"=>"12/2020", "saldo_last_pg" => 665,31, "receita_last_pg" => 123.45,  "saldo_final_last_pg"=>432.22, "link"=>"#"),
             Array("id" => "2863", "name" => "São João", "last_pg"=>"01/2021", "saldo_last_pg" => 465,31, "receita_last_pg" => 723.45,  "saldo_final_last_pg"=>435.22, "link"=>"#"),
             Array("id" => "2864", "name" => "Nossá Senora da Paz", "last_pg"=>"04/2021", "saldo_last_pg" => 4665,31, "receita_last_pg" => 1323.45,  "saldo_final_last_pg"=>32.22, "link"=>"#"),
             Array("id" => "3030", "name" => "Nossá Senora do Rosário", "last_pg"=>"03/2021", "saldo_last_pg" => 6965,31, "receita_last_pg" => 1213.45,  "saldo_final_last_pg"=>4332.22, "link"=>"#"),

             Array("id" => "1", "name" => "Nossá Senora de Nazaré", "last_pg"=>"12/2020", "saldo_last_pg" => 665,31, "receita_last_pg" => 123.45,  "saldo_final_last_pg"=>432.22, "link"=>"#"),
             Array("id" => "2", "name" => "São João", "last_pg"=>"01/2021", "saldo_last_pg" => 465,31, "receita_last_pg" => 723.45,  "saldo_final_last_pg"=>435.22, "link"=>"#"),
             Array("id" => "3", "name" => "Nossá Senora da Paz", "last_pg"=>"04/2021", "saldo_last_pg" => 4665,31, "receita_last_pg" => 1323.45,  "saldo_final_last_pg"=>32.22, "link"=>"#"),
             Array("id" => "4", "name" => "Nossá Senora do Rosário", "last_pg"=>"03/2021", "saldo_last_pg" => 6965,31, "receita_last_pg" => 1213.45,  "saldo_final_last_pg"=>4332.22, "link"=>"#"),

             Array("id" => "4862", "name" => "Nossá Senora de Nazaré", "last_pg"=>"12/2020", "saldo_last_pg" => 665,31, "receita_last_pg" => 123.45,  "saldo_final_last_pg"=>432.22, "link"=>"#"),
             Array("id" => "4863", "name" => "São João", "last_pg"=>"01/2021", "saldo_last_pg" => 465,31, "receita_last_pg" => 723.45,  "saldo_final_last_pg"=>435.22, "link"=>"#"),
             Array("id" => "4864", "name" => "Nossá Senora da Paz", "last_pg"=>"04/2021", "saldo_last_pg" => 4665,31, "receita_last_pg" => 1323.45,  "saldo_final_last_pg"=>32.22, "link"=>"#"),
             Array("id" => "4030", "name" => "Nossá Senora do Rosário", "last_pg"=>"03/2021", "saldo_last_pg" => 6965,31, "receita_last_pg" => 1213.45,  "saldo_final_last_pg"=>4332.22, "link"=>"#"),

             Array("id" => "5862", "name" => "Nossá Senora de Nazaré", "last_pg"=>"12/2020", "saldo_last_pg" => 665,31, "receita_last_pg" => 123.45,  "saldo_final_last_pg"=>432.22, "link"=>"#"),
             Array("id" => "5863", "name" => "São João", "last_pg"=>"01/2021", "saldo_last_pg" => 465,31, "receita_last_pg" => 723.45,  "saldo_final_last_pg"=>435.22, "link"=>"#"),
             Array("id" => "5864", "name" => "Nossá Senora da Paz", "last_pg"=>"04/2021", "saldo_last_pg" => 4665,31, "receita_last_pg" => 1323.45,  "saldo_final_last_pg"=>32.22, "link"=>"#"),
             Array("id" => "5030", "name" => "Nossá Senora do Rosário", "last_pg"=>"03/2021", "saldo_last_pg" => 6965,31, "receita_last_pg" => 1213.45,  "saldo_final_last_pg"=>4332.22, "link"=>"#"),

             Array("id" => "2662", "name" => "Nossá Senora de Nazaré", "last_pg"=>"12/2020", "saldo_last_pg" => 665,31, "receita_last_pg" => 123.45,  "saldo_final_last_pg"=>432.22, "link"=>"#"),
             Array("id" => "2663", "name" => "São João", "last_pg"=>"01/2021", "saldo_last_pg" => 465,31, "receita_last_pg" => 723.45,  "saldo_final_last_pg"=>435.22, "link"=>"#"),
             Array("id" => "2664", "name" => "Nossá Senora da Paz", "last_pg"=>"04/2021", "saldo_last_pg" => 4665,31, "receita_last_pg" => 1323.45,  "saldo_final_last_pg"=>32.22, "link"=>"#"),
             Array("id" => "3630", "name" => "Nossá Senora do Rosário", "last_pg"=>"03/2021", "saldo_last_pg" => 6965,31, "receita_last_pg" => 1213.45,  "saldo_final_last_pg"=>4332.22, "link"=>"#"),

         );
         
        $this->render("diocese.phtml");
         
    }
    
    
}


$action = new ZendActionMockup();
$action->dioceseAction();

include_once("footer_mockup.php");
