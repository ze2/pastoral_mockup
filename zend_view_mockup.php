<?php
class ZendViewMockup
{
	public function render($fileName)
	{
		include($fileName);
		
	}
}

class TranslateMockup
{
	public function _($txt)
	{
		$args = func_get_args();
		return $this->_replaceMagic("$txt", $args);
	}
	
	public function _replaceMagic($text, $args)
    {
        for($x=1; $x<count($args); $x++){
            $text = str_replace('%' . $x, "{$args[$x]}", $text);
        }
        
        return $text;
    }
}

class Pastoral_Session_Namespace{
	public function __construct()
	{
		$this->user = new StdClass();
		$this->user->id = 62;
		$this->user->name = "Gilberto Griesbach Junior";
	}
}
